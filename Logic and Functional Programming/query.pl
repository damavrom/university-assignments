parse_weight(Atom, Phrase, Weight) :-
    Atom = Phrase-Weight,
    number(Weight).
parse_weight(Atom, Atom, 1) :-
    Atom = _-Weight,
    not(number(Weight)).
parse_weight(Atom, Atom, 1) :-
    not(Atom = _-_).

make_queries(Phrase, _, []):-
    tokenize_atom(Phrase, L),
    length(L, 1).
make_queries(Phrase, Weight, Queries):-
    tokenize_atom(Phrase, L),
    length(L, N),
    not(length(L, 1)),
    not(length(L, 0)),
    NewWeight is Weight / N,
    append_weight(L, NewWeight, Queries).

append_weight([], _, []).
append_weight([H|T], Weight, [NewH|NewT]):-
    NewH = H - Weight,
    append_weight(T, Weight, NewT).

preprosses_queries([], [], []).
preprosses_queries([HQ|TQ], [HP|TP], [HW|TW]):- 
    parse_weight(HQ, HP, HW),
    make_queries(HP, HW, Rest),
    append(TQ, Rest, All),
    preprosses_queries(All, TP, TW).

topic_query_score(Query, Weight, Topic, Weight):-
    sub_string(case_insensitive, Query, Topic).
topic_query_score(Query, _, Topic, 0):-
    not(sub_string(case_insensitive, Query, Topic)).
    
topic_queries_score([], [], _, 0).
topic_queries_score([HQ|TQ], [HW|TW], Topic, Score):-
    topic_query_score(HQ, HW, Topic, HS),
    topic_queries_score(TQ, TW, Topic, TS),
    Score is HS + TS.

topics_queries_score(_, _, [], []).
topics_queries_score(Phrases, Weights, [HT|TT], [HS|TS]):-
    topic_queries_score(Phrases, Weights, HT, HS),
    topics_queries_score(Phrases, Weights, TT, TS).

score_session(Phrases, Weights, Title, Topics, Score):-
    topic_queries_score(Phrases, Weights, Title, TitleS),
    TitleScore is TitleS * 2,
    topics_queries_score(Phrases, Weights, Topics, Scores),
    max_list([TitleScore|Scores],Max),
    sum_list([TitleScore|Scores],Sum),
    Score is Sum + Max * 1000.

query(Queries):-
    session(X,Y),
    preprosses_queries(Queries, Phrases, Weights),
    score_session(Phrases, Weights, X, Y, Score),
    write("Session: "),
    write(X),
    write("\n\tRevelance = "),
    write(Score).
