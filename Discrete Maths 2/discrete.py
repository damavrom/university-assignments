kids=[]
table={}
def prop21rec(a,b):
	cnt=int((b-1)/2)-a
	if cnt<0:
		return 0
	global kids
	if cnt!=0:
		kids=kids+[int((b-1)/2)-a]
	i=a+1
	while True:
#		if not (i,b-i) in table:
#			table[(i,b-i)]=prop21rec(i,b-i)
		t=prop21rec(i,b-i)
#		cnt+=table[(i,b-i)]
		cnt+=t
#		if table[(i,b-i)]==0:
		if t==0:
			return cnt
		i+=1
	return cnt
def prop21(n):
	table.clear()
	kids.clear()
	r=prop21rec(0,n)
	kids.sort(reverse=True)
	#print(kids)
	d={}
	for i in range(max(kids),0,-1):
		d[i]=kids.count(i)
	print(d.values())
	return r+1

def prop21b(n):
	t=dict()
	t[0,n]=1
	t[1,n-1]=1
	for i in range(2,n):
		#for j in range(i+1,n+1-i):
		for j in range(max(n-int((i**2+i)/2),i+1),n+1-i):
			t[i,j]=0
			if (i-1,j+i) in t:
				t[i,j]+=t[i-1,j+i]
			if (i-1,j+1) in t:
				t[i,j]+=t[i-1,j+1]
	#print(t)
	return sum(t.values())

def prop22rec(a,b):
	cnt=1
	i=a
	while i<=b/2:
		if not (i,b-i) in table:
			table[(i,b-i)]=prop22rec(i,b-i)
		cnt+=table[(i,b-i)]
		i+=1
	return cnt
def prop22(n):
	table.clear()
	i=prop22rec(1,n)
	return i

def mult(p1,p2,n):
	new=[]
	for i in p1:
		for j in p2:
			if i[1]+j[1]<=n:
				f=True
				for k in range(len(new)):
					if new[k][1]==i[1]+j[1]:
						new[k][0]+=i[0]*j[0]
						f=False
				if f:
					new=new+[[i[0]*j[0],i[1]+j[1]]]
	r=[]
	for i in new:
		if i[0]!=0:
			r=r+[i]
	return r

def prop23(n,A):
	parts=[]
	for i in range(len(A)):
		parts=parts+[[]]
		for j in range(0,n+1):
			if j*A[i]<=n:
				parts[i]=parts[i]+[[1,j*A[i]]]
	t=[[1,0]]
	for i in parts:
		t=mult(t,i,n)
	for i in t:
		if i[1]==n:
			return i[0]
	return 0
