#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
unsigned long long prop21baux(int a, int b, unsigned long long **table,int **table2)
{
	if (a>=0&b>=0)
		table2[a][b]++;
//	if (table[a][b]>0)
//		return table[a][b];
//	if (table[a-1][b-2]==(b-3)/2-a+1){
//		table[a][b]=(b-3)/2-a+1;
//		return table[a][b];
//	}
//	unsigned long long cnt=((b-1)/2)-a;
//	for (int i=a+1;(b-1)/3>=i;i++)
//		cnt+=prop21baux(i,b-i,table,table2);
//	table[a][b]=cnt;
	unsigned long long cnt=1;
	for (int i=a+1;(b-1)/2>=i;i++)
		cnt+=prop21baux(i,b-i,table,table2);
	table[a][b]=cnt;
	return cnt;
}
unsigned long long prop21b(int n, unsigned long long **table)
{
	table=(unsigned long long **)malloc(sizeof(unsigned long long*)*n);
	int **table2;
	table2=(int **)malloc(sizeof(int *)*n);
	for (int i=0;i<n;i++){
		table[i]=(unsigned long long *)malloc(sizeof(unsigned long long)*(n+1));
		table2[i]=(int *)malloc(sizeof(int)*(n+1));
		for (int j=0;j<n+1;j++){
			table[i][j]=0;
			table2[i][j]=0;
		}
	}
//	for (int i=0;i<n+1;i++)
//		for (int j=(int)(-1+sqrt(1+8*(n-i)))/2;3*j+i<n+1;j++)
//			prop21baux(j,2*j+i,table);
	unsigned long long r=prop21baux(0,n,table,table2);
//	for (int i=0;i<n/2;i++)
//		for (int j=2*i+1;j<n+1;j++)
//			table[i][j]=prop21baux(i,j,table);
	for (int j=12;j<n+1;j++){
		//unsigned long long p=table[i-1][j-2]-table[i][j];
		printf("%d",n-j);
		if (n-j<10)
			printf("   ");
		else if (n-j<100)
			printf("  ");
		else if (n-j<1000)
			printf(" ");
		else
			printf("");
	}
	printf("\n");
	for (int i=0;i<n/2;i++){
		for (int j=12;j<n+1;j++){
			//unsigned long long p=table[i-1][j-2]-table[i][j];
			unsigned long long p=table[i][j];
			p=table2[i][j];
			printf("%llu",p);
			if (p<10)
				printf("   ");
			else if (p<100)
				printf("  ");
			else if (p<1000)
				printf(" ");
			else
				printf("");
		}
		printf(" %d\n",i);
	}
	for (int i=0;i<n/2;i++)
		free(table[i]);
	free(table);
	return r;
}
unsigned long long prop21(int n, unsigned long long **table)
{
	table=(unsigned long long **)malloc(sizeof(unsigned long long*)*n);
	for (int i=0;i<n;i++){
		table[i]=(unsigned long long *)malloc(sizeof(unsigned long long)*(n+1));
		for (int j=0;j<n+1;j++)
			table[i][j]=0;
	}
	table[0][n]=1;
	table[1][n-1]=1;
	for (int i=2;i<n;i++)
		for (int j=i+1;j<n+1-i;j++){
			table[i][j]+=table[i-1][j+1];
			if (i+j<n+1)
				table[i][j]+=table[i-1][j+i];
		}
	unsigned long long sum=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<n+1;j++)
			sum+=table[i][j];
	for (int i=0;i<n;i++)
		free(table[i]);
	free(table);
	return sum;
}

int main()
{
	int s=10;
	int e=s+68;
	if (e>792)
		e=792;
	int runs=10;
	clock_t times[e-s];
	unsigned long long r;
	for (int i=0;i<e-s;i++)
		times[i]=0;
	for (int j=0;j<runs;j++){
		unsigned long long **table=(unsigned long long **)malloc(sizeof(unsigned long long*)*(e+1));
		for (int i=0;i<e+1;i++){
			table[i]=(unsigned long long *)malloc(sizeof(unsigned long long)*(e+1));
			for (int j=0;j<e+1;j++)
				table[i][j]=0;
		}
		for (int i=0;i<e-s;i++){
			clock_t start=clock();
			r=prop21b(i+s,table);
			times[i]+=(clock()-start);
			if (j==0)
				printf("%d %llu\n",i+s,r);
		}
		FILE *f=fopen("times.dat","w");
		for (int i=1;i<e-s;i++)
			fprintf(f,"%d %d\n",i+s,times[i]/(j+1));
		fclose(f);
	}
}
