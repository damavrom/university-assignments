import random
# including the file where convolution is implemented
from myconvolve import MyConvolve

# Reading size of first vector
N = int(input('Size of first vector:')) 

# Defining and initializing first vector with random values
# The randoms are of the (-1, 1) range
A = [2*random.random()-1 for i in range(N)]
# Defining and initializing second vector with fixed values
B = [1/5]*5

# Computing and printing the product of convolution of first and second
# vectors
C = MyConvolve(A, B)
print(C)
