# This package is necessary for reading wav files
from scipy.io import wavfile
# This package is necessary for signal processing
import scipy.signal
from myconvolve import MyConvolve
# numpy is necessary for editing the vectors return from reading the
# audio files
import numpy

# This function is used so if the vector has values exceeding the
# values that can be written into the file, it normalizes the vector
# so it can fit into the file
def normalize(v, maxim):
    m = max(-v.min(), v.max())
    r = maxim/m
    v = (v*r).astype("int")
    return v

# Reading the audio files
print("reading audio files...")
noise_rate, noise_data = wavfile.read("pink_noise.wav")
sample_rate, sample_data = wavfile.read("sample_audio.wav")
# Converting the acquired vectors to `int` to avoid memory overflow
noise_data = noise_data.astype('int')
sample_data = sample_data.astype('int')

# Downsample noise to have the same rate as the sample audio
print("downsampling noise...")
n_samples = round(len(noise_data)*sample_rate/noise_rate)
noise_data = scipy.signal.resample(noise_data, n_samples).astype('int')

# Run my function that convolves
print("convolving sample audio with pink noise...")
conv = MyConvolve(sample_data, noise_data)
# I need the vector to be of numpy array instead of the build-in list
conv = numpy.array(conv)
# Normalizing in order to fit in 16-bit file
conv = normalize(conv, 2**15-1)
# Converting the result to 16-bit numpy matrix
conv = conv.astype('int16')
# Writing to the desired file
print("writing result into wav file...")
wavfile.write("pinkNoise_sampleAudio.wav", sample_rate, conv)
print("wav file is ready")

# Generating a random vector -white noise- in the same shape as the
# pink noise. Its maximum possible value is 2**15-1 and its minimum
# possible value is -2**15 in order to fit in a 16-bit numpy array.
print("generating white noise signal...")
noise_data = numpy.random.randint(-2**15, high = 2**15-1, size = noise_data.shape)

# From this point on, the program goes like the above part
print("convolving sample audio with white noise...")
conv = MyConvolve(sample_data, noise_data)
conv = numpy.array(conv)
conv = normalize(conv, 2**15-1)
conv = conv.astype('int16')
print("writing result into wav file...")
wavfile.write("whiteNoise_sampleAudio.wav", sample_rate, numpy.array(conv).astype('int16'))
print("wav file is ready")

# The whole program may need several hours to be executed
