# This function returns the convolution of `A` and `B` at the `n` point.
# `A` and `B` are list of numbers objects and `n` is an integer number.
# It returns either an integer or a float depending on the types of `A`
# and `B`
def MyConvolveAux(n, A, B):
    s = 0
    # Compute the bounds of the sum for all of its non zero parts
    lb = max(0, n-len(B)+1)
    ub = min(n+1, len(A))
    # Compute the sum
    for k in range(lb, ub):
        s += A[k]*B[n-k]
    return s

# This function returns the vector product of the convolution `A` and
# `B` at all the points that convolution is not certain that it is
# zero.
# `A` and `B` are list of numbers objects and `n` is an integer number.
# It returns a list that is the vector product of the convolution.
def MyConvolve(A, B):
    # Compute the bounds where the convolution is not certainly zero
    lb = 0
    ub = len(A)+len(B)-1
    C = list()
    per = (ub-lb)//200
    if per == 0:
        # do not show progress log if vectors are too small
        per = 200
    # Compute every specific point of the convolution and add it to the
    # `C` vector
    for n in range(lb, ub):
        if (n-lb)%per == 0:
            print("{}%  \r".format((100*(n-lb))//(ub-lb)), end = "")
        C.append(MyConvolveAux(n, A, B))
    return C
