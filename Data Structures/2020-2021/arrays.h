class Array {
    private:

    /*
    ** array that contains all the elements
    */
    std::string* table;

    /*
    ** size of the array
    */
    int tableSize;

    /*
    ** number of elements that are currently in the array
    */
    int elements;

    /*
    ** resizes the current array to the double of the number of the 
    ** contained elements
    */
    void resize();
    
    public:

    /*
    ** constructor of the array
    */
    Array();

    /*
    ** inserts the element q in the array
    */
    void insert(const std::string q);

    /*
    **  returns random element of table 
    */
    std::string randomElement();

    /*
    ** implementation of [] operator
    ** it returns "" for index out of range
    */
    std::string operator[](const int indx);
};

class UnsortedArray {
    protected:

    /*
    ** array that contains all the elements
    */
    std::string* table;

    /*
    ** array that that contains the quantity of an element
    */
    int* quantity;

    /*
    ** size of the array
    */
    int tableSize;

    /*
    ** number of elements that are currently in the array
    */
    int elements;

    /*
    ** resizes the current array to the double of the number of the 
    ** contained elements
    */
    void resize();

    public:

    /*
    ** constructor of the array
    */
    UnsortedArray();

    /*
    ** inserts the element q in the array
    */
    void insert(const std::string q);

    /*
    ** counts the occurrences of element q
    */
    int contains(const std::string q);
};

class SortedArray: public UnsortedArray {
    private:

    /*
    ** returns the position of element
    ** if element is not in array then it returns the position it would
    ** have if it existed
    */
    int position(const std::string q);

    public:

    /*
    ** overloaded inherited methods
    */
    void insert(const std::string q);
    int contains(const std::string q);
};
