#include <string>
#include "hash_table.h"

HashTable::HashTable() {
    this->tableSize = 2;
    this->table = new std::string[this->tableSize];
    this->quantity = new int[this->tableSize];
    for (int i = 0; i < tableSize; i++) {
        this->quantity[i] = 0;
    }
    this->elements = 0;
}

void HashTable::reorder() {
    int oldTableSize = this->tableSize;
    int* oldQuantity = this->quantity;
    std::string* oldTable = this->table;
    //initialize new tables
    this->tableSize = this->elements * 2;
    this->table = new std::string[this->tableSize];
    this->quantity = new int[this->tableSize];
    for (int i = 0; i < tableSize; i++) {
        this->quantity[i] = 0;
    }
    //insert elements from old tables
    for (int i = 0; i < oldTableSize; i++)
        if (oldQuantity[i] > 0)
            this->insert(oldTable[i], oldQuantity[i]);
    //delete old tables
    delete[] oldQuantity;
    delete[] oldTable;
}

int HashTable::hf(const std::string s) {
    long int n = 0;
    for (int i = 0; i < s.length(); i++) {
        long int a = 1;
        for (int j = 0; j < i && j < 5; j++)
            a *= 26;
        n += s[i]*a;
    }
    return n%this->tableSize;
}

int HashTable::contains(const std::string q) {
    int startPos = hf(q);
    int p = startPos;
    do {
        if (this->quantity[p] == 0)
            return 0;
        if (this->table[p].compare(q) == 0)
            return this->quantity[p];
        p = (p+1)%this->tableSize;
    } while (startPos != p);
    return 0;
}

void HashTable::insert(const std::string q, const int count) {
    int startPos = this->hf(q);
    int p = startPos;
    do {
        if (this->quantity[p] == 0) {
            this->quantity[p] = count;
            this->table[p] = q;
            this->elements++;
            return;
        }
        if (this->table[p].compare(q) == 0) {
            this->quantity[p] += count;
            return;
        }
        p = (p+1)%this->tableSize;
    } while (startPos != p);

    this->reorder();
    this->insert(q);
}

void HashTable::insert(const std::string q) {
    this->insert(q, 1);
}
