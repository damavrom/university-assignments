#include <fstream>
#include <iostream>
#include <string>
#include <ctime>

#include "arrays.h"
#include "trees.h"
#include "hash_table.h"

#define NUMBER_OF_QUERIES 1000

int main(const int argc, const char **argv) {

    if (argc < 2) {
        std::cout << "please give filename as argument" << std::endl;
            return 1;
    }
    clock_t start;
    clock_t duration;

    srand(time(0));

    /******************************************************************
    ** reading and preparing words
    */
    Array words = Array();
    std::cout << "reading text... ";
    std::string filename = argv[1];
    std::ifstream inputFile(filename);
    int wordsCnt = 0;
    while (!inputFile.eof() && wordsCnt < 1000000) {
        std::string wordRaw;
        inputFile >> wordRaw;
        std::string word;
        for (int i = 0; i < wordRaw.length(); i++)
            if (wordRaw[i] >= 65 && wordRaw[i] <= 90)
                word.push_back(wordRaw[i]+32);
            else if (wordRaw[i] >= 97 && wordRaw[i] <= 122)
                word.push_back(wordRaw[i]);
        if (word.compare("") != 0) {
            words.insert(word);
            wordsCnt++;
        }
    }
    inputFile.close();
    std::cout << wordsCnt << " words" << std::endl;

    /******************************************************************
    ** selecting words to query
    */
    std::cout << "selecting query words... " << std::endl;
    Array queries = Array();
    for (int i = 0; i < NUMBER_OF_QUERIES; i++)
        queries.insert(words.randomElement());

    /******************************************************************
    ** instantiate structures
    */
    UnsortedArray uArray = UnsortedArray();
    SortedArray sArray = SortedArray();
    Tree simpleTree = Tree();
    AVLTree avlTree = AVLTree();
    HashTable hashTable = HashTable();


    /******************************************************************
    ** inserting in structures
    */
    std::cout << std::endl;
    std::cout << "running insert benchmarks... " << std::endl;
    std::cout << "(inserts/sec)" << std::endl;

    //inserting words to unsorted array
    std::cout << "unsorted array: ";
    start = clock();
    for (int i = 0; i < wordsCnt; i++){
        uArray.insert(words[i]);
    }
    duration = clock()-start;
    std::cout << wordsCnt*CLOCKS_PER_SEC/duration << std::endl;

    //inserting words to sorted array
    std::cout << "sorted array:   ";
    start = clock();
    for (int i = 0; i < wordsCnt; i++)
        sArray.insert(words[i]);
    duration = clock()-start;
    std::cout << wordsCnt*CLOCKS_PER_SEC/duration << std::endl;

    //inserting words to simple binary tree
    std::cout << "simple tree:    ";
    start = clock();
    for (int i = 0; i < wordsCnt; i++)
        simpleTree.insert(words[i]);
    duration = clock()-start;
    std::cout << wordsCnt*CLOCKS_PER_SEC/duration << std::endl;

    //inserting words to avl tree
    std::cout << "avl tree:       ";
    start = clock();
    for (int i = 0; i < wordsCnt; i++) {
        avlTree.insert(words[i]);
    }
    duration = clock()-start;
    std::cout << wordsCnt*CLOCKS_PER_SEC/duration << std::endl;

    //inserting words to hash table
    std::cout << "hash table:     ";
    start = clock();
    for (int i = 0; i < wordsCnt; i++) {
        hashTable.insert(words[i]);
    }
    duration = clock()-start;
    std::cout << wordsCnt*CLOCKS_PER_SEC/duration << std::endl;


    //uArray.print();
    //std::cout << std::endl;
    //sArray.print();
    /******************************************************************
    ** querying structures
    */
    int* results = new int[NUMBER_OF_QUERIES];
    std::cout << std::endl;
    std::cout << "running query benchmarks... " << std::endl;
    std::cout << "(queries/sec)" << std::endl;

    //querying words from unsorted array
    //also keeping results to check if comply with the other structures
    std::cout << "unsorted array: ";
    start = clock();
    for (int i = 0; i < NUMBER_OF_QUERIES; i++) {
        const int r = uArray.contains(queries[i]);
        results[i] = r;
    }
    duration = clock()-start;
    std::cout << NUMBER_OF_QUERIES*CLOCKS_PER_SEC/duration << std::endl;

    //querying words from sorted array
    std::cout << "sorted array:   ";
    start = clock();
    for (int i = 0; i < NUMBER_OF_QUERIES; i++) {
        const int r = sArray.contains(queries[i]);
        if (results[i] != r) {
            //std::cout
                //<< "querying results do not match with the other structures"
                //<< std::endl;

            std::cout<<queries[i]<<" "<<results[i]<<" "<<r<<std::endl;
            //return 1;
        }
    }
    duration = clock()-start;
    std::cout << NUMBER_OF_QUERIES*CLOCKS_PER_SEC/duration << std::endl;

    //querying words from simple binary tree
    std::cout << "simple tree:    ";
    start = clock();
    for (int i = 0; i < NUMBER_OF_QUERIES; i++) {
        const int r = simpleTree.contains(queries[i]);
        if (results[i] != r) {
            std::cout
                << "querying results do not match with the other structures"
                << std::endl;
            return 1;
        }
    }
    //querying words from avl tree
    duration = clock()-start;
    std::cout << NUMBER_OF_QUERIES*CLOCKS_PER_SEC/duration << std::endl;

    std::cout << "avl tree:       ";
    start = clock();
    for (int i = 0; i < NUMBER_OF_QUERIES; i++) {
        const int r = avlTree.contains(queries[i]);
        if (results[i] != r) {
            std::cout
                << "querying results do not match with the other structures"
                << std::endl;
            return 1;
        }
    }
    duration = clock()-start;
    std::cout << NUMBER_OF_QUERIES*CLOCKS_PER_SEC/duration << std::endl;

    //querying words from hash table
    std::cout << "hash table:     ";
    start = clock();
    for (int i = 0; i < NUMBER_OF_QUERIES; i++) {
        const int r = hashTable.contains(queries[i]);
        if (results[i] != r) {
            std::cout
                << "querying results do not match with the other structures"
                << std::endl;
            return 1;
        }
    }
    duration = clock()-start;
    std::cout << NUMBER_OF_QUERIES*CLOCKS_PER_SEC/duration << std::endl;

    return 0;
}
