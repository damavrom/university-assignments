#include <string>
#include "trees.h"

//helper max function
inline int max(int a, int b) {
    if (a > b)
        return a;
    return b;
}

Node::Node(const std::string k){
    this->key = std::string(k);
    this->leftNode = 0;
    this->rightNode = 0;
    this->quantity = 1;
    this->height = 1;
    this->parentNode = 0;
}

void Node::setLeftNode(Node* child){
    this->leftNode = child;
    if (child != 0)
        child->parentNode = this;
}

void Node::setRightNode(Node* child){
    this->rightNode = child;
    if (child != 0)
        child->parentNode = this;
}

void Node::setHeight(){
    int leftHeight = 0;
    int rightHeight = 0;
    if (this->leftNode != 0)
        leftHeight = this->leftNode->height;
    if (this->rightNode != 0)
        rightHeight = this->rightNode->height;
    this->height = max(leftHeight, rightHeight)+1;
}

int Node::getBalance(){
    int leftHeight = 0;
    int rightHeight = 0;
    if (this->leftNode != 0)
        leftHeight = this->leftNode->height;
    if (this->rightNode != 0)
        rightHeight = this->rightNode->height;
    return rightHeight-leftHeight;
}

Tree::Tree() {
    this->head = 0;
    this->size = 0;
}

int Tree::contains(const std::string q) {
    Node* p = this->head;
    while (p != 0) {
        if (p->key.compare(q) < 0)
            p = p->rightNode;
        else if (p->key.compare(q) > 0)
            p = p->leftNode;
        else
            return p->quantity;
    }
    return 0;
}

int Tree::inorderAux(std::string* array, int i, Node* n) {
    if (n == 0)
        return i;
    i = this->inorderAux(array, i, n->leftNode);
    array[i] = std::string(n->key);
    i++;
    i = this->inorderAux(array, i, n->rightNode);
    return i;
}

std::string* Tree::inorder() {
    std::string* r = new std::string[this->size];
    this->inorderAux(r, 0, this->head);
    return r;
}

int Tree::preorderAux(std::string* array, int i, Node* n) {
    if (n == 0)
        return i;
    array[i] = std::string(n->key);
    i++;
    i = this->preorderAux(array, i, n->leftNode);
    i = this->preorderAux(array, i, n->rightNode);
    return i;
}

std::string* Tree::preorder() {
    std::string* r = new std::string[this->size];
    this->preorderAux(r, 0, this->head);
    return r;
}

int Tree::postorderAux(std::string* array, int i, Node* n) {
    if (n == 0)
        return i;
    i = this->postorderAux(array, i, n->leftNode);
    i = this->postorderAux(array, i, n->rightNode);
    array[i] = std::string(n->key);
    i++;
    return i;
}

std::string* Tree::postorder() {
    std::string* r = new std::string[this->size];
    this->postorderAux(r, 0, this->head);
    return r;
}

void Tree::insert(const std::string q) {
    //insert first element ever
    if (this->head == 0) {
        this->head = new Node(q);
        this->size = 1;
        return;
    }
    Node* p = this->head;
    while (1) {
        if (p->key.compare(q) < 0) {
            if (p->rightNode == 0) {
                p->setRightNode(new Node(q));
                this->size++;
                return;
            } else {
                p = p->rightNode;
            }
        } else if (p->key.compare(q) > 0) {
            if (p->leftNode == 0) {
                p->setLeftNode(new Node(q));
                this->size++;
                return;
            } else {
                p = p->leftNode;
            }
        } else {
            p->quantity++;
            return;
        }
    }
}

void Tree::remove(const std::string q) {
    if (this->head == 0)
        return;
    Node* p = this->head;
    Node* parent = 0;
    while (1) {
        if (p->key.compare(q) < 0) {
            if (p->rightNode == 0)
                return;
            else {
                parent = p;
                p = p->rightNode;
            }
        } else if (p->key.compare(q) > 0) {
            if (p->leftNode == 0)
                return;
            else {
                parent = p;
                p = p->leftNode;
            }
        } else if (p->quantity > 1) {
            p->quantity--;
            return;
        } else if (p->leftNode == 0 && p->rightNode == 0) {
            //Node for deletion has no children
            p = parent;
            if (p == 0) {
                delete this->head;
                this->head = 0;
                this->size = 0;
            } else if (p->key.compare(q) < 0) {
                delete p->rightNode;
                p->setRightNode(0);
                this->size--;
            } else if (p->key.compare(q) > 0) {
                delete p->leftNode;
                p->setLeftNode(0);
                this->size--;
            }
            return;
        } else if (p->leftNode == 0) {
            //Node for deletion has only right child
            p = parent;
            if (p == 0) {
                Node* tmp = this->head->rightNode;
                delete this->head;
                this->size = 0;
                this->head = tmp;
            } else if (p->key.compare(q) < 0) {
                Node* tmp = p->rightNode->rightNode;
                delete p->rightNode;
                this->size--;
                p->setRightNode(tmp);
            } else if (p->key.compare(q) > 0) {
                Node* tmp = p->leftNode->rightNode;
                delete p->leftNode;
                this->size--;
                p->setLeftNode(tmp);
            }
            return;
        } else if (p->rightNode == 0) {
            //Node for deletion has only left child
            p = parent;
            if (p == 0) {
                Node* tmp = this->head->leftNode;
                delete this->head;
                this->size = 0;
                this->head = tmp;
            } else if (p->key.compare(q) < 0) {
                Node* tmp = p->rightNode->leftNode;
                delete p->rightNode;
                this->size--;
                p->setRightNode(tmp);
            } else if (p->key.compare(q) > 0) {
                Node* tmp = p->leftNode->leftNode;
                delete p->leftNode;
                this->size--;
                p->setLeftNode(tmp);
            }
            return;
        } else {
            //Node for deletion has two children
            Node* succ = p->rightNode;
            Node* parent = p;
            while (succ->leftNode != 0) {
                parent = succ;
                succ = succ->leftNode;
            }
            p->key = succ->key;
            if (p == parent)
                parent->setRightNode(succ->rightNode);
            else
                parent->setLeftNode(succ->rightNode);
            delete succ;
            this->size--;
            p = parent;
            return;
        }
    }
}

int AVLTree::contains(const std::string q) {
    Node* p = this->head;
    while (p != 0) {
        if (p->key.compare(q) < 0)
            p = p->rightNode;
        else if (p->key.compare(q) > 0)
            p = p->leftNode;
        else
            return p->quantity;
    }
    return 0;
}

void AVLTree::insert(const std::string q) {
    //insert first element ever
    if (this->head == 0) {
        this->head = new Node(q);
        this->size = 1;
        return;
    }

    Node* p = this->head;
    while (1) {
        if (p->key.compare(q) < 0) {
            if (p->rightNode == 0) {
                p->setRightNode(new Node(q));
                this->size++;
                break;
            } else {
                p = p->rightNode;
            }
        } else if (p->key.compare(q) > 0) {
            if (p->leftNode == 0) {
                p->setLeftNode(new Node(q));
                this->size++;
                break;
            } else {
                p = p->leftNode;
            }
        } else {
            //element to be added is found
            p->quantity++;
            return;
        }
    }
    //climb up the tree to check for needed balancing
    while (p != 0) {
        p->setHeight();
        int balance = p->getBalance();
        Node* par = p->parentNode;
        Node* z = p;
        Node* x;
        if (balance == -2 || balance == 2) {
            if (p->key.compare(q) > 0) {
                if (p->leftNode->key.compare(q) > 0) {
                //LL rotation
                    x = z->leftNode;
                    z->setLeftNode(x->rightNode);
                    x->setRightNode(z);
                    z->setHeight();
                    x->setHeight();
                } else if (p->leftNode->key.compare(q) < 0) {
                //LR rotation
                    Node* y = z->leftNode;
                    x = y->rightNode;
                    y->setRightNode(x->leftNode);
                    z->setLeftNode(x->rightNode);
                    x->setLeftNode(y);
                    x->setRightNode(z);
                    z->setHeight();
                    y->setHeight();
                    x->setHeight();
                }
            } else if (p->key.compare(q) < 0) {
                if (p->rightNode->key.compare(q) < 0) {
                //RR rotation
                    x = z->rightNode;
                    z->setRightNode(x->leftNode);
                    x->setLeftNode(z);
                    z->setHeight();
                    x->setHeight();
                } else if (p->rightNode->key.compare(q) > 0) {
                //RL rotation
                    Node* y = z->rightNode;
                    x = y->leftNode;
                    y->setLeftNode(x->rightNode);
                    z->setRightNode(x->leftNode);
                    x->setRightNode(y);
                    x->setLeftNode(z);
                    z->setHeight();
                    y->setHeight();
                    x->setHeight();
                }
            }
        }
        p = par;
        if (balance == 2 || balance == -2) {
            //update pointers of parent node
            if (p == 0) {
                this->head = x;
                x->parentNode = 0;
            } else if (p->key.compare(q) < 0) {
                p->setRightNode(x);
                p->setHeight();
            } else if (p->key.compare(q) > 0) {
                p->setLeftNode(x);
                p->setHeight();
            }
        }
    }
}

void AVLTree::remove(const std::string q) {
    if (this->head == 0)
        //tree is empty; no need to take action
        return;
    Node* p = this->head;
    while (1) {
        if (p->key.compare(q) < 0) {
            if (p->rightNode == 0)
                return;
            else 
                p = p->rightNode;
        } else if (p->key.compare(q) > 0) {
            if (p->leftNode == 0)
                return;
            else
                p = p->leftNode;
        } else if (p->height == 1) {
            //Node for deletion has no children
            p = p->parentNode;
            if (p == 0) {
                delete this->head;
                this->head = 0;
                this->size--;
            } else if (p->key < q) {
                delete p->rightNode;
                p->setRightNode(0);
                this->size--;
            } else if (p->key > q) {
                delete p->leftNode;
                p->setLeftNode(0);
                this->size--;
            }
            break;
        } else if (p->leftNode == 0) {
            //Node for deletion has only right child
            p = p->parentNode;
            if (p == 0) {
                Node* tmp = this->head->rightNode;
                delete this->head;
                this->head = tmp;
                this->head->parentNode = 0;
                this->size--;
            } else if (p->key.compare(q) < 0) {
                Node* tmp = p->rightNode->rightNode;
                delete p->rightNode;
                p->setRightNode(tmp);
                this->size--;
            } else if (p->key.compare(q) > 0) {
                Node* tmp = p->leftNode->rightNode;
                delete p->leftNode;
                p->setLeftNode(tmp);
                this->size--;
            }
            break;
        } else if (p->rightNode == 0) {
            //Node for deletion has only left child
            p = p->parentNode;
            if (p == 0) {
                Node* tmp = this->head->leftNode;
                delete this->head;
                this->head = tmp;
                this->head->parentNode = 0;
                this->size--;
            } else if (p->key.compare(q) < 0) {
                Node* tmp = p->rightNode->leftNode;
                delete p->rightNode;
                p->setRightNode(tmp);
                this->size--;
            } else if (p->key.compare(q) > 0) {
                Node* tmp = p->leftNode->leftNode;
                delete p->leftNode;
                p->setLeftNode(tmp);
                this->size--;
            }
            break;
        } else {
            //Node for deletion has two children
            Node* succ = p->rightNode;
            while (succ->leftNode != 0)
                succ = succ->leftNode;
            p->key = succ->key;
            Node *par = succ->parentNode;
            if (p == par)
                par->setRightNode(succ->rightNode);
            else
                par->setLeftNode(succ->rightNode);
            delete succ;
            this->size--;
            p = par;
            break;
        }
    }
    //climb up the tree to check for needed balancing
    while (p != 0) {
        p->setHeight();
        int balance = p->getBalance();
        Node* par = p->parentNode;
        Node* z = p;
        Node* x;
        if (p->getBalance() == -2) {
            if (p->leftNode->getBalance() <= 0) {
            //LL rotation
                x = z->leftNode;
                z->setLeftNode(x->rightNode);
                x->setRightNode(z);
                z->setHeight();
                x->setHeight();
            } else if (p->leftNode->getBalance() > 0) {
            //LR rotation
                Node* y = z->leftNode;
                x = y->rightNode;
                y->setRightNode(x->leftNode);
                z->setLeftNode(x->rightNode);
                x->setLeftNode(y);
                x->setRightNode(z);
                z->setHeight();
                y->setHeight();
                x->setHeight();
            }
        } else if (p->getBalance() ==  2) {
            if (p->rightNode->getBalance() >= 0) {
            //RR rotation
                x = z->rightNode;
                z->setRightNode(x->leftNode);
                x->setLeftNode(z);
                z->setHeight();
                x->setHeight();
            } else if (p->rightNode->getBalance() < 0) {
            //RL rotation
                Node* y = z->rightNode;
                x = y->leftNode;
                y->setLeftNode(x->rightNode);
                z->setRightNode(x->leftNode);
                x->setRightNode(y);
                x->setLeftNode(z);
                z->setHeight();
                y->setHeight();
                x->setHeight();
            }
        }
        if (balance == 2 || balance == -2) {
            //update pointers of parent node
            if (par == 0) {
                this->head = x;
                x->parentNode = 0;
            } else if (par->rightNode == p) {
                par->setRightNode(x);
                par->setHeight();
            } else if (par->leftNode == p) {
                par->setLeftNode(x);
                par->setHeight();
            }
        }
        p = par;
    }
}
