#include <string>

//for randomElement method
#include <stdlib.h>

#include "arrays.h"

//the initial size of the array
#define STARTING_TABLE_SIZE 2

using namespace std;

Array::Array() {
    this->tableSize = STARTING_TABLE_SIZE;
    this->table = new std::string[this->tableSize];
    this->elements = 0;
}

void Array::resize() {
    const int oldTableSize = this->tableSize;
    const std::string* oldTable = this->table;
    this->tableSize = this->elements*2;
    this->table = new std::string[this->tableSize];
    for (int i = 0; i < this->elements; i++) {
        this->table[i] = oldTable[i];
    }
    delete[] oldTable;
}

void Array::insert(const std::string q) {
    //add element at the end
    this->table[this->elements] = std::string(q);

    //successful insertion must increase the number of elements
    this->elements++;

    //if array is full then resize
    if (this->elements == this->tableSize)
        this->resize();
}

std::string Array::randomElement() {
    return this->table[rand()%this->elements];
}

std::string Array::operator[](const int indx) {
    if (indx >= this->elements || indx < 0)
        return std::string();
    return std::string(this->table[indx]);
}

UnsortedArray::UnsortedArray() {
    this->tableSize = STARTING_TABLE_SIZE;
    this->table = new std::string[this->tableSize];
    this->quantity = new int[this->tableSize];
    this->elements = 0;
}

void UnsortedArray::resize() {
    const int oldTableSize = this->tableSize;
    const int* oldQuantity = this->quantity;
    const std::string* oldTable = this->table;
    this->tableSize = this->elements*2;
    this->table = new std::string[this->tableSize];
    this->quantity = new int[this->tableSize];
    for (int i = 0; i < this->tableSize; i++) {
        this->quantity[i] = 0;
    }
    for (int i = 0; i < this->elements; i++) {
        this->table[i] = oldTable[i];
        this->quantity[i] = oldQuantity[i];
    }
    delete[] oldQuantity;
    delete[] oldTable;
}

void UnsortedArray::insert(const std::string q) {
    for (int i = 0; i < this->elements; i++)
        if (this->table[i].compare(q) == 0) {
            this->quantity[i]++;
            return;
        }
    //add element at the end
    this->table[this->elements] = std::string(q);
    this->quantity[this->elements] = 1;

    //successful insertion must increase the number of elements
    this->elements++;

    //if array is full then resize
    if (this->elements == this->tableSize)
        this->resize();
}

int UnsortedArray::contains(const std::string q) {
    //performs serial search
    for (int i = 0; i < this->elements; i++)
        if (q.compare(this->table[i]) == 0)
            return this->quantity[i];
    return 0;
}

int SortedArray::position(const std::string q) {
    if (this->elements == 0)
        return 0;
    //performs binary search
    int start = 0;
    if (this->table[start].compare(q) >= 0)
        return 0;
    int end = this->elements-1;
    if (this->table[end].compare(q) < 0)
        return end+1;
    while (end-start > 1) {
        const int m = (start+end)/2;
        if (this->table[m].compare(q) < 0) {
            start = m;
        } else if (this->table[m].compare(q) > 0) {
            end = m;
        } else {
            return m;
        }
    }
    return end;
}

void SortedArray::insert(const std::string q) {
    const int p = this->position(q);
    if (this->table[p].compare(q) == 0) {
        this->quantity[p]++;
    } else {
        for (int i = this->elements-1; i >= p; i--) {
            this->table[i+1] = this->table[i];
            this->quantity[i+1] = this->quantity[i];
        }
        this->table[p] = std::string(q);
        this->quantity[p] = 1;
        this->elements++;
        //if array is full then resize
        if (this->elements == this->tableSize)
            this->resize();
    }
}

int SortedArray::contains(const std::string q) {
    const int p = this->position(q);
    if (this->table[p].compare(q) == 0)
        return this->quantity[p];
    else
        return 0;
}
