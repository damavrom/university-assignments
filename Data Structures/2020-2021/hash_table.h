class HashTable {
    private:

    /*
    ** array that contains the elements
    */
    std::string* table;

    /*
    ** array that that contains the quantity of an element
    */
    int* quantity;

    /*
    ** size of the table
    */
    int tableSize;

    /*
    ** number of occupied cells in the table
    */
    int elements;

    /*
    ** hash function, it takes as parameter a string and it returns
    ** a number that falls between 0 and the size of the arrays
    */
    int hf(const std::string s);

    /*
    ** resets the hash table so it can support more insertions
    ** also it doubles its size
    */
    void reorder();

    /*
    ** helper function for inserting
    */
    void insert(const std::string q, const int count);

    public:

    /*
    ** constructor of the hash table
    */
    HashTable();

    /*
    ** returns the number of occurrences of the given string
    */
    int contains(const std::string q);

    /*
    ** inserts a string in the hash table
    */
    void insert(const std::string q);
};
