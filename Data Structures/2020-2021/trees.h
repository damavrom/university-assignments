class Node {
    public:

    /*
    ** key
    */
    std::string key;

    /*
    ** number of occurrences of the key
    */
    int quantity;

    /*
    ** left child
    */
    Node* leftNode;

    /*
    ** right child
    */
    Node* rightNode;

    /*
    ** The parent node
    */
    Node* parentNode;

    /*
    ** The height of this subtree
    */
    int height;

    /*
    ** constructor; it takes a key as a parameter
    */
    Node(const std::string k);

    /*
    ** Setter for left child
    */
    void setLeftNode(Node* p);

    /*
    ** Setter for right child
    */
    void setRightNode(Node* p);

    /*
    ** Setter for height attribute
    */
    void setHeight();

    /*
    ** Getter for balance attribute
    */
    int getBalance();
};

class Tree {
    protected:

    /*
    ** head node
    */
    Node* head;

    
    /*
    ** helper recursive functions of the inorder, preorder and postorder
    ** functions
    */
    int inorderAux(std::string* array, int i, Node* n);
    int preorderAux(std::string* array, int i, Node* n);
    int postorderAux(std::string* array, int i, Node* n);

    /*
    ** amount of nodes
    */
    int size;

    public:

    /*
    ** constructor
    */
    Tree();

    /*
    ** inserts an element
    */
    void insert(const std::string q);

    /*
    ** returns the number of occurrences of an element
    */
    int contains(const std::string q);

    /*
    ** it deletes an element of the tree
    ** if the element doesn't exist in the tree, it doesn't
    ** take any action
    */
    void remove(const std::string q);

    /*
    ** returns an array of all unique elements
    ** in the order: left, root, right
    ** the result is a sorted array
    */
    std::string* inorder();

    /*
    ** returns an array of all unique elements
    ** in the order: root, left, right
    */
    std::string* preorder();

    /*
    ** returns an array of all unique elements
    ** in the order: left, right, root 
    */
    std::string* postorder();
};

class AVLTree: public Tree {
    public:

    /*
    ** overloaded inherited methods
    */
    int contains(const std::string q);
    void insert(const std::string q);
    void remove(const std::string q);
};
