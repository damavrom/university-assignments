#include <iostream>
#include "avl_tree.h"
int max(int a, int b){
    if (a > b)
        return a;
    return b;
}

Node::Node(int k){
    this->key = k;
    this->height = 1;
    this->leftNode = 0;
    this->rightNode = 0;
    this->parentNode = 0;
}

void Node::setHeight(){
    int leftHeight = 0;
    int rightHeight = 0;
    if (this->leftNode != 0)
        leftHeight = this->leftNode->height;
    if (this->rightNode != 0)
        rightHeight = this->rightNode->height;
    this->height = max(leftHeight, rightHeight) + 1;
}

void Node::setLeftNode(Node* child){
    this->leftNode = child;
    if (child != 0)
        child->parentNode = this;
}

void Node::setRightNode(Node* child){
    this->rightNode = child;
    if (child != 0)
        child->parentNode = this;
}

int Node::getBalance(){
    int leftHeight = 0;
    int rightHeight = 0;
    if (this->leftNode != 0)
        leftHeight = this->leftNode->height;
    if (this->rightNode != 0)
        rightHeight = this->rightNode->height;
    return rightHeight - leftHeight;
}

AVLTree::AVLTree(){
    this->head = 0;
    this->size = 0;
}

int AVLTree::search(int q){
    Node* p = this->head;
    while (p != 0){
        if (p->key < q)
            p = p->rightNode;
        else if (p->key > q)
            p = p->leftNode;
        else
            return 1;
    }
    return 0;
}

void AVLTree::insert(int q){
    //insert first element ever
    if (this->head == 0){
        this->head = new Node(q);
        this->size = 1;
        return;
    }
    Node* p = this->head;
    while (1){
        if (p->key < q){
            if (p->rightNode == 0){
                p->setRightNode(new Node(q));
                break;
            }else{ 
                p = p->rightNode;
            }
        }else if (p->key > q){
            if (p->leftNode == 0){
                p->setLeftNode(new Node(q));
                break;
            }else{
                p = p->leftNode;
            }
        }else{
            //element to be added is found
            //thus aborting insertion
            return;
        }
    }
    //successful insertion means increase in size
    this->size++;
    //climb up the tree to check for needed balancing
    while (p != 0){
        p->setHeight();
        int balance = p->getBalance();
        Node* par = p->parentNode;
        Node* z = p;
        Node* x;
        if (balance == -2 || balance == 2){
            if (q < p->key){
                if (q < p->leftNode->key){
                //LL rotation
                    x = z->leftNode;
                    z->setLeftNode(x->rightNode);
                    x->setRightNode(z);
                    z->setHeight();
                    x->setHeight();
                }else if (q > p->leftNode->key){
                //LR rotation
                    Node* y = z->leftNode;
                    x = y->rightNode;
                    y->setRightNode(x->leftNode);
                    z->setLeftNode(x->rightNode);
                    x->setLeftNode(y);
                    x->setRightNode(z);
                    z->setHeight();
                    y->setHeight();
                    x->setHeight();
                }
            }else if (q > p->key){
                if (q > p->rightNode->key){
                //RR rotation
                    x = z->rightNode;
                    z->setRightNode(x->leftNode);
                    x->setLeftNode(z);
                    z->setHeight();
                    x->setHeight();
                }else if (q < p->rightNode->key){
                //RL rotation
                    Node* y = z->rightNode;
                    x = y->leftNode;
                    y->setLeftNode(x->rightNode);
                    z->setRightNode(x->leftNode);
                    x->setRightNode(y);
                    x->setLeftNode(z);
                    z->setHeight();
                    y->setHeight();
                    x->setHeight();
                }
            }
        }
        p = par;
        if (balance == 2 || balance == -2){
            //update pointers of parent node
            if (p == 0){
                this->head = x;
                x->parentNode = 0;
            }else if (p->key < q){
                p->setRightNode(x);
                p->setHeight();
            }else if (p->key > q){
                p->setLeftNode(x);
                p->setHeight();
            }
        }
    }
}

void AVLTree::remove(int q){
    if (this->head == 0)
        //tree is empty; no need to take action
        return;
    Node* p = this->head;
    while (1){
        if (p->key < q){
            if (p->rightNode == 0)
                return;
            else 
                p = p->rightNode;
        }else if (p->key > q){
            if (p->leftNode == 0)
                return;
            else
                p = p->leftNode;
        }else if (p->height == 1){
            //Node for deletion has no children
            p = p->parentNode;
            if (p == 0){
                delete this->head;
                this->head = 0;
            }else if (p->key < q){
                delete p->rightNode;
                p->setRightNode(0);
            }else if (p->key > q){
                delete p->leftNode;
                p->setLeftNode(0);
            }
            break;
        }else if (p->leftNode == 0){
            //Node for deletion has only right child
            p = p->parentNode;
            if (p == 0){
                Node* tmp = this->head->rightNode;
                delete this->head;
                this->head = tmp;
                this->head->parentNode = 0;
            }else if (p->key < q){
                Node* tmp = p->rightNode->rightNode;
                delete p->rightNode;
                p->setRightNode(tmp);
            }else if (p->key > q){
                Node* tmp = p->leftNode->rightNode;
                delete p->leftNode;
                p->setLeftNode(tmp);
            }
            break;
        }else if (p->rightNode == 0){
            //Node for deletion has only left child
            p = p->parentNode;
            if (p == 0){
                Node* tmp = this->head->leftNode;
                delete this->head;
                this->head = tmp;
                this->head->parentNode = 0;
            }else if (p->key < q){
                Node* tmp = p->rightNode->leftNode;
                delete p->rightNode;
                p->setRightNode(tmp);
            }else if (p->key > q){
                Node* tmp = p->leftNode->leftNode;
                delete p->leftNode;
                p->setLeftNode(tmp);
            }
            break;
        }else{
            //Node for deletion has two children
            Node* succ = p->rightNode;
            while (succ->leftNode != 0)
                succ = succ->leftNode;
            p->key = succ->key;
            Node *par = succ->parentNode;
            if (p == par)
                par->setRightNode(succ->rightNode);
            else
                par->setLeftNode(succ->rightNode);
            delete succ;
            p = par;
            break;
        }
    }
    //successful deletion means decrease in size
    this->size--;
    //climb up the tree to check for needed balancing
    while (p != 0){
        p->setHeight();
        int balance = p->getBalance();
        Node* par = p->parentNode;
        Node* z = p;
        Node* x;
        if (p->getBalance() == -2){
            if (p->leftNode->getBalance() <= 0){
            //LL rotation
                x = z->leftNode;
                z->setLeftNode(x->rightNode);
                x->setRightNode(z);
                z->setHeight();
                x->setHeight();
            }else if (p->leftNode->getBalance() > 0){
            //LR rotation
                Node* y = z->leftNode;
                x = y->rightNode;
                y->setRightNode(x->leftNode);
                z->setLeftNode(x->rightNode);
                x->setLeftNode(y);
                x->setRightNode(z);
                z->setHeight();
                y->setHeight();
                x->setHeight();
            }
        }else if (p->getBalance() ==  2){
            if (p->rightNode->getBalance() >= 0){
            //RR rotation
                x = z->rightNode;
                z->setRightNode(x->leftNode);
                x->setLeftNode(z);
                z->setHeight();
                x->setHeight();
            }else if (p->rightNode->getBalance() < 0){
            //RL rotation
                Node* y = z->rightNode;
                x = y->leftNode;
                y->setLeftNode(x->rightNode);
                z->setRightNode(x->leftNode);
                x->setRightNode(y);
                x->setLeftNode(z);
                z->setHeight();
                y->setHeight();
                x->setHeight();
            }
        }
        if (balance == 2 || balance == -2){
            //update pointers of parent node
            if (par == 0){
                this->head = x;
                x->parentNode = 0;
            }else if (par->rightNode == p){
                par->setRightNode(x);
                par->setHeight();
            }else if (par->leftNode == p){
                par->setLeftNode(x);
                par->setHeight();
            }
        }
        p = par;
    }
}

int AVLTree::getSize(){
    return size;
}

int AVLTree::findMin(){
    Node* p = this->head;
    int min = 0;
    while (p != 0){
        min = p->key;
        p = p->leftNode;
    }
    return min;
}
