class Graph{

    /*
    ** This matrix specifies the neighbors
    */
    int** neighbors;

    /*
    ** This array contains every index of every edge ID
    */
    int* edgeIndices;

    /*
    ** The number of edges in the graph
    */
    int edges;

    /*
    ** The number of vertices in the graph
    */
    int vertices;

    /*
    ** It inserts an new edge in the graph, it takes the edge Id as
    ** parameter 
    */
    void insertEdge(int edge);

    /*
    ** It returns the index of and edge, it takes the edge Id as
    ** parameter 
    */
    int getEdgeIndex(int edge);

    /*
    ** Auxiliary method for finding connected components
    */
    void findConnectedAux(int* accounted, int pos);

    public:

    /*
    ** Constructor, it constructs an empty graph
    */
    Graph();

    /*
    ** It computes and returns the total of the spanning trees vertices
    */
    int computeSpanningTree();

    /*
    ** It computes and returns the shortest path between two edges, it 
    ** takes to edges' ID as parameter and it returns -1 if there is no
    ** connection between them
    */
    int computeShortestPath(int edge1, int edge2);

    /*
    ** It computes and returns the number of connected components
    */
    int findConnectedComponents();

    /*
    ** It inserts an vertex into the graph. It takes the edges' IDs as
    ** parameters. If the edges haven't been added to the graph, they 
    ** are added 
    */
    void insert(int edge1, int edge2);

    /*
    ** It deletes an vertex into the graph. It takes the edges' IDs as
    ** parameters. If edges or the vertex is not part of the graph, then
    ** no action is taken
    */
    void remove(int edge1, int edge2);

    /*
    ** It returns the total of the graph vertices
    */
    int getVerticesNum();

    /*
    ** It returns the total of the graph edges
    */
    int getEdgesNum();
};
