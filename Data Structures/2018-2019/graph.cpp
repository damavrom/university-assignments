#include "graph.h"
Graph::Graph(){
    this->neighbors = 0;
    this->edges = 0;
    this->vertices = 0;
    this->edgeIndices = 0;
}

int Graph::getEdgeIndex(int e){
    for (int i = 0; i < this->edges; i++)
        if (this->edgeIndices[i] == e)
            return i;
    return -1;
}

void Graph::insertEdge(int e){
    //add the first edge
    if (this->edgeIndices == 0){
        this->edgeIndices = new int[1];
        this->edgeIndices[0] = e;
        this->neighbors = new int*[1];
        this->neighbors[0] = new int[1];
        this->edges = 1;
        return;
    }

    //check if edge exists, if yes exit
    if (this->getEdgeIndex(e) != -1)
        return;

    //add edge to the indices
    int* oldIndices = this->edgeIndices;
    this->edgeIndices = new int[this->edges+1];
    for (int i = 0; i < this->edges; i++)
        this->edgeIndices[i] = oldIndices[i];
    this->edgeIndices[this->edges] = e;


    //add edge to the neighbors
    int** oldMatrix = this->neighbors;
    this->neighbors = new int*[this->edges+1];
    for (int i = 0; i < this->edges; i++){
       this->neighbors[i] = new int[this->edges+1];
       for (int j = 0; j < this->edges; j++)
           this->neighbors[i][j] = oldMatrix[i][j];
    }

    this->neighbors[this->edges] = new int[this->edges+1];

    for (int i = 0; i < this->edges; i++){
       this->neighbors[this->edges][i] = 0;
       this->neighbors[i][this->edges] = 0;
    }
    this->neighbors[this->edges][this->edges] = 0;

    for (int i = 0; i < this->edges; i++)
        delete oldMatrix[i];
    delete oldMatrix;
    delete oldIndices;

    //if insertion was successful; increment number of edges
    this->edges++;
}

void Graph::insert(int e1, int e2){
    this->insertEdge(e1);
    this->insertEdge(e2);
    int i1 = this->getEdgeIndex(e1);
    int i2 = this->getEdgeIndex(e2);
    if (this->neighbors[i1][i2] == 0)
        if (this->neighbors[i2][i1] == 0){
            //it only inserts if edges are present
            this->neighbors[i1][i2] = 1;
            this->neighbors[i2][i1] = 1;
            this->vertices++;
        }
}

void Graph::remove(int e1, int e2){
    int i1 = this->getEdgeIndex(e1);
    if (i1 == -1)
        return;
    int i2 = this->getEdgeIndex(e2);
    if (i2 == -1)
        return;
    if (this->neighbors[i1][i2] == 1)
        if (this->neighbors[i2][i1] == 1){
            this->neighbors[i1][i2] = 0;
            this->neighbors[i2][i1] = 0;
            this->vertices--;
        }
}

int Graph::computeShortestPath(int start, int end){
    int* dists = new int[this->edges];
    int startIndex = this->getEdgeIndex(start);
    if (startIndex == -1)
        return -1;
    int endIndex = this->getEdgeIndex(end);
    if (endIndex == -1)
        return -1;
    for (int i = 0; i < this->edges; i++){
        dists[i] = this->neighbors[startIndex][i];
    }
    for (int k = 0; k < this->vertices; k++){
        for (int i = 0; i < this->edges; i++){
            if (dists[i] != 0){
                if (i == endIndex)
                    return dists[i];
                for (int j = 0; j < this->edges; j++){
                    if (neighbors[i][j] == 1){
                        if (dists[j] == 0){
                            dists[j] = dists[i]+1;
                        }
                    }
                }
            }
        }
    }
    return -1;
}
void Graph::findConnectedAux(int* accounted, int p){
    for (int i = 0; i < this->edges; i++){
        if (this->neighbors[p][i] == 1){
            if (accounted[i] == 0){
                accounted[i] = 1;
                this->findConnectedAux(accounted, i);
            }
        }
    }
}

int Graph::findConnectedComponents(){
    if (this->edges == 0)
        return 0;
    int* accounted = new int[this->edges];
    for (int i = 0; i < this->edges; i++)
        accounted[i] = 0;
    int cnt = 0;
    for (int i = 0; i < this->edges; i++){
        if (accounted[i] == 0){
            cnt++;
            accounted[i] = 1;
            this->findConnectedAux(accounted, i);
        }
    }
    return cnt;
}

int Graph::computeSpanningTree(){
    return this->edges-this->findConnectedComponents();
}

int Graph::getVerticesNum(){
    return this->vertices;
}

int Graph::getEdgesNum(){
    return this->edges;
}
