#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include <iomanip>

#include "avl_tree.h"
#include "heaps.h"
#include "graph.h"
#include "hash_table.h"

int main(){
    //constructing instances of every structure
    AVLTree avl_tree;
    MinHeap min_heap;
    MaxHeap max_heap;
    HashTable hash;
    Graph graph;
    std::ifstream inputFile("commands.txt");
    std::ofstream outputFile("output.txt");
    while (!inputFile.eof()){
        clock_t start;
        clock_t duration = 0;
        std::string action;
        std::string structure;
        if (inputFile >> action){
            if (action == "BUILD"){
                std::string filename;
                inputFile >> structure >> filename;
                std::ifstream buildFile(filename);
                if (structure == "MINHEAP"){
                    int n;
                    while (!buildFile.eof()){
                        if (buildFile >> n)
                            start = clock();
                            min_heap.insert(n);
                            duration += clock() - start;
                    }
                }else if (structure == "MAXHEAP"){
                    int n;
                    while (!buildFile.eof()){
                        if (buildFile >> n)
                            start = clock();
                            max_heap.insert(n);
                            duration += clock() - start;
                    }
                }else if (structure == "AVLTREE"){
                    int n;
                    while (!buildFile.eof()){
                        if (buildFile >> n)
                            start = clock();
                            avl_tree.insert(n);
                            duration += clock() - start;
                    }
                }else if (structure == "GRAPH"){
                    int n1;
                    int n2;
                    while (!buildFile.eof()){
                        if (buildFile >> n1 >> n2)
                            start = clock();
                            graph.insert(n1, n2);
                            duration += clock() - start;
                    }
                }else if (structure == "HASHTABLE"){
                    int n;
                    while (!buildFile.eof()){
                        if (buildFile >> n)
                            start = clock();
                            hash.insert(n);
                            duration += clock() - start;
                    }
                }
                buildFile.close();
                outputFile << std::setw(10) << "";
            }else if (action == "GETSIZE"){
                inputFile >> structure;
                if (structure == "MINHEAP"){
                    int size;
                    start = clock();
                    size = min_heap.getSize();
                    duration = clock() - start;
                    outputFile << std::setw(10) << size;
                }else if (structure == "MAXHEAP"){
                    int size;
                    start = clock();
                    size = max_heap.getSize();
                    duration = clock() - start;
                    outputFile << std::setw(10) << size;
                }else if (structure == "AVLTREE"){
                    int size;
                    start = clock();
                    size = avl_tree.getSize();
                    duration = clock() - start;
                    outputFile << std::setw(10) << size;
                }else if (structure == "GRAPH"){
                    int edges;
                    int vertices;
                    start = clock();
                    edges = graph.getEdgesNum();
                    vertices = graph.getVerticesNum();
                    duration = clock() - start;
                    outputFile << std::setw(5) << edges;
                    outputFile << std::setw(5) << vertices;
                }else if (structure == "HASHTABLE"){
                    int size;
                    start = clock();
                    size = hash.getSize();
                    duration = clock() - start;
                    outputFile << std::setw(10) << size;
                }
            }else if (action == "FINDMIN"){
                inputFile >> structure;
                if (structure == "MINHEAP"){
                    start = clock();
                    outputFile << std::setw(10) << min_heap.findMin();
                    duration = clock() - start;
                }
                if (structure == "AVLTREE"){
                    start = clock();
                    outputFile << std::setw(10) << avl_tree.findMin();
                    duration = clock() - start;
                }
            }else if (action == "FINDMAX"){
                inputFile >> structure;
                if (structure == "MAXHEAP"){
                    start = clock();
                    outputFile << std::setw(10) << max_heap.findMax();
                    duration = clock() - start;
                }
            }else if (action == "SEARCH"){
                int n;
                inputFile >> structure >> n;
                if (structure == "AVLTREE"){
                    int found;
                    start = clock();
                    found = avl_tree.search(n);
                    duration = clock() - start;
                    if (found == 1)
                        outputFile << std::setw(10) << "SUCCESS";
                    else
                        outputFile << std::setw(10) << "FAILURE";
                }else if (structure == "HASHTABLE"){
                    int found;
                    start = clock();
                    found = hash.search(n);
                    duration = clock() - start;
                    if (found == 1)
                        outputFile << std::setw(10) << "SUCCESS";
                    else
                        outputFile << std::setw(10) << "FAILURE";
                }
            }else if (action == "COMPUTESHORTESTPATH"){
                int n1;
                int n2;
                int shortestPath;
                inputFile >> structure >> n1 >> n2;
                if (structure == "GRAPH"){
                    start = clock();
                    shortestPath = graph.computeShortestPath(n1, n2);
                    duration = clock() - start;
                    outputFile << std::setw(10) << shortestPath;
                }
            }else if (action == "COMPUTESPANNINGTREE"){
                int spanningTree;
                inputFile >> structure;
                if (structure == "GRAPH"){
                    start = clock();
                    spanningTree = graph.computeSpanningTree();
                    duration = clock() - start;
                    outputFile << std::setw(10) << spanningTree;
                }
            }else if (action == "FINDCONNECTEDCOMPONENTS"){
                int connectedComponents;
                inputFile >> structure;
                if (structure == "GRAPH"){
                    start = clock();
                    connectedComponents = graph.findConnectedComponents();
                    duration = clock() - start;
                    outputFile << std::setw(10) << connectedComponents;
                }
            }else if (action == "INSERT"){
                inputFile >> structure;
                if (structure == "MINHEAP"){
                    int n;
                    inputFile >> n;
                    start = clock();
                    min_heap.insert(n);
                    duration = clock() - start;
                }else if (structure == "MAXHEAP"){
                    int n;
                    inputFile >> n;
                    start = clock();
                    max_heap.insert(n);
                    duration = clock() - start;
                }else if (structure == "AVLTREE"){
                    int n;
                    inputFile >> n;
                    start = clock();
                    avl_tree.insert(n);
                    duration = clock() - start;
                }else if (structure == "GRAPH"){
                    int n1;
                    int n2;
                    inputFile >> n1 >> n2;
                    start = clock();
                    graph.insert(n1, n2);
                    duration = clock() - start;
                }else if (structure == "HASHTABLE"){
                    int n;
                    inputFile >> n;
                    start = clock();
                    hash.insert(n);
                    duration = clock() - start;
                }
                outputFile << std::setw(10) << "";
            }else if (action == "DELETEMIN"){
                inputFile >> structure;
                if (structure == "MINHEAP"){
                    start = clock();
                    min_heap.removeMin();
                    duration = clock() - start;
                }
                outputFile << std::setw(10) << "";
            }else if (action == "DELETEMAX"){
                inputFile >> structure;
                if (structure == "MAXHEAP"){
                    start = clock();
                    max_heap.removeMax();
                    duration = clock() - start;
                }
                outputFile << std::setw(10) << "";
            }else if (action == "DELETE"){
                inputFile >> structure;
                if (structure == "AVLTREE"){
                    int n;
                    inputFile >> n;
                    start = clock();
                    avl_tree.remove(n);
                    duration = clock() - start;
                }else if (structure == "GRAPH"){
                    int n1;
                    int n2;
                    inputFile >> n1 >> n2;
                    start = clock();
                    graph.remove(n1, n2);
                    duration = clock() - start;
                }
                outputFile << std::setw(10) << "";
            }
            outputFile << std::fixed << std::setw(10) << 1.0*duration/CLOCKS_PER_SEC;
            outputFile << std::endl;
        }
    }
    inputFile.close();
    outputFile.close();
    return 0;
}
