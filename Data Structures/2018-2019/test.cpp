#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "heaps.h"
using namespace std;
int main(){
    MaxHeap heap = MaxHeap();
    srand(time(0));
    int ins = 100;
    int rang  = ins/10;
    int num = ins/10;
    for (int i = 0; i < num; i++){
        int r1 = rand() % rang;
        int r2 = rand() % rang;
        heap.insert(r2);
        std::cout << "inserted " << r2 << std::endl;
    }
    for (int i = 0; i < num; i++){
        std::cout << "deleted " << heap.findMax() << std::endl;
        heap.removeMax();
    }
//    cout << graph.findConnectedComponents() << endl;
//    cout << graph.getVerticesNum() << endl;
//    cout << graph.getEdgesNum() << endl;
//    for (int i = 0; i < num; i++){
//        int r1 = rand() % rang;
//        int r2 = rand() % rang;
//        graph.remove(r1, r2);
//        //std::cout << "inserted " << r1 << "-" << r2 << std::endl;
//    }
//    cout << graph.findConnectedComponents() << endl;
//    cout << graph.computeSpanningTree() << endl;
//    cout << graph.getVerticesNum() << endl;
//    cout << graph.getEdgesNum() << endl;
    return 0;
}
