// My hash table implementation
class HashTable{
    private:

    /*
    ** The array that contains all of the elements
    */
    int* table;

    /*
    ** This array contains flags whether the cells in the hash table 
    ** have been used once or more in the past. This array serves for
    ** deletion
    */
    int* neverUsed;

    /*
    ** This array contains flags whether or not the cells in the hash
    ** table are currently in use
    */
    int* used;

    /*
    ** The number of elements that are currently in the hash table
    */
    int elements;

    /*
    ** The size of the arrays
    */
    int tableSize;

    /*
    ** The hash function, it takes as parameter a number and it returns
    ** one that falls between 0 and the size of the arrays
    */
    int hf(int n);

    /*
    ** It resets the hash table so it can support more insertions and
    ** deletions. It also unclutters the hash table
    */
    void reorder();

    public:

    /*
    ** The constructor of the hash table
    */
    HashTable();

    /*
    ** It searches for an element in the hash table; it takes an element
    ** as parameter and it returns 1 if element is found in the hash
    ** table, otherwise it returns 0
    */
    int search(int q);

    /*
    ** It inserts an element in the hash table; it takes an element as a
    ** parameter, the hash table doesn't support doubles hence if the
    ** element already exists in the hash table doesn't take any action
    */
    void insert(int q);

    /*
    ** It deletes an element of the hash table; it takes an element as a
    ** parameter. If the element doesn't exist in the hash table, it
    ** doesn't take any action
    */
    void remove(int q);

    /*
    ** It returns the current size of the hash table
    */
    int getSize();
};
