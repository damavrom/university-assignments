#include "hash_table.h"

//The constructor; it creates an empty hash table with initial size 10
HashTable::HashTable(){
    this->tableSize = 10;
    this->table = new int[this->tableSize];
    this->neverUsed = new int[this->tableSize];
    this->used = new int[this->tableSize];
    for (int i = 0; i < tableSize; i++){
        this->neverUsed[i] = 1;
        this->used[i] = 0;
    }
    this->elements = 0;
}

// This function is for resetting the `neverUsed` feature and for
// readjusting the size of the table according to the number of
// elements there exist in the structure
void HashTable::reorder(){
    int oldTableSize = this->tableSize;
    int* oldUsed = this->used;
    int* oldTable = this->table;
    this->tableSize = this->elements * 2;
    this->table = new int[this->tableSize];
    delete this->neverUsed;
    this->neverUsed = new int[this->tableSize];
    this->used = new int[this->tableSize];
    for (int i = 0; i < tableSize; i++){
        this->neverUsed[i] = 1;
        this->used[i] = 0;
    }
    for (int i = 0; i < oldTableSize; i++)
        if (oldUsed[i] == 1)
            this->insert(oldTable[i]);
    delete oldUsed;
    delete oldTable;
}

// An extra ordinary hash function
int HashTable::hf(int n){
    return n % this->tableSize;
}

// The search function
int HashTable::search(int q){
    int startPos = hf(q);
    int p = startPos;
    do{
        if (this->neverUsed[p] == 1)
            return 0;
        if (this->table[p] == q)
            return 1;
        p = (p + 1) % this->tableSize;
    }while (startPos != p);
    this->reorder();
    return 0;
}

// The insert function; it reorders the table if there is need for it
void HashTable::insert(int q){
    int startPos = this->hf(q);
    int p = startPos;
    do{
        if (this->used[p] == 0){
            this->used[p] = 1;
            this->neverUsed[p] = 0;
            this->table[p] = q;
            this->elements++;
            return;
        }
        if (table[p] == q)
            return;
        p = (p + 1) % this->tableSize;
    }while (startPos != p);
    this->reorder();
    this->insert(q);
}

// The delete function; it reorders the table if there is need for it
void HashTable::remove(int q){
    int startPos = this->hf(q);
    int p = startPos;
    do{
        if (this->neverUsed[p] == 1)
            return;
        if (this->table[p] == q){
            this->used[p] = 0;
            this->elements--;
            return;
        }
        p = (p + 1) % this->tableSize;
    }while (startPos != p);
    this->reorder();
}

// This function simply returns the elements that the table contains
// and not the size of the table
int HashTable::getSize(){
    return this->elements;
}
