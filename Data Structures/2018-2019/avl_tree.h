class Node{
    public:

    /*
    ** The height of this subtree
    */
    int height;

    /*
    ** The key of the node
    */
    int key;

    /*
    ** The left child
    */
    Node* leftNode;

    /*
    ** The right child
    */
    Node* rightNode;

    /*
    ** The parent node
    */
    Node* parentNode;

    /*
    ** The constructor; it takes a key as a parameter
    */
    Node(int k);

    /*
    ** Setter for height attribute
    */
    void setHeight();

    /*
    ** Setter for left child
    */
    void setLeftNode(Node* p);

    /*
    ** Setter for right child
    */
    void setRightNode(Node* p);

    /*
    ** Getter for balance attribute
    */
    int getBalance();
};

class AVLTree{
    private:

    /*
    ** Header node of tree
    */
    Node* head;

    /*
    ** The current size of the tree
    */
    int size;

    public:

    /*
    ** The constructor of the tree
    */
    AVLTree();

    /*
    ** It searches for an element in the tree; it takes an element as
    ** parameter and it returns 1 if element is found in the tree,
    ** otherwise it returns 0
    */
    int search(int q);

    /*
    ** It inserts an element in the tree; it takes an element as a
    ** parameter, the tree doesn't support doubles hence if the
    ** element already exists in the tree doesn't take any action
    */
    void insert(int q);

    /*
    ** It deletes an element of the tree; it takes an element as a
    ** parameter. If the element doesn't exist in the tree, it doesn't
    ** take any action
    */
    void remove(int q);

    /*
    ** It finds and returns the minimum element of the tree. Must be
    ** used with caution because if the tree is empty then it returns 0
    */
    int findMin();

    /*
    ** It returns the current size of the tree
    */
    int getSize();
};
