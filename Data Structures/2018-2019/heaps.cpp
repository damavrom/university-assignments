#include <iostream>
#include "heaps.h"
Heap::Heap(){
    //The initial size of the heap is 10
    this->tableSize = 10;
    this->table = new int[this->tableSize];
    this->elements = 0;
}
void Heap::resize(){
    //it resizes the array with elements to have the double of the
    //number of the contained elements
    int oldTableSize = this->tableSize;
    int* oldTable = this->table;
    this->tableSize = this->elements*2;
    this->table = new int[this->tableSize];
    for (int i = 0; i < elements; i++)
        this->table[i] = oldTable[i];
}
void Heap::insert(int q){
    //add element at the end
    this->table[this->elements] = q;
    int i = this->elements;
    //move it towards the beggining if it is smaller/bigger than parrent
    while (i != 0){
        int par = (i-1)/2;
        if (this->com(this->table[par], this->table[i]) == 1){
            break;
        }else{
            int tmp = this->table[par];
            this->table[par] = this->table[i];
            this->table[i] = tmp;
            i = par;
        }
    }
    //successful insertion must increase the number of elements
    this->elements++;
    //if heap is full then resize
    if (this->elements == this->tableSize)
        this->resize();
}
int Heap::getTop(){
    if (this->elements == 0)
        return 0;
    return this->table[0];
}
void Heap::removeTop(){
    //if it is empty, don't take action
    if (this->elements == 0)
        return;
    int i = 0;
    //replace first element with the last
    this->table[0] = this->table[this->elements-1];
    //move it towards the end in order to be for the heap to keap
    //balance
    while (1){
        if (2*i+1 < this->elements){
            if (2*i+2 < this->elements){
                if (this->com(this->table[2*i+1], this->table[2*i+2]) == 1){
                    if (this->com(this->table[2*i+1], this->table[i]) == 1){
                        int tmp = this->table[2*i+1];
                        this->table[2*i+1] = this->table[i];
                        this->table[i] = tmp;
                        i = i*2+1;
                    }else{
                        break;
                    }
                }else{
                    if (this->com(this->table[2*i+2], this->table[i]) == 1){
                        int tmp = this->table[2*i+2];
                        this->table[2*i+2] = this->table[i];
                        this->table[i] = tmp;
                        i = i*2+2;
                    }else{
                        break;
                    }
                }
            }else{
                if (this->com(this->table[2*i+1], this->table[i]) == 1){
                    int tmp = this->table[2*i+1];
                    this->table[2*i+1] = this->table[i];
                    this->table[i] = tmp;
                    i = i*2+1;
                }else{
                    break;
                }
            }
        }else{
            break;
        }
    }
    //successful deletion must decrease the number of elements
    this->elements--;
}
int Heap::getSize(){
    return this->elements;
}
int Heap::com(int a, int b){
    return 0;
}

MinHeap::MinHeap(){
}
int MinHeap::com(int a, int b){
    if (a < b)
        return 1;
    return 0;
}
int MinHeap::findMin(){
    return this->getTop();
}
void MinHeap::removeMin(){
    this->removeTop();
}
MaxHeap::MaxHeap(){
}
int MaxHeap::com(int a, int b){
    if (a > b)
        return 1;
    return 0;
}
int MaxHeap::findMax(){
    return this->getTop();
}
void MaxHeap::removeMax(){
    this->removeTop();
}
