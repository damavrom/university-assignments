class Heap{
    protected:

    /*
    ** The array that contains all of the elements
    */
    int* table;

    /*
    ** The size of the array
    */
    int tableSize;

    /*
    ** The number of elements that are currently in the heap
    */
    int elements;

    /*
    ** It resizes the current array to the double of the number of the
    ** contained elements
    */
    void resize();

    /*
    ** This is an abstract method that compares two numbers
    */
    virtual int com(int a, int b);

    /*
    ** The constructor of the heap
    */
    Heap();

    /*
    ** It returns the first element of the heap
    */
    int getTop();

    /*
    ** It removes the first element of the heap
    */
    void removeTop();

    public:

    /*
    ** It inserts an element in the heap; it takes an element as a
    ** parameter
    */
    void insert(int q);

    /*
    ** Getter of elements contained
    */
    int getSize();
};

class MinHeap: public Heap{

    /*
    ** Compares two numbers, it takes two numbers as parameters and it 
    ** returns 1 if the first is smaller than the latter, it returns 0
    ** otherwise
    */
    int com(int a, int b);

    public:

    /*
    ** The constructor of the minheap
    */
    MinHeap();

    /*
    ** It returns the minimum element of the heap. It must be used with
    ** cation because it will return 0 if the heap is empty
    */
    int findMin();

    /*
    ** It deletes the minimum element of the heap. If the heap is empty
    ** it will not take any action
    */
    void removeMin();
};
class MaxHeap: public Heap{

    /*
    ** Compares two numbers, it takes two numbers as parameters and it 
    ** returns 1 if the first is bigger than the latter, it returns 0
    ** otherwise
    */
    int com(int a, int b);

    public:

    /*
    ** The constructor of the maxheap
    */
    MaxHeap();

    /*
    ** It returns the maximum element of the heap. It must be used with
    ** cation because it will return 0 if the heap is empty
    */
    int findMax();

    /*
    ** It deletes the maximum element of the heap. If the heap is empty
    ** it will not take any action
    */
    void removeMax();

};
