#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <time.h>
#include <stdlib.h>

typedef GLfloat point3[3];
char title[] = "Project 3";
float wright=-100;
float wleft=100;
float wtop=100;
float wbottom=-100;
float camTheta=10;
GLfloat camx=0;
GLfloat camy=70;
GLfloat camz=70;

GLfloat radius=1;
int num=200;
point3 balls[200];
point3 colors[200];
point3 *hist[200];
int kill[200];
int killTime=50;
int histSize=1000;
int age[200];

void initGL()
{
	glClearDepth(1.0f);
	glPointSize(4.0);
	glLineWidth(3.0);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.95, 0.95, 0.95, 1.0);
	glColor3f(0.2, 0.2, 0.2);
}
void genBall()
{
	int nb=random()%1;
	for (int i=0;i<num;i++)
		if (hist[i]==0){
			hist[i]=(point3 *)malloc(sizeof(point3)*histSize);
			for (int j=0;j<histSize;j++)
				for (int k=0;k<3;k++)
					hist[i][j][k]=0;
			age[i]=0;
			kill[i]=killTime;
			balls[i][0]=71+rand()%100;
			balls[i][1]=rand()%100-50;
			balls[i][2]=rand()%100-50;
			for (int k=0;k<3;k++)
				colors[i][k]=1.0*rand()/RAND_MAX;
			printf("generationg ball %.2f %.2f %.2f\n",balls[i][0],balls[i][1],balls[i][2]);
			nb--;
			if (nb<0)
				break;
		}
}
void dellBall()
{
	for (int i=0;i<num;i++)
		if (hist[i]!=0){
			if (kill[i]<0){
				hist[i]=0;
				printf("deleting ball\n");
			}
			if (age[i]==histSize-killTime-1){
				kill[i]--;
			}
			if (kill[i]<killTime)
				kill[i]--;
		}
}
void drawBall()
{
	for (int i=0;i<num;i++)
		if (hist[i]!=0){
			GLfloat r=radius*(1-pow(1-1.0*kill[i]/killTime,10));
			//GLfloat r=radius;
			point3 temp;
			for (int k=0;k<3;k++){
				double sum=0;
				for (int j=0;j<age[i];j++)
					sum+=(age[i]-j)*hist[i][j][k];
				temp[k]=sum+balls[i][k];
			}
			glPushMatrix();
			glTranslatef(temp[0],temp[1],temp[2]);
			//glColor3f(colors[i][0]/(1.0*kill[i]/killTime),colors[i][1]/(1.0*kill[i]/killTime),colors[i][2]/(1.0*kill[i]/killTime));
			glColor3f(colors[i][0],colors[i][1],colors[i][2]);
			glBegin(GL_TRIANGLES);
			int detail=50;
			for (int j=0;j<detail;j++){
				glVertex3f(cos(2*j*M_PI/detail)*r,sin(2*j*M_PI/detail)*r,0);
				glVertex3f(cos(2*(j+1)*M_PI/detail)*r,sin(2*(j+1)*M_PI/detail)*r,0);
				glVertex3f(0,0,0);
			}
			glEnd();
			glColor3f(0.2, 0.2, 0.2);
			glPopMatrix();
		}
}
void updateHist(){
	float jumpFactor=1.7;//+.1*rand()/RAND_MAX;
	for (int i=0;i<num;i++)
		if (hist[i]!=0){
			hist[i][age[i]][0]=-.03;
			hist[i][age[i]][1]=0;
			hist[i][age[i]][2]=0;
			GLfloat r=radius*(1-pow(1-1.0*kill[i]/killTime,10));
			point3 temp;
			double sum=0;
			for (int k=0;k<3;k++){
				sum=0;
				for (int j=0;j<age[i];j++)
					sum+=(age[i]-j)*hist[i][j][k];
				temp[k]=sum+balls[i][k];
			}
			if (temp[1]<=-(61-r)){
				sum=0;
				for (int j=0;j<age[i];j++)
					sum+=hist[i][j][1];
				if (sum<0){
					hist[i][age[i]][1]+=-jumpFactor*sum;
				}
			}
			if (temp[1]>=61-r){
				sum=0;
				for (int j=0;j<age[i];j++)
					sum+=hist[i][j][1];
				if (sum>0){
					hist[i][age[i]][1]+=-jumpFactor*sum;
				}
			}
			if (age[i]==0){
				hist[i][age[i]][1]=2.0*rand()/RAND_MAX-1;
				//hist[i][age[i]][2]=(rand()%10-5)/10.;
			}
			if (temp[0]<=-(61-r)){
				sum=0;
				for (int j=0;j<age[i];j++)
					sum+=hist[i][j][0];
				if (sum<0){
					hist[i][age[i]][0]+=-jumpFactor*sum;
					//hist[i][age[i]][1]=(rand()%10-5)/30.;
					//hist[i][age[i]][2]=(rand()%10-5)/30.;
					if (sum>-.2)
						if (kill[i]==killTime)
							kill[i]--;
				}
			}
		}
}
void updateAge(){
	for (int i=0;i<num;i++)
		if (hist[i]!=0){
			age[i]++;
			//printf("%d ",age[i]);
		}
	//printf("\n");
}
	
void setup3D()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(61.0,-61.0,-61.0,61.0,-201.0, 201.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//camy=70*cos(camTheta*M_PI/40);
	//camz=70*sin(camTheta*M_PI/40);
	camy=0;
	camz=-70;
	gluLookAt(camx,camy,camz, 0.0,0.0,0.0,  1.0,0.0,0.0);
}
void display()
{
	setup3D();
	if (1.0*rand()/RAND_MAX<.1)
	genBall();
	dellBall();
	updateHist();
	drawBall();
	updateAge();
	glutSwapBuffers();
}
void movePOV(int key, int xx, int yy)
{
	switch (key){
		case GLUT_KEY_LEFT:
		camTheta-=1;
		break;
		case GLUT_KEY_RIGHT:
		camTheta+=1;
		break;
	}
	glutPostRedisplay();
}
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(700,700);
	glutInitWindowPosition(10,10);
	glutCreateWindow(title);

        glutDisplayFunc(display); 
        glutIdleFunc(display); 
	glutSpecialFunc(movePOV);
	initGL();
	for (int i=0;i<num;i++){
		hist[i]=0;
	}
	srand(time(0));
	glutMainLoop();
}
