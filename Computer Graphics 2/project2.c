#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <time.h>
#include <stdlib.h>

typedef GLfloat point3[3];
typedef struct treenode{
GLfloat m[16];
void (*f)();
struct treenode *sibling;
struct treenode *child;
} treenode;

char title[] = "Project 2";
float wright=-100;
float wleft=100;
float wtop=100;
float wbottom=-100;
float camTheta=10;

GLfloat camx=60;
GLfloat camy=70;
GLfloat camz=70;

GLfloat speed=15;
GLfloat drawCyl=0;

void initGL(){
	glClearDepth(1.0f);
	glPointSize(6.0);
	glLineWidth(3.0);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.95, 0.95, 0.95, 1.0);
	glColor3f(0.2, 0.2, 0.2);
} 

void setup3D(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(100.0,-100.0,-50.0,150.0,-201.0, 201.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	camy=70*cos(camTheta*M_PI/40);
	camz=70*sin(camTheta*M_PI/40);
	gluLookAt(camx,camy,camz, 0.0,0.0,0.0,  1.0,0.0,0.0);
}

GLfloat torso_length=5;
GLfloat neck_length=2;
GLfloat head_length=3;
GLfloat upper_leg_length=2;
GLfloat lower_leg_length=2;
GLfloat paw_length=.5;
GLfloat margin=0;

GLfloat right_knee_angle=0;
GLfloat idle_torso_angle=0;
GLfloat standing_torso_angle=80;
GLfloat current_torso_angle=0;
GLfloat idle_neck_angle=0;
GLfloat lifted_neck_angle=60;
GLfloat current_neck_angle=0;
GLfloat idle_rufl_angle=0;
GLfloat lifted_rufl_angle=60;
GLfloat current_rufl_angle=0;
GLfloat idle_rlfl_angle=0;
GLfloat lifted_rlfl_angle=120;
GLfloat current_rlfl_angle=0;
GLfloat idle_rfp_angle=0;
GLfloat lifted_rfp_angle=170;
GLfloat current_rfp_angle=0;
GLfloat idle_lufl_angle=0;
GLfloat lifted_lufl_angle=60;
GLfloat current_lufl_angle=0;
GLfloat idle_llfl_angle=0;
GLfloat lifted_llfl_angle=120;
GLfloat current_llfl_angle=0;
GLfloat idle_lfp_angle=0;
GLfloat lifted_lfp_angle=170;
GLfloat current_lfp_angle=0;
GLfloat current_tail_angle=0;
int tail_theta=0;

void torso(){
	if (drawCyl)
		gluCylinder(gluNewQuadric(),.5,.5,torso_length,32,32);
	else{
		glBegin(GL_LINES);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,torso_length);
		glEnd();
	}
}
void neck(){
        glTranslatef(0.0,0.0,margin);
	if (drawCyl)
		gluCylinder(gluNewQuadric(),.5,.5,neck_length,32,32);
	else{
		glBegin(GL_LINES);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,neck_length);
		glEnd();
	}
}
void head(){
        glTranslatef(margin,0.0,0.0);
	if (drawCyl)
		gluCylinder(gluNewQuadric(),.5,0.2,head_length,32,32);
	else{
		glBegin(GL_LINES);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,head_length);
		glEnd();
	}
}
void tail(){
        glTranslatef(0.0,0.0,margin);
        glTranslatef(-1.0,0.0,0.0);
        glBegin(GL_LINE_STRIP);
        for (int i=0;i<20;i++)
                glVertex3f(1*cos(i*M_PI/40),0.0,2*sin(i*M_PI/40));
        glEnd();
}
void upper_leg(){
        glTranslatef(0.0,0.0,margin);
	if (drawCyl)
		gluCylinder(gluNewQuadric(),.5,.5,upper_leg_length,32,32);
	else{
		glBegin(GL_LINES);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,upper_leg_length);
		glEnd();
	}
}
void lower_leg(){
        glTranslatef(0.0,0.0,margin);
	if (drawCyl)
		gluCylinder(gluNewQuadric(),.5,.5,lower_leg_length,32,32);
	else{
		glBegin(GL_LINES);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,lower_leg_length);
		glEnd();
	}
}
void paw(){
        glTranslatef(0.0,0.0,margin);
	if (drawCyl)
		gluCylinder(gluNewQuadric(),.5,.1,paw_length,32,32);
	else{
		glBegin(GL_LINES);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,paw_length);
		glEnd();
	}
}

void drawDogoTree(treenode *n){
        if (n==NULL)
                return;
        glPushMatrix();
        glMultMatrixf(n->m);
        n->f();
        drawDogoTree(n->child);
        glPopMatrix();
        drawDogoTree(n->sibling);
}

void drawDogo(){

        treenode torso_node;
        treenode neck_node;
        treenode head_node;
        treenode tail_node;
        treenode rufl_node; //right upper front leg
        treenode lufl_node;
        treenode rubl_node;
        treenode lubl_node;
        treenode rlfl_node;
        treenode llfl_node;
        treenode rlbl_node;
        treenode llbl_node; //left lower back leg
        treenode rfp_node; //right front paw
        treenode lfp_node;
        treenode rbp_node;
        treenode lbp_node;

	glLoadIdentity();
        glScalef(10.0,10.0,10.0);
        glTranslatef(lower_leg_length+upper_leg_length,0.0,0.0);
        glTranslatef(0.0,0.0,-torso_length/2);
        glRotatef(current_torso_angle,0.0,1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,torso_node.m);
        torso_node.f=torso;
        torso_node.sibling=NULL;
        torso_node.child=&neck_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,torso_length);
        glRotatef(45.0,0.0,1.0,0.0);
        glRotatef(current_neck_angle,0.0,-1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,neck_node.m);
        neck_node.f=neck;
        neck_node.sibling=&rufl_node;
        neck_node.child=&head_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,neck_length);
        glRotatef(75.0,0.0,-1.0,0.0);
        glTranslatef(0.0,0.0,-head_length/3);
        glGetFloatv(GL_MODELVIEW_MATRIX,head_node.m);
        head_node.f=head;
        head_node.sibling=NULL;
        head_node.child=NULL;

	glLoadIdentity();
        glRotatef(current_torso_angle/2,0.0,-1.0,0.0);
        glRotatef(140.0,0.0,-1.0,0.0);
        glRotatef(current_tail_angle,1.0,0.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,tail_node.m);
        tail_node.f=tail;
        tail_node.sibling=NULL;
        tail_node.child=NULL;

	glLoadIdentity();
        glTranslatef(0.0,0.0,torso_length);
        glRotatef(current_rufl_angle,0.0,-1.0,0.0);
        glRotatef(90.0,0.0,-1.0,0.0);
        glRotatef(30.0,1.0,0.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,rufl_node.m);
        rufl_node.f=upper_leg;
        rufl_node.sibling=&lufl_node;
        rufl_node.child=&rlfl_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,upper_leg_length);
        glRotatef(30.0,-1.0,0.0,0.0);
        glRotatef(current_rlfl_angle,0.0,1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,rlfl_node.m);
        rlfl_node.f=lower_leg;
        rlfl_node.sibling=NULL;
        rlfl_node.child=&rfp_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,lower_leg_length);
        glRotatef(90.0,0.0,1.0,0.0);
        glRotatef(current_rfp_angle,0.0,-1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,rfp_node.m);
        rfp_node.f=paw;
        rfp_node.sibling=NULL;
        rfp_node.child=NULL;

	glLoadIdentity();
        glTranslatef(0.0,0.0,torso_length);
        glRotatef(current_lufl_angle,0.0,-1.0,0.0);
        glRotatef(90.0,0.0,-1.0,0.0);
        glRotatef(30.0,-1.0,0.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,lufl_node.m);
        lufl_node.f=upper_leg;
        lufl_node.sibling=&rubl_node;
        lufl_node.child=&llfl_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,upper_leg_length);
        glRotatef(30.0,1.0,0.0,0.0);
        glRotatef(current_llfl_angle,0.0,1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,llfl_node.m);
        llfl_node.f=lower_leg;
        llfl_node.sibling=NULL;
        llfl_node.child=&lfp_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,lower_leg_length);
        glRotatef(90.0,0.0,1.0,0.0);
        glRotatef(current_lfp_angle,0.0,-1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,lfp_node.m);
        lfp_node.f=paw;
        lfp_node.sibling=NULL;
        lfp_node.child=NULL;

	glLoadIdentity();
        glRotatef(current_torso_angle,0.0,-1.0,0.0);
        glRotatef(90.0,0.0,-1.0,0.0);
        glRotatef(30.0,1.0,0.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,rubl_node.m);
        rubl_node.f=upper_leg;
        rubl_node.sibling=&lubl_node;
        rubl_node.child=&rlbl_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,upper_leg_length);
        glRotatef(30.0,-1.0,0.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,rlbl_node.m);
        rlbl_node.f=lower_leg;
        rlbl_node.sibling=NULL;
        rlbl_node.child=&rbp_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,lower_leg_length);
        glRotatef(90.0,0.0,1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,rbp_node.m);
        rbp_node.f=paw;
        rbp_node.sibling=NULL;
        rbp_node.child=NULL;

	glLoadIdentity();
        glRotatef(current_torso_angle,0.0,-1.0,0.0);
        glRotatef(90.0,0.0,-1.0,0.0);
        glRotatef(30.0,-1.0,0.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,lubl_node.m);
        lubl_node.f=upper_leg;
        lubl_node.sibling=&tail_node;
        lubl_node.child=&llbl_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,upper_leg_length);
        glRotatef(30.0,1.0,0.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,llbl_node.m);
        llbl_node.f=lower_leg;
        llbl_node.sibling=NULL;
        llbl_node.child=&lbp_node;

	glLoadIdentity();
        glTranslatef(0.0,0.0,lower_leg_length);
        glRotatef(90.0,0.0,1.0,0.0);
        glGetFloatv(GL_MODELVIEW_MATRIX,lbp_node.m);
        lbp_node.f=paw;
        lbp_node.sibling=NULL;
        lbp_node.child=NULL;

        setup3D();
        drawDogoTree(&torso_node);
        glutSwapBuffers();
}

int order_rufl=0;
int order_rlfl=0;
int order_rfp=0;
int order_lufl=0;
int order_llfl=0;
int order_lfp=0;
int order_torso=0;
int order_neck=0;
int order_tail=1;

void order(){
        int order_all=0;
        if (order_rufl==1){
                order_all=1;
                order_rlfl=1;
                order_rfp=1;
                if (current_rufl_angle>=lifted_rufl_angle){
                        current_rufl_angle=lifted_rufl_angle;
                        order_rufl=0;
                }else{
                        current_rufl_angle+=(lifted_rufl_angle-idle_rufl_angle)/speed;
                }
        }else if (order_rufl==-1){
                order_all=1;
                order_rlfl=-1;
                order_rfp=-1;
                if (current_rufl_angle<=idle_rufl_angle){
                        current_rufl_angle=idle_rufl_angle;
                        order_rufl=0;
                }else{
                        current_rufl_angle-=(lifted_rufl_angle-idle_rufl_angle)/speed;
                }
        }
        if (order_rlfl==1){
                order_all=1;
                if (current_rlfl_angle>=lifted_rlfl_angle){
                        current_rlfl_angle=lifted_rlfl_angle;
                        order_rlfl=0;
                }else{
                        current_rlfl_angle+=(lifted_rlfl_angle-idle_rlfl_angle)/speed;
                }
        }else if (order_rlfl==-1){
                order_all=1;
                if (current_rlfl_angle<=idle_rlfl_angle){
                        current_rlfl_angle=idle_rlfl_angle;
                        order_rlfl=0;
                }else{
                        current_rlfl_angle-=(lifted_rlfl_angle-idle_rlfl_angle)/speed;
                }
        }
        if (order_rfp==1){
                order_all=1;
                if (current_rfp_angle>=lifted_rfp_angle){
                        current_rfp_angle=lifted_rfp_angle;
                        order_rfp=0;
                }else{
                        current_rfp_angle+=(lifted_rfp_angle-idle_rfp_angle)/speed;
                }
        }else if (order_rfp==-1){
                order_all=1;
                if (current_rfp_angle<=idle_rfp_angle){
                        current_rfp_angle=idle_rfp_angle;
                        order_rfp=0;
                }else{
                        current_rfp_angle-=(lifted_rfp_angle-idle_rfp_angle)/speed;
                }
        }
        if (order_lufl==1){
                order_all=1;
                order_llfl=1;
                order_lfp=1;
                if (current_lufl_angle>=lifted_lufl_angle){
                        current_lufl_angle=lifted_lufl_angle;
                        order_lufl=0;
                }else{
                        current_lufl_angle+=(lifted_lufl_angle-idle_lufl_angle)/speed;
                }
        }else if (order_lufl==-1){
                order_all=1;
                order_llfl=-1;
                order_lfp=-1;
                if (current_lufl_angle<=idle_lufl_angle){
                        current_lufl_angle=idle_lufl_angle;
                        order_lufl=0;
                }else{
                        current_lufl_angle-=(lifted_lufl_angle-idle_lufl_angle)/speed;
                }
        }
        if (order_llfl==1){
                order_all=1;
                if (current_llfl_angle>=lifted_llfl_angle){
                        current_llfl_angle=lifted_llfl_angle;
                        order_llfl=0;
                }else{
                        current_llfl_angle+=(lifted_llfl_angle-idle_llfl_angle)/speed;
                }
        }else if (order_llfl==-1){
                order_all=1;
                if (current_llfl_angle<=idle_llfl_angle){
                        current_llfl_angle=idle_llfl_angle;
                        order_llfl=0;
                }else{
                        current_llfl_angle-=(lifted_llfl_angle-idle_llfl_angle)/speed;
                }
        }
        if (order_lfp==1){
                order_all=1;
                if (current_lfp_angle>=lifted_lfp_angle){
                        current_lfp_angle=lifted_lfp_angle;
                        order_lfp=0;
                }else{
                        current_lfp_angle+=(lifted_rfp_angle-idle_lfp_angle)/speed;
                }
        }else if (order_lfp==-1){
                order_all=1;
                if (current_lfp_angle<=idle_lfp_angle){
                        current_lfp_angle=idle_lfp_angle;
                        order_lfp=0;
                }else{
                        current_lfp_angle-=(lifted_lfp_angle-idle_lfp_angle)/speed;
                }
        }
        if (order_neck==1){
                order_all=1;
                order_tail=-1;
                if (current_neck_angle>=lifted_neck_angle){
                        current_neck_angle=lifted_neck_angle;
                        order_neck=0;
                }else{
                        current_neck_angle+=(lifted_neck_angle-idle_neck_angle)/speed;
                }
        }else if (order_neck==-1){
                order_all=1;
                order_tail=1;
                if (current_neck_angle<=idle_neck_angle){
                        current_neck_angle=idle_neck_angle;
                        order_neck=0;
                }else{
                        current_neck_angle-=(lifted_neck_angle-idle_neck_angle)/speed;
                }
        }
        if (order_torso==1){
                order_tail=-1;
                order_rufl=1;
                order_lufl=1;
                order_neck=1;
                order_all=1;
                if (current_torso_angle>=standing_torso_angle){
                        current_torso_angle=standing_torso_angle;
                        order_torso=0;
                }else{
                        current_torso_angle+=(standing_torso_angle-idle_torso_angle)/speed;
                }
        }else if (order_torso==-1){
                order_tail=1;
                order_rufl=-1;
                order_lufl=-1;
                order_neck=-1;
                order_all=1;
                if (current_torso_angle<=idle_torso_angle){
                        current_torso_angle=idle_torso_angle;
                        order_torso=0;
                }else{
                        current_torso_angle-=(standing_torso_angle-idle_torso_angle)/speed;
                }
        }
        if (order_tail==1){
                order_all=1;
                current_tail_angle=60*sin(20*M_PI*tail_theta/200.0);
                tail_theta+=1;
        }else if (order_tail=-1){
                order_all=1;
                if (abs(current_tail_angle)<=0.1){
                        current_tail_angle=0;
                        order_tail=0;
                }else{
                current_tail_angle=60*sin(15*M_PI*tail_theta/200.0);
                tail_theta+=1;
                }
        }
        if (!order_all)
                glutIdleFunc(NULL); 
        glutPostRedisplay();
}

void movePOV(int key, int x, int y){
	switch (key){
		case GLUT_KEY_LEFT:
                        camTheta-=1;
                        break;
		case GLUT_KEY_RIGHT:
                        camTheta+=1;
                        break;
	}
	glutPostRedisplay();
}
int menuId;
void menu(int id){
	switch (id){
                case 1:
                        glutDestroyMenu(menuId);
                        menuId=glutCreateMenu(menu);
                        glutAddMenuEntry("down!", 2);
                        glutAddMenuEntry("exit", 0);
                        glutAttachMenu(GLUT_RIGHT_BUTTON);

                        glutIdleFunc(order); 
                        order_torso=1;
                        break;
                case 2:
                        glutDestroyMenu(menuId);
                        menuId=glutCreateMenu(menu);
                        glutAddMenuEntry("stand!", 1);
                        glutAddMenuEntry("leg up!", 3);
                        glutAddMenuEntry("head down!", 5);
                        glutAddMenuEntry("exit", 0);
                        glutAttachMenu(GLUT_RIGHT_BUTTON);

                        glutIdleFunc(order); 
                        order_torso=-1;
                        break;
                case 3:
                        glutDestroyMenu(menuId);
                        menuId=glutCreateMenu(menu);
                        glutAddMenuEntry("stand!", 1);
                        glutAddMenuEntry("leg down!", 4);
                        glutAddMenuEntry("exit", 0);
                        glutAttachMenu(GLUT_RIGHT_BUTTON);

                        glutIdleFunc(order); 
                        order_rufl=1;
                        break;
                case 4:
                        glutDestroyMenu(menuId);
                        menuId=glutCreateMenu(menu);
                        glutAddMenuEntry("stand!", 1);
                        glutAddMenuEntry("leg up!", 3);
                        glutAddMenuEntry("head down!", 5);
                        glutAddMenuEntry("exit", 0);
                        glutAttachMenu(GLUT_RIGHT_BUTTON);

                        glutIdleFunc(order); 
                        order_rufl=-1;
                        break;
                case 5:
                        glutDestroyMenu(menuId);
                        menuId=glutCreateMenu(menu);
                        glutAddMenuEntry("stand!", 1);
                        glutAddMenuEntry("head up!", 6);
                        glutAddMenuEntry("exit", 0);
                        glutAttachMenu(GLUT_RIGHT_BUTTON);

                        glutIdleFunc(order); 
                        order_neck=1;
                        break;
                case 6:
                        glutDestroyMenu(menuId);
                        menuId=glutCreateMenu(menu);
                        glutAddMenuEntry("stand!", 1);
                        glutAddMenuEntry("leg up!", 3);
                        glutAddMenuEntry("head down!", 5);
                        glutAddMenuEntry("exit", 0);
                        glutAttachMenu(GLUT_RIGHT_BUTTON);

                        glutIdleFunc(order); 
                        order_neck=-1;
                        break;
                case 0:
                        exit(0);
                        break;
	}
}
void click(int b, int s, int x, int y)
{
	if (b==GLUT_LEFT_BUTTON)
		if (s==GLUT_DOWN)
			drawCyl=!drawCyl;
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(700,700);
	glutInitWindowPosition(10,10);
	glutCreateWindow(title);

	menuId=glutCreateMenu(menu);
	glutAddMenuEntry("stand!", 1);
	glutAddMenuEntry("leg up!", 3);
	glutAddMenuEntry("head down!", 5);
	glutAddMenuEntry("exit", 0);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

        glutDisplayFunc(drawDogo); 
        glutMouseFunc(click); 
        glutIdleFunc(order); 
	glutSpecialFunc(movePOV);
	initGL();
	glutMainLoop();
}
