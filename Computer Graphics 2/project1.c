#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <time.h>
#include <stdlib.h>

typedef GLfloat point3[3];
char title[] = "Project 1";
float wright=-100;
float wleft=100;
float wtop=100;
float wbottom=-100;
float d=100;
float grid=100;
float camTheta=10;
GLfloat camx=70;
GLfloat camy=70;
GLfloat camz=70;

int n=0;
point3 *points;
GLfloat *pointToMove;

point3 intCtrlPnts[4];
point3 bezCtrlPnts[7];
point3 bez2CtrlPnts[7];
point3 bez3CtrlPnts[16];
point3 prev4;

void initGL()
{
	glClearDepth(1.0f);
	glPointSize(6.0);
	glLineWidth(3.0);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.95, 0.95, 0.95, 1.0);
	glColor3f(0.2, 0.2, 0.2);

	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}
void initPoints()
{
        srand(time(0));
        for (int i=0;i<4;i++)
        {
                intCtrlPnts[i][0]=rand()%200-100;
                intCtrlPnts[i][1]=rand()%200-100;
                intCtrlPnts[i][2]=0;
        }
        for (int i=0;i<7;i++)
        {
                bezCtrlPnts[i][0]=rand()%200-100;
                bezCtrlPnts[i][1]=rand()%200-100;
                bezCtrlPnts[i][2]=0;
        }
        for (int i=0;i<7;i++)
        {
                bez2CtrlPnts[i][0]=rand()%200-100;
                bez2CtrlPnts[i][1]=rand()%200-100;
                bez2CtrlPnts[i][2]=0;
        }
        bez2CtrlPnts[4][0]=-bez2CtrlPnts[2][0]+2*bez2CtrlPnts[3][0];
        bez2CtrlPnts[4][1]=-bez2CtrlPnts[2][1]+2*bez2CtrlPnts[3][1];
        for (int i=0;i<4;i++)
                for (int j=0;j<4;j++)
                {
                        bez3CtrlPnts[4*i+j][0]=rand()%40-20.0;
                        bez3CtrlPnts[4*i+j][1]=(100.0*j-150.0)/4.0;
                        bez3CtrlPnts[4*i+j][2]=(100.0*i-150.0)/4.0;
                }
}
void setup2D()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(wright, wleft, wbottom, wtop, -100.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0,0.0,-1.0,  0.0,0.0,0.0,  0.0,1.0,0.0);
}
void drawInterpolation()
{
	setup2D();
        point3 bezierCtrlPnts[4];
        for (int i=0;i<4;i++)
        {
                bezierCtrlPnts[0][i]=intCtrlPnts[0][i];
                bezierCtrlPnts[3][i]=intCtrlPnts[3][i];
        }
        bezierCtrlPnts[1][0]=-5.0/6.0*intCtrlPnts[0][0]+3*intCtrlPnts[1][0]-3.0/2.0*intCtrlPnts[2][0]+1.0/3.0*intCtrlPnts[3][0];
        bezierCtrlPnts[1][1]=-5.0/6.0*intCtrlPnts[0][1]+3*intCtrlPnts[1][1]-3.0/2.0*intCtrlPnts[2][1]+1.0/3.0*intCtrlPnts[3][1];
        bezierCtrlPnts[1][2]=-5.0/6.0*intCtrlPnts[0][2]+3*intCtrlPnts[1][2]-3.0/2.0*intCtrlPnts[2][2]+1.0/3.0*intCtrlPnts[3][2];
        bezierCtrlPnts[2][0]=1.0/3.0*intCtrlPnts[0][0]-3.0/2.0*intCtrlPnts[1][0]+3*intCtrlPnts[2][0]-5.0/6.0*intCtrlPnts[3][0];
        bezierCtrlPnts[2][1]=1.0/3.0*intCtrlPnts[0][1]-3.0/2.0*intCtrlPnts[1][1]+3*intCtrlPnts[2][1]-5.0/6.0*intCtrlPnts[3][1];
        bezierCtrlPnts[2][2]=1.0/3.0*intCtrlPnts[0][2]-3.0/2.0*intCtrlPnts[1][2]+3*intCtrlPnts[2][2]-5.0/6.0*intCtrlPnts[3][2];
        glMap1f(GL_MAP1_VERTEX_3,0,1,3,4,bezierCtrlPnts);
        glEnable(GL_MAP1_VERTEX_3);
        glBegin(GL_LINE_STRIP);
        for (int i=0;i<=d;i++)
                glEvalCoord1f(i/d);
        glEnd();
	glColor3f(1.0, 0.2, 0.2);
        glBegin(GL_POINTS);
        for (int i=0;i<4;i++)
                glVertex3fv(intCtrlPnts[i]);
        glEnd();
	glColor3f(0.2, 0.2, 0.2);
	glutSwapBuffers();
}

void drawBezier7()
{
	setup2D();
        bezCtrlPnts[6][0]=bezCtrlPnts[0][0];
        bezCtrlPnts[6][1]=bezCtrlPnts[0][1];
        glMap1f(GL_MAP1_VERTEX_3,0,1,3,7,bezCtrlPnts);
        glEnable(GL_MAP1_VERTEX_3);
        glBegin(GL_LINE_STRIP);
        for (int i=0;i<=d;i++)
                glEvalCoord1f(i/d);
        glEnd();
	glColor3f(1.0, 0.2, 0.2);
        glBegin(GL_POINTS);
        for (int i=0;i<7;i++)
                glVertex3fv(bezCtrlPnts[i]);
        glEnd();
	glColor3f(0.2, 0.2, 0.2);
	glutSwapBuffers();
}
void draw2Bezier()
{
	setup2D();
        if (pointToMove==bez2CtrlPnts[3])
        {
                bez2CtrlPnts[2][0]-=prev4[0]-bez2CtrlPnts[3][0];
                bez2CtrlPnts[2][1]-=prev4[1]-bez2CtrlPnts[3][1];
                bez2CtrlPnts[4][0]-=prev4[0]-bez2CtrlPnts[3][0];
                bez2CtrlPnts[4][1]-=prev4[1]-bez2CtrlPnts[3][1];
        }
        else if (pointToMove==bez2CtrlPnts[2])
        {
                bez2CtrlPnts[4][0]=-bez2CtrlPnts[2][0]+2*bez2CtrlPnts[3][0];
                bez2CtrlPnts[4][1]=-bez2CtrlPnts[2][1]+2*bez2CtrlPnts[3][1];
        }
        else if (pointToMove==bez2CtrlPnts[4])
        {
                bez2CtrlPnts[2][0]=-bez2CtrlPnts[4][0]+2*bez2CtrlPnts[3][0];
                bez2CtrlPnts[2][1]=-bez2CtrlPnts[4][1]+2*bez2CtrlPnts[3][1];
        }
        glMap1f(GL_MAP1_VERTEX_3,0,1,3,4,bez2CtrlPnts);
        glEnable(GL_MAP1_VERTEX_3);
        glBegin(GL_LINE_STRIP);
        for (int i=0;i<=d;i++)
                glEvalCoord1f(i/d);
        glEnd();
        glMap1f(GL_MAP1_VERTEX_3,0,1,3,4,bez2CtrlPnts+3);
        glEnable(GL_MAP1_VERTEX_3);
        glBegin(GL_LINE_STRIP);
        for (int i=0;i<=d;i++)
                glEvalCoord1f(i/d);
        glEnd();
	glColor3f(1.0, 0.2, 0.2);
        glBegin(GL_POINTS);
        for (int i=0;i<7;i++)
                glVertex3fv(bez2CtrlPnts[i]);
        glEnd();
	glColor3f(0.2, 0.2, 0.2);
        prev4[0]=bez2CtrlPnts[3][0];
        prev4[1]=bez2CtrlPnts[3][1];
	glutSwapBuffers();
}
void setup3D()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(61.0,-61.0,-61.0,61.0,-201.0, 201.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	camy=70*cos(camTheta*M_PI/40);
	camz=70*sin(camTheta*M_PI/40);
	gluLookAt(camx,camy,camz, 0.0,0.0,0.0,  1.0,0.0,0.0);
}
void draw3Bezier()
{
	setup3D();
//        glBegin(GL_LINES);
//        glVertex3f(0.0,0.0,0.0);
//        glVertex3f(100.0,0.0,0.0);
//        glVertex3f(0.0,0.0,0.0);
//        glVertex3f(0.0,100.0,0.0);
//        glVertex3f(0.0,0.0,0.0);
//        glVertex3f(0.0,0.0,100.0);
//        glEnd();
	glLineWidth(1.0);
        glMap2f(GL_MAP2_VERTEX_3,0,1,3,4,0,1,12,4,bez3CtrlPnts);
        glEnable(GL_MAP2_VERTEX_3);
        for (int i=0;i<=grid;i++)
        {
                glBegin(GL_LINE_STRIP);
                for (int j=0;j<=grid;j++)
                        glEvalCoord2f(i/grid,j/grid);
                glEnd();
                glBegin(GL_LINE_STRIP);
                for (int j=0;j<=grid;j++)
                        glEvalCoord2f(j/grid,i/grid);
                glEnd();
        }
	glLineWidth(3.0);
	glutSwapBuffers();
}
void movePoint(int x, int y)
{
        pointToMove[0]=wleft+x*(wright-wleft)/glutGet(GLUT_WINDOW_WIDTH);
        pointToMove[1]=-wbottom-y*(wtop-wbottom)/glutGet(GLUT_WINDOW_HEIGHT);
        glutPostRedisplay();
}
void click(int b,int s,int x, int y)
{
	if (b==GLUT_LEFT_BUTTON)
		if (s==GLUT_DOWN)	
                {
                        float xn=wleft+x*(wright-wleft)/glutGet(GLUT_WINDOW_WIDTH);
                        float yn=-wbottom-y*(wtop-wbottom)/glutGet(GLUT_WINDOW_HEIGHT);
                        float minDist=10;
                        int flag=0;
                        for (int i=0;i<n;i++)
                        {
                                if (minDist>pow(xn-points[i][0],2)+pow(yn-points[i][1],2))
                                {
                                        flag=1;
                                        minDist=pow(xn-points[i][0],2)+pow(yn-points[i][1],2);
                                        pointToMove=points[i];
                                }
                        }
                        if (flag)
                                glutMotionFunc(movePoint);
                }
		else if (s==GLUT_UP)	
			glutMotionFunc(NULL);
}
void click2(int b,int s,int x, int y)
{
	if (b==GLUT_LEFT_BUTTON)
		if (s==GLUT_DOWN)	
                        for (int i=0;i<4;i++)
                                for (int j=0;j<4;j++)
                                {
                                        bez3CtrlPnts[4*i+j][0]=rand()%40-20.0;
                                        bez3CtrlPnts[4*i+j][1]=(100.0*j-150.0)/4.0;
                                        bez3CtrlPnts[4*i+j][2]=(100.0*i-150.0)/4.0;
                                }
        glutPostRedisplay();
}
void movePOV(int key, int xx, int yy) {

	switch (key)
	{
		case GLUT_KEY_LEFT:
		camTheta-=1;
		break;
		case GLUT_KEY_RIGHT:
		camTheta+=1;
		break;
	}
	glutPostRedisplay();
}
void menu(int id)
{
	switch (id)
        {
                case 1:
                {
                        glutDisplayFunc(drawInterpolation); 
                        glutMouseFunc(click);
                        glutSpecialFunc(NULL);
                        points=intCtrlPnts;
                        n=4;
                        glutPostRedisplay();
                        break;
                }
                case 2:
                {
                        glutDisplayFunc(drawBezier7); 
                        glutMouseFunc(click);
                        glutSpecialFunc(NULL);
                        points=bezCtrlPnts;
                        n=6;
                        glutPostRedisplay();
                        break;
                }
                case 3:
                {
                        glutDisplayFunc(draw2Bezier); 
                        glutMouseFunc(click);
                        glutSpecialFunc(NULL);
                        points=bez2CtrlPnts;
                        n=7;
                        glutPostRedisplay();
                        break;
                }
                case 4:
                {
                        glutDisplayFunc(draw3Bezier); 
                        glutMouseFunc(click2);
                        glutSpecialFunc(movePOV);
                        glutPostRedisplay();
                        break;
                }
                case 5:
                {
                        exit(0);
                        break;
                }
	}
}
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(700,700);
	glutInitWindowPosition(10,10);
	glutCreateWindow(title);

	glutCreateMenu(menu);
	glutAddMenuEntry("interpolation", 1);
	glutAddMenuEntry("6th degree Bezier", 2);
	glutAddMenuEntry("two 4th degree Bezier", 3);
	glutAddMenuEntry("Bezier surface", 4);
	glutAddMenuEntry("Exit", 5);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
        glutDisplayFunc(draw3Bezier); 
	glutMouseFunc(click2);
	glutSpecialFunc(movePOV);
	initGL();
	initPoints();
	glutMainLoop();
}
