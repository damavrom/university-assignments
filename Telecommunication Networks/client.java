import java.net.*;
import java.io.*;

class client
{
	private static Socket clientSocket;
	private static DataInputStream in;
	private static DataOutputStream out;
	private static int connect()
	{
		try
		{
			clientSocket=new Socket("localhost",8080);
			in = new DataInputStream(clientSocket.getInputStream());
			out = new DataOutputStream(clientSocket.getOutputStream());
		}
		catch (IOException e)
		{
			System.err.println("error");
		}
		return 1;
	}
	public static void main(String args[])
	{
		connect();
		try
		{
			Thread.sleep(10000);
			out.writeUTF("hello");
		}
		catch (InterruptedException e)
		{
			System.err.println("error");
		}
		catch (IOException e)
		{
			System.err.println("error");
		}
	}
}
