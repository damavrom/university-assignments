import java.net.*;
import java.io.*;

class MailClient
{
	private Socket clientSocket;
	private DataInputStream in;
	private DataOutputStream out;
	private final int port;
	private final String URL;
	private Account user;
	private void startingMenu()
	{
		System.out.println("Hello!");
		int choice=0;
		while (true)
		{
			choice=MenuUtils.menu("Log in","Register","Exit");
			if (choice==1)
				logIn();
			else if (choice==2)
				register();
			else if (choice==3)
				break;
		}
		clientSocket.close();
	}
	private void mainMenu()
	{
		int choice=0;
		while (true)
		{
			int choice=MenuUtils.menu("New email","Show inbox","Read Email","Delete email","Log out");
			if (choice==1)
				newMail();
			else if (choice==2)
				showEmails();
			else if (choice==3)
				readEmail();
			else if (choice==4)
				deleteEmail();
			else if (choice==5)
				break;
		}
	}
	private int connect()
	{
		try
		{
			clientSocket=new Socket(URL,port);
			in = new DataInputStream(clientSocket.getInputStream())
			out = new DataOutputStream(clientSocket.getOutputStream())
		}
		catch (IOException e)
		{
			System.err.println("error");
		}
	}
	private void register()
	{
		String username;
		String password;
		do
		{
			username=MenuUtils.prompt("Give new username");
			out.writeUTF("register");
			out.writeUTF(username);
		}
		while (in.readInt())
		password=MenuUtils.prompt("Give new password");
		out.writeUTF(password);
		user=in.readObject();
	}
	private void logIn()
	{
		String username;
		String password;
		do
		{
			username=MenuUtils.prompt("Give your username");
			password=MenuUtils.prompt("Give your password");
			out.writeUTF("log in");
			out.writeUTF(username);
			out.writeUTF(password);
		}
		while (in.readInt())
		user=in.readObject();
	}
	private void newEmail()
	{
		String receiver;
		String subject;
		String bodyText;
		do
		{
			reciever=MenuUtils.prompt("To");
			out.writeUTF("new email");
			out.writeUTF(reciever);
		}
		while (in.readInt())
		subject=MenuUtils.prompt("Subject");
		bodyText=MenuUtils.prompt("Body text");
		out.writeUTF(subject);
		out.writeUTF(bodytext);
	}
	private void updateEmailList()
	{
		out.writeUTF("get emails");
		user.setMailList(in.writeObject());
	}
	private void showEmails()
	{
		int cnt=1;
		for (Email email:user.mailbox)
		{
			System.out.print(cnt);
			System.out.print(email.sender);
			if (email.isNew)
				System.out.print("[New]");
			System.out.println(email.subject);
			cnt++;
		}
	}
	private void readEmail()
	{
		int choice;
		updateEmailList();
		showEmails();
		choice=promtInt("Chose email to read");

		MenuUtils.drawDoubleLine();
		System.out.println("To:");
		System.out.println(sender);
		MenuUtils.drawLine();
		System.out.println("Subject:");
		System.out.println(subject);
		MenuUtils.drawLine();
		System.out.println("Body text:");
		System.out.println(bodyText);
		MenuUtils.drawDoubleLine();
	}
	private void deleteEmail()
	{
		int choice;
		updateEmailList();
		showEmails();
		choice=promtInt("Chose email to delete");
		out.writeUTF("delete email");
		out.writeInt(choice);
	}
	public static void main(String args[])
	{
		URL=args[0];
		port=args[1];
		connect();
		user=new Account("Guest");
		startingMenu();
	}
}
