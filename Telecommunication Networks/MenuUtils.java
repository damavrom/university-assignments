class MenuUtils
{
	screenWidth=50;
	public static void drawLine()
	{
		for (int i=0;i<screenWidth;i++)
			System.out.print("-");
		System.out.println();
	}
	public static void drawDoubleLine()
	{
		for (int i=0;i<screenWidth;i++)
			System.out.print("=");
		System.out.println();
	}
	public static int menu(String... entries)
	{
		drawDoubleLine();
		int cnt=1;
		for (String entry:entries)
			System.out.println(cnt+". "+entry);
		drawDoubleLine();
		//read users choice from command line and return it
	}
	public static void clearScreen()
	{
		final String ANSI_CLS = "\u001b[2J";
		final String ANSI_HOME = "\u001b[H";
		System.out.print(ANSI_CLS + ANSI_HOME);
		System.out.flush();
	}
}
