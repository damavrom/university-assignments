#include "graph.h"
#include <list>
#include <ctime>
int hasCircle(int **T,int n)
{
//needs tiding
	//if graph has less than 2 vertces
	if (n<=2)
		return 0;
	int leaf=-1;
	for (int i=0;i<n;i++) 
	{
		int flag1=0;
		int flag2=0;
		for (int j=0;j<n;j++)
		{
			if (T[i][j])
				flag1=1;
			if (T[j][i])
				flag2=1;
		}
		if ((!flag1)||(!flag2))
		{
			leaf=i;
			break;
		}
	}
	//if no leaf found
	if (leaf==-1)
		return 1;
	//called recrusively whout the first leaf found
	int **auxT=new int*[n-1];
	for (int i=0;i<n-1;i++)
	{
		auxT[i]=new int[n-1]; 
		for (int j=0;j<n-1;j++)
			if (i>=leaf)
				if (j>=leaf)
					auxT[i][j]=T[i+1][j+1];
				else
					auxT[i][j]=T[i+1][j];
			else
				if (j>=leaf)
					auxT[i][j]=T[i][j+1];
				else
					auxT[i][j]=T[i][j];
	}
	return hasCircle(auxT,n-1);
}
graph kruskal(graph G)
{
	int m=G.getM();
	int n=G.getN();
	list<vertex> vertices;
	list<vertex>::iterator it;
	vertex *V=G.getVertices();
	vertex v=V[0];
cout<<V[0].getWeight()<<endl;
	vertices.push_front(v);
cout<<"test\n";
	for (int i=1;i<m;i++)
	{
cout<<i<<endl;
		it=vertices.begin(); 
		while (it->getWeight()<V[i].getWeight()) 
			it++;
		vertices.insert(it,V[i]); 
	}
	int **T=new int*[n];
	for (int i=0;i<n;i++)
	{
		T[i]=new int[n];
		for (int j=0;j<n;j++)
			T[i][j]=0; 
	}
	for (int cnt=0;cnt<n-1&&vertices.empty();cnt++)
	{
		T[vertices.begin()->getFrom()->getID()][vertices.begin()->getTo()->getID()]=vertices.begin()->getWeight();
		T[vertices.begin()->getTo()->getID()][vertices.begin()->getFrom()->getID()]=vertices.begin()->getWeight();
		if (hasCircle(T,n))
		{
			T[vertices.begin()->getFrom()->getID()][vertices.begin()->getTo()->getID()]=0;
			T[vertices.begin()->getTo()->getID()][vertices.begin()->getFrom()->getID()]=0;
		}
		else
			cnt++;
		vertices.pop_front();
	}
	graph graf=graph(T,n,n-1);
	return graf; 
}
//graph prim(graph G)
//{
//	int m=G.getM();
//	int n=G.getN();
//	int **M=G.getMatrix();
//	list<int*> vertices;
//	list<int*>::iterator it;
//	list<int> edges;
//	list<int>::iterator it2;
//	int **T=new int*[n];
//	for (int i=0;i<n;i++)
//	{
//		T[i]=new int[n];
//		for (int j=0;j<n;j++) 
//			T[i][j]=0;
//	}
//	edges.push_front(0);
//	int cnt=0; {
//		for (int i=0;i<n;i++)
//			if (M[*edges.begin()][i])
//			{
//				int* temp=new int[3];	
//				temp[0]=M[*edges.begin()][i];
//				temp[1]=*edges.begin();
//				temp[2]=i;
//				if (vertices.empty())
//					vertices.push_front(temp);
//				else if ((*vertices.end())[0]<temp[0])
//					vertices.push_back(temp);
//				else
//				{
//					it=vertices.begin(); 
//					while ((*it)[0]<temp[0])
//						it++; 
//					if ((*it)[1]!=temp[1]||(*it)[2]!=temp[2])
//						if ((*it)[2]!=temp[1]||(*it)[1]!=temp[2]) 
//							vertices.insert(it,temp);
//				}
//			}
//		while (!vertices.empty()) 
//		{
//			int *flag=new int[2];
//			flag[0]=0;
//			flag[1]=0;
//			for (int i=0;i<2;i++)
//			{
//				for (it2=edges.begin();it2!=edges.end();it2++) 
//					if (*it2==(*vertices.begin())[i+1])
//						flag[i]=1;
//				if ((*edges.end())==(*vertices.begin())[i+1])
//						flag[i]=1;
//			}	
//			if (!(flag[0]&&flag[1]))
//			{
//				if (flag[0])
//					edges.push_front((*vertices.begin())[1]);
//				else
//					edges.push_front((*vertices.begin())[2]);
//				T[(*vertices.begin())[1]][(*vertices.begin())[2]]=(*vertices.begin())[0];
//				T[(*vertices.begin())[2]][(*vertices.begin())[1]]=(*vertices.begin())[0];
//				break;
//			}
//			vertices.pop_front();
//		}
//		cnt++;
//	}
//	while (cnt<n-1&&!vertices.empty());
//	graph graf=graph(T,n,n-1); 
//	return graf; 
//}
//graph boruvka(graph G)
//{
//	int m=G.getM();
//	int n=G.getN();
//	int **M=G.getMatrix();
//	int **T=new int*[n];
//	for (int i=0;i<n;i++)
//	{
//		T[i]=new int[n];
//		for (int j=0;j<nj++)
//			T[i][j]=0;
//	}
//	list<list<int>> trees;
//	list<list<int>>::iterator it;
//	graph graf=graph(T,n,n-1);
//	return graf; 
//}
void compare(graph G) {
	time_t start,end;
	start=time(0);
	kruskal(G);
	end=time(0);
	cout<<"Kruskal's algorhm took "<<end-start<<" seconds to execute\n";
//	start=time(0);
//	prim(G);
//	end=time(0);
//	cout<<"Prim's algorhm took "<<end-start<<" seconds to execute\n";
//	start=time(0);
//	baruvka(G);
//	end=time(0);
//	cout<<"Baruvka's algorhm took "<<end-start<<" seconds to execute\n";
}
