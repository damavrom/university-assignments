#! /bin/bash
n=$(cat dir |wc -l)
sqrt=$(echo "sqrt($n)" |bc)
echo $n 0 > input.txt
cnt=1
for i in $(cat dir)
do
	if [ $i == "l" ]
	then
		j=$[ $cnt - 1 ]
		while [ "$[ $[ $j  ] % $sqrt  ]" -gt 0 ]
		do
			printf "$j " >> input.txt
			j=$[ $j - 1 ]
		done
		echo >> input.txt
	elif [ $i == "r" ]
	then
		j=$[ $cnt + 1 ]
		while [ "$[ $[ $j - 1] % $sqrt ]" -gt 0 ]
		do
			printf "$j " >> input.txt
			j=$[ $j + 1 ]
		done
		echo >> input.txt
	elif [ $i == "u" ]
	then
		j=$[ $cnt - $sqrt ]
		while [ $j -gt 0 ]
		do
			printf "$j " >> input.txt
			j=$[ $j - $sqrt ]
		done
		echo >> input.txt
	elif [ $i == "d" ]
	then
		j=$[ $cnt + $sqrt ]
		while [ $j -lt $[ $n + 1 ] ]
		do
			printf "$j " >> input.txt
			j=$[ $j + $sqrt ]
		done
		echo >> input.txt
	elif [ $i == "lu" ]
	then
		j=$[ $cnt - 1 - $sqrt ]
		while [ "$[ $[ $j  ] % $sqrt  ]" -gt 0 -a $j -gt 0 ]
		do
			printf "$j " >> input.txt
			j=$[ $j - 1 - $sqrt ]
		done
		echo >> input.txt
	elif [ $i == "ld" ]
	then
		j=$[ $cnt - 1 + $sqrt ]
		while [ "$[ $[ $j  ] % $sqrt  ]" -gt 0 -a $j -lt "$[ $n + 1 ]" ]
		do
			printf "$j " >> input.txt
			j=$[ $j - 1 + $sqrt ]
		done
		echo >> input.txt
	elif [ $i == "ru" ]
	then
		j=$[ $cnt + 1 - $sqrt ]
		while [ "$[ $[ $j - 1] % $sqrt ]" -gt 0 -a $j -gt 0 ]
		do
			printf "$j " >> input.txt
			j=$[ $j + 1 - $sqrt ]
		done
		echo >> input.txt
	elif [ $i == "rd" ]
	then
		j=$[ $cnt + 1 + $sqrt ]
		while [ "$[ $[ $j - 1] % $sqrt ]" -gt 0 -a $j -lt "$[ $n + 1 ]" ]
		do
			printf "$j " >> input.txt
			j=$[ $j + 1 + $sqrt ]
		done
		echo >> input.txt
	fi
cnt=$[ $cnt + 1 ]
done
