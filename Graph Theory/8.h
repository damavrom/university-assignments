#include "graph.h"

void testPrint(int ****M,int n,int k)
{
	for (int i=0;i<n;i++)
	{
		for (int j=0;j<n;j++)
			if (M[i][j])
			{
				for (int m=0;m<k;m++)
					cout<<M[i][j][0][m];
				cout<<"\t";
			}
			else
				cout<<"0\t";
		cout<<endl;
	}
}
graph getHam(graph A)
{
	int **M=A.getMatrix();
	int n=A.getN();
	int *****Mj=new int****[n];
	//initiating Mj
	Mj[0]=new int***[n];
	for (int i=0;i<n;i++)
	{
		Mj[0][i]=new int**[n];
		for (int j=0;j<n;j++)
			if (i!=j&&M[i][j])
			{
				Mj[0][i][j]=new int*[2];
				Mj[0][i][j][0]=new int[2];
				Mj[0][i][j][0][0]=i+1;
				Mj[0][i][j][0][1]=j+1;
				Mj[0][i][j][1]=0;
			}
			else
				Mj[0][i][j]=0;
	}
	//initiating M
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++)
			if (M[i][j]&&i!=j)
				M[i][j]=j+1;
			else 
				M[i][j]=-1;

	//calculate Hamiltonian
	for (int j=1;j<n;j++)
	{
		Mj[j]=new int***[n];
		for (int r=0;r<n;r++)
		{
			Mj[j][r]=new int**[n];
			for (int s=0;s<n;s++)
			{
				int cnt=0;
				for (int t=0;t<n;t++)
					if (M[t][s]!=-1 && Mj[j-1][r][t])
						for (int k=0;Mj[j-1][r][t][k];k++)
						{
							int flag=1;
							int a=0;
							if (j==n-1)
								a++;
							for (;a<j+1;a++)
								if (M[t][s]==Mj[j-1][r][t][k][a])
									flag=0;
							if (flag)
								cnt++;
						}
							
				if (cnt)
				{
					Mj[j][r][s]=new int*[cnt];
					cnt=0;
					for (int t=0;t<n;t++)
						if (M[t][s]!=-1 && Mj[j-1][r][t])
							for (int k=0;Mj[j-1][r][t][k];k++)
							{
								int flag=1;
								int a=0;
								if (j==n-1)
									a++;
								for (;a<j+1;a++)
									if (M[t][s]==Mj[j-1][r][t][k][a])
										flag=0;
								if (flag)
								{
									Mj[j][r][s][cnt]=new int[j+2];
									for (int m=0;m<j+1;m++)
										Mj[j][r][s][cnt][m]=Mj[j-1][r][t][k][m];
									Mj[j][r][s][cnt][j+1]=M[t][s];
									cnt++;
								}
							}
					Mj[j][r][s][cnt]=0;
				}
				else
					Mj[j][r][s]=0;
			}
		}

	}
	//return results
	int **R= new int*[n];
	for (int i=0;i<n;i++)
	{
		R[i]=new int[n];
		for (int j=0;j<n;j++)

			R[i][j]=0;
	}
	if (Mj[n-1][0][0])
	{
		for (int k=0;k<n;k++)
			R[ Mj[n-1][0][0][0][k]-1 ][ Mj[n-1][0][0][0][k+1]-1 ]=1;
      	}	
	graph gr=graph(R,n,n);
	return gr;
}
