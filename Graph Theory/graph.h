#include <iostream>
#include <fstream>
using namespace std;
int strToInt(char* ch)
{
	int numb=0;
	for (int i=0;ch[i]!='\0';i++)
	{
		numb*=10;
		numb+=(int)ch[i]-'0';
	}
	return numb;
}
int isBlank(char ch)
{
	if (ch==' '||ch=='\t')
		return 1;
	return 0;
}
class edge
{
	int id;
	int color;
	public:
	edge() 
	{
		id=0;
		color=0;
	}
	edge(int n) 
	{
		id=n;
		color=0;
	}
	edge(int n,int c)
	{
		id=n;
		color=c;
	}
	int getID()
	{
		return id;
	}
	int getColor()
	{
		return color;
	}
};
class vertex
{
	edge *from;
	edge *to;
	int weight;
	public:
	vertex()
	{
		from=0;
		to=0;
		weight=1;
	}
	vertex(edge *a,edge *b)
	{
		from=a;
		to=b;
		weight=1;
	}
	vertex(edge *a,edge *b,int w)
	{
		from=a;
		to=b;
		weight=w;
	}
	edge *getTo()
	{
		return to;
	}
	edge *getFrom()
	{
		return from;
	}
	void setWeight(int w)
	{
		weight=w;
	}
	int getWeight()
	{
		return weight;
	}
	edge *getOtherEdge(edge e)
	{
		if (e.getID()==from->getID()) 
			return to;
		else if (e.getID()==to->getID())
			return from;
		return 0;
	}
};
class graph
{
	int n;
	int m;
	edge *edges;
	vertex *vertices;
	int **adjacencyMatrix;
	void setMatrix()
	{
		adjacencyMatrix=new int*[n];
		for (int i=0;i<n;i++)
		{
			adjacencyMatrix[i]=new int[n];
			for (int j=0;j<n;j++)
				adjacencyMatrix[i][j]=0;	
		}
		for (int i=0;i<m;i++)
		{
			adjacencyMatrix[vertices[i].getFrom()->getID()-1][vertices[i].getTo()->getID()-1]=vertices[i].getWeight();
		}
	}
	public:
	graph()
	{
		ifstream inputFile ("input.txt");
		char ch;
		char *str;
		str = new char[10];		// needs tiding
		do
			inputFile >> noskipws >> ch;
		while (isBlank(ch));
		int i;
		for (i=0;!isBlank(ch)&&ch!='\n';i++)
		{
			str[i]=ch;
			inputFile >> noskipws >> ch;
		}
		str[i]='\0';
		n=strToInt(str);
		str = new char[10];		// needs tiding
		do
			inputFile >> noskipws >> ch;
		while (isBlank(ch));
		for (i=0;!isBlank(ch)&&ch!='\n';i++)
		{
			str[i]=ch;
			inputFile >> noskipws >> ch;
		}
		str[i]='\0';
		m=strToInt(str);
		while (ch!='\n')
			inputFile >> noskipws >> ch;
		edges=new edge[n];
		for (i=0;i<n;i++)
			edges[i]=edge(i+1);
		int cnt=0;
		for (i=0;i<n&&cnt<m;i++)
		{
			int neighbor;
			do
			{
				str = new char[10];
				do
					inputFile >> noskipws >> ch;
				while (isBlank(ch));
				if (ch!='\n'&&ch!='d')
				{
					int k;
					for (k=0;!isBlank(ch)&&ch!='\n';k++)
					{
						str[k]=ch;
						inputFile >> noskipws >> ch;
					}
					str[k]='\0';
					neighbor=strToInt(str)-1;
					vertices[cnt]=vertex(edges+i,edges+neighbor,1);
					cnt++;
				}
				else if (ch=='d')
				{
					int k;
					do
						inputFile >> noskipws >> ch;
					while (isBlank(ch));
					for (k=0;!isBlank(ch)&&ch!='\n';k++)
					{
						str[k]=ch;
						inputFile >> noskipws >> ch;
					}
					str[k]='\0';
					int weight=strToInt(str);
					vertices[cnt-1].setWeight(weight);
				}
			}
			while (ch!='\n');
		}
		inputFile.close();
		setMatrix();
	}
	graph(int **Mat,int N,int M)
	{
		n=N;
		m=M;
		edge *edges=new edge[n];
		for (int i=0;i<n;i++)
			edges[i]=edge(i+1);
		vertices=new vertex[m];
		int cnt=0;
		for (int i=0;i<n;i++)
		{
			for (int j=0;j<n;j++)
				if (Mat[i][j])
				{
					vertices[cnt]=vertex(edges+i,edges+j,Mat[i][j]);
					cnt++;
				}
		}
		setMatrix();
	}
	int **getMatrix()
	{
		int **M=new int*[n];
		for (int i=0;i<n;i++)
		{
			M[i]=new int[n];
			for (int j=0;j<n;j++)
				M[i][j]=adjacencyMatrix[i][j];
		}
		return M;
	}
	int getN()
	{
		return n;
	}
	int getM()
	{
		return m;
	}
	edge *getEdges()
	{
		return edges;
	}
	vertex *getVertices()
	{
		vertex *V=new vertex[n];
		for (int i=0;i<m;i++)
			V[i]=vertices[i];
		return vertices;
	}
	void printToFile()
	{
		cout<<"Writing graph in file"<<endl;
		ofstream outputFile ("output.txt");
		outputFile << n << " " << m << endl;
		for (int i=0;i<n;i++)
		{
			for (int j=0;j<n;j++)
				if (adjacencyMatrix[i][j])
				{
					outputFile << j+1 << " ";
					if (adjacencyMatrix[i][j]!=1)
						outputFile << "-d "<< adjacencyMatrix[i][j] << " ";
				}
			outputFile << endl;
		}
		cout<<"succes\n";
	}
	void printMatrix()
	{
		for (int i=0;i<n;i++)
		{
			for (int j=0;j<n;j++)
				cout<<adjacencyMatrix[i][j]<<" ";
			cout<<endl;
		}
	}	
};

