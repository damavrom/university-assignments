#include <iostream>
#include <fstream>
using namespace std;

int strToInt(char* ch)
{
	int numb=0;
	for (int i=0;ch[i]!='\0';i++)
	{
		numb*=10;
		numb+=(int)ch[i]-'0';
	}
	return numb;
}
int isBlank(char ch)
{
	if (ch==' '||ch=='\t')
		return 1;
	return 0;
}
class graph
{
	int **adjacencyMatrix;
	int n;
	int m;
	public:
	graph()
	{
		ifstream inputFile ("input.txt");
		char ch;
		char *str;
		
		str = new char[10];		// needs tiding
		do
			inputFile >> noskipws >> ch;
		while (isBlank(ch));
		int i;
		for (i=0;!isBlank(ch)&&ch!='\n';i++)
		{
			str[i]=ch;
			inputFile >> noskipws >> ch;
		}
		str[i]='\0';
		n=strToInt(str);
		
		str = new char[10];		// needs tiding
		do
			inputFile >> noskipws >> ch;
		while (isBlank(ch));
		for (i=0;!isBlank(ch)&&ch!='\n';i++)
		{
			str[i]=ch;
			inputFile >> noskipws >> ch;
		}
		str[i]='\0';
		m=strToInt(str);
		while (ch!='\n')
			inputFile >> noskipws >> ch;
		adjacencyMatrix=new int*[n];
		for (i=0;i<n;i++)
		{
			adjacencyMatrix[i]=new int[n];
			for (int j=0;j<n;j++)
				adjacencyMatrix[i][j]=0;
			do
			{
				str = new char[10];
				do
					inputFile >> noskipws >> ch;
				while (isBlank(ch));
				if (ch!='\n')
				{
					int k;
					for (k=0;!isBlank(ch)&&ch!='\n';k++)
					{
						str[k]=ch;
						inputFile >> noskipws >> ch;
					}
					str[k]='\0';
					int neighbor=strToInt(str)-1;
					if (neighbor<0||neighbor>=n)
						cout << "invalide value "<< (char)(neighbor+1+'0') << endl << "ignored" << endl;
					else
						adjacencyMatrix[i][neighbor]=1;
				}
			}
			while (ch!='\n');
		}
		inputFile.close();
	}
	graph(int **Mat,int N,int M)
	{
		n=N;
		m=M;
		adjacencyMatrix=new int*[n];
		for (int i=0;i<n;i++)
		{
			adjacencyMatrix[i]=new int[n];
			for (int j=0;j<n;j++)
				adjacencyMatrix[i][j]=Mat[i][j];
		}
	}
	int **getMatrix()
	{
		int **M=new int*[n];
		for (int i=0;i<n;i++)
		{
			M[i]=new int[n];
			for (int j=0;j<n;j++)
				M[i][j]=adjacencyMatrix[i][j];
		}
		return M;
	}
	int getN()
	{
		return n;
	}
	void printToFile()
	{
		cout<<"Writing graph in file"<<endl;
		ofstream outputFile ("output.txt");
		outputFile << n << " " << m << endl;
		for (int i=0;i<n;i++)
		{
			for (int j=0;j<n;j++)
				if (adjacencyMatrix[i][j])
					outputFile << j+1 << " ";
			outputFile << endl;
		}
		cout<<"succes\n";
	}
	void printMatrix()
	{
		for (int i=0;i<n;i++)
		{
			for (int j=0;j<n;j++)
				cout<<adjacencyMatrix[i][j]<<" ";
			cout<<endl;
		}
	}	
};

