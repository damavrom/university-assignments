;Students: 
;Mavromatakis Dimitris 2215
;Anastasiadis Loukas 2333

;The static knowlegde is left as it was from my first assignment (Dimitris Mavromatakis).
;We tried to sovle the problem so it will have the least control structures as posible.
;Therefore we only used rules and facts for the solution without any if-then and while-do structures.
;The solution can be applied to any well defined circuit and not just the required one.
;The code for the second project beggins at line 735

(defclass Circuit+Measurement "It is all the measurements of a circuit in a measurement circle."
	(is-a USER)
	(role concrete)
	(single-slot circuit
;+		(comment "It's the circuit that the measurment refers to.")
		(type INSTANCE)
		(allowed-classes Complete+Circuit)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot measurements
;+		(comment "It's the collection of measurments of the gates that make a circuit.")
		(type INSTANCE)
		(allowed-classes Measurement)
		(create-accessor read-write))
	(single-slot number
;+		(comment "It's the identification number of the measurement circle.")
		(type INTEGER)
		(range 1 ?VARIABLE)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Measurement "This is the measurement of a sensor or an input."
	(is-a USER)
	(role concrete)
	(single-slot element
;+		(comment "It's the element that is measured, It can be input or a sensor.")
		(type INSTANCE)
		(allowed-classes Input Sensor)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot value
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Input "It functions as the input of the circuit"
	(is-a USER)
	(role concrete))

(defclass Complete+Circuit "It is the complete circuit made of gates and inputs."
	(is-a USER)
	(role concrete)
	(multislot gates
;+		(comment "It's the collection of gates and sensors a circuit can have.")
		(type INSTANCE)
		(allowed-classes Gate)
		(create-accessor read-write))
	(multislot inputs
;+		(comment "it's the collection of the inputs a circuit can have.")
		(type INSTANCE)
		(allowed-classes Input)
		(create-accessor read-write)))

(defclass Gate "This is the abstract class of gate, something that takes an input and gives an output, it can be adder, multiplier, sensor or even gates that are not yet implemented, such us OR, AND..."
	(is-a USER)
	(role abstract)
	(single-slot state
;+		(comment "It is the state of the gate, it can take two values. Normal or malfunction.")
		(type SYMBOL)
		(allowed-values Normal Malfunction)
		(default Normal)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot gateInput
;+		(comment "It's the gate or the input instant that passes it's value to this instant.")
		(type INSTANCE)
		(allowed-classes Gate Input)
		(create-accessor read-write)))

(defclass Adder "This is the adder gate that takes inputs from gateInput attributes and gives their sum in modulo."
	(is-a Gate)
	(role concrete)
        (pattern-match reactive)
	(single-slot malfunction
;+		(comment "It's the time of malfunction an adder or a multiplier can have. It can take three values. None, ShortCircuit and NoImportantDigit")
		(type SYMBOL)
		(allowed-values NoImportantDigit ShortCircuit None)
		(default None)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Multiplier "This is the multiplier gate that takes inputs from gateInput attributes and gives them multiplied by each other in modulo."
	(is-a Gate)
	(role concrete)
        (pattern-match reactive)
	(single-slot malfunction
;+		(comment "It's the time of malfunction an adder or a multiplier can have. It can take three values. None, ShortCircuit and NoImportantDigit")
		(type SYMBOL)
		(allowed-values NoImportantDigit ShortCircuit None)
		(default None)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Sensor "This is the sensor that it displays it's input if it funtions normally."
	(is-a Gate)
        (pattern-match reactive)
	(role concrete))

; Sat Dec 08 13:04:09 EET 2018
; 
;+ (version "3.5")
;+ (build "Build 663")

(definstances facts 
        ([A1] of  Adder

                (gateInput
                        [input1]
                        [input1])
                (malfunction None)
                (state Normal))

        ([A2] of  Adder

                (gateInput
                        [S2]
                        [S3])
                (malfunction None)
                (state Normal))

        ([input1] of  Input
        )

        ([input1-1] of  Measurement

                (element [input1])
                (value 21))

        ([input1-10] of  Measurement

                (element [input1])
                (value 6))

        ([input1-2] of  Measurement

                (element [input1])
                (value 7))

        ([input1-3] of  Measurement

                (element [input1])
                (value 11))

        ([input1-4] of  Measurement

                (element [input1])
                (value 18))

        ([input1-5] of  Measurement

                (element [input1])
                (value 25))

        ([input1-6] of  Measurement

                (element [input1])
                (value 12))

        ([input1-7] of  Measurement

                (element [input1])
                (value 1))

        ([input1-8] of  Measurement

                (element [input1])
                (value 0))

        ([input1-9] of  Measurement

                (element [input1])
                (value 31))

        ([input2] of  Input
        )

        ([input2-1] of  Measurement

                (element [input2])
                (value 28))

        ([input2-10] of  Measurement

                (element [input2])
                (value 4))

        ([input2-2] of  Measurement

                (element [input2])
                (value 25))

        ([input2-3] of  Measurement

                (element [input2])
                (value 17))

        ([input2-4] of  Measurement

                (element [input2])
                (value 11))

        ([input2-5] of  Measurement

                (element [input2])
                (value 24))

        ([input2-6] of  Measurement

                (element [input2])
                (value 19))

        ([input2-7] of  Measurement

                (element [input2])
                (value 31))

        ([input2-8] of  Measurement

                (element [input2])
                (value 31))

        ([input2-9] of  Measurement

                (element [input2])
                (value 1))

        ([input3] of  Input
        )

        ([input3-1] of  Measurement

                (element [input3])
                (value 10))

        ([input3-10] of  Measurement

                (element [input3])
                (value 25))

        ([input3-2] of  Measurement

                (element [input3])
                (value 13))

        ([input3-3] of  Measurement

                (element [input3])
                (value 24))

        ([input3-4] of  Measurement

                (element [input3])
                (value 28))

        ([input3-5] of  Measurement

                (element [input3])
                (value 30))

        ([input3-6] of  Measurement

                (element [input3])
                (value 11))

        ([input3-7] of  Measurement

                (element [input3])
                (value 7))

        ([input3-8] of  Measurement

                (element [input3])
                (value 3))

        ([input3-9] of  Measurement

                (element [input3])
                (value 6))

        ([input4] of  Input
        )

        ([input4-1] of  Measurement

                (element [input4])
                (value 25))

        ([input4-10] of  Measurement

                (element [input4])
                (value 12))

        ([input4-2] of  Measurement

                (element [input4])
                (value 15))

        ([input4-3] of  Measurement

                (element [input4])
                (value 31))

        ([input4-4] of  Measurement

                (element [input4])
                (value 21))

        ([input4-5] of  Measurement

                (element [input4])
                (value 10))

        ([input4-6] of  Measurement

                (element [input4])
                (value 19))

        ([input4-7] of  Measurement

                (element [input4])
                (value 22))

        ([input4-8] of  Measurement

                (element [input4])
                (value 23))

        ([input4-9] of  Measurement

                (element [input4])
                (value 8))

        ([M1] of  Multiplier

                (gateInput
                        [S1]
                        [input2])
                (malfunction None)
                (state Normal))

        ([M2] of  Multiplier

                (gateInput
                        [input3]
                        [input4])
                (malfunction None)
                (state Normal))

        ([measurement1] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-1]
                        [input2-1]
                        [input3-1]
                        [input4-1]
                        [OUT-1]
                        [S1-1]
                        [S2-1]
                        [S3-1])
                (number 1))

        ([measurement10] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-10]
                        [input2-10]
                        [input3-10]
                        [input4-10]
                        [OUT-10]
                        [S1-10]
                        [S2-10]
                        [S3-10])
                (number 10))

        ([measurement2] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-2]
                        [input2-2]
                        [input3-2]
                        [input4-2]
                        [OUT-2]
                        [S1-2]
                        [S2-2]
                        [S3-2])
                (number 2))

        ([measurement3] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-3]
                        [input2-3]
                        [input3-3]
                        [input4-3]
                        [OUT-3]
                        [S1-3]
                        [S2-3]
                        [S3-3])
                (number 3))

        ([measurement4] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-4]
                        [input2-4]
                        [input3-4]
                        [input4-4]
                        [OUT-4]
                        [S1-4]
                        [S2-4]
                        [S3-4])
                (number 4))

        ([measurement5] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-5]
                        [input2-5]
                        [input3-5]
                        [input4-5]
                        [OUT-5]
                        [S1-5]
                        [S2-5]
                        [S3-5])
                (number 5))

        ([measurement6] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-6]
                        [input2-6]
                        [input3-6]
                        [input4-6]
                        [OUT-6]
                        [S1-6]
                        [S2-6]
                        [S3-6])
                (number 6))

        ([measurement7] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-7]
                        [input2-7]
                        [input3-7]
                        [input4-7]
                        [OUT-7]
                        [S1-7]
                        [S2-7]
                        [S3-7])
                (number 7))

        ([measurement8] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-8]
                        [input2-8]
                        [input3-8]
                        [input4-8]
                        [OUT-8]
                        [S1-8]
                        [S2-8]
                        [S3-8])
                (number 8))

        ([measurement9] of  Circuit+Measurement

                (circuit [projectCircuit])
                (measurements
                        [input1-9]
                        [input2-9]
                        [input3-9]
                        [input4-9]
                        [OUT-9]
                        [S1-9]
                        [S2-9]
                        [S3-9])
                (number 9))

        ([OUT] of  Sensor

                (gateInput [A2])
                (state Normal))

        ([OUT-1] of  Measurement

                (element [OUT])
                (value 18))

        ([OUT-10] of  Measurement

                (element [OUT])
                (value 28))

        ([OUT-2] of  Measurement

                (element [OUT])
                (value 3))

        ([OUT-3] of  Measurement

                (element [OUT])
                (value 14))

        ([OUT-4] of  Measurement

                (element [OUT])
                (value 0))

        ([OUT-5] of  Measurement

                (element [OUT])
                (value 12))

        ([OUT-6] of  Measurement

                (element [OUT])
                (value 9))

        ([OUT-7] of  Measurement

                (element [OUT])
                (value 26))

        ([OUT-8] of  Measurement

                (element [OUT])
                (value 0))

        ([OUT-9] of  Measurement

                (element [OUT])
                (value 30))

        ([projectCircuit] of  Complete+Circuit

                (gates
                        [A1]
                        [A2]
                        [M1]
                        [M2]
                        [OUT]
                        [S1]
                        [S2]
                        [S3])
                (inputs
                        [input1]
                        [input2]
                        [input3]
                        [input4]))

        ([S1] of  Sensor

                (gateInput [A1])
                (state Normal))

        ([S1-1] of  Measurement

                (element [S1])
                (value 10))

        ([S1-10] of  Measurement

                (element [S1])
                (value 12))

        ([S1-2] of  Measurement

                (element [S1])
                (value 0))

        ([S1-3] of  Measurement

                (element [S1])
                (value 22))

        ([S1-4] of  Measurement

                (element [S1])
                (value 4))

        ([S1-5] of  Measurement

                (element [S1])
                (value 18))

        ([S1-6] of  Measurement

                (element [S1])
                (value 8))

        ([S1-7] of  Measurement

                (element [S1])
                (value 2))

        ([S1-8] of  Measurement

                (element [S1])
                (value 0))

        ([S1-9] of  Measurement

                (element [S1])
                (value 30))

        ([S2] of  Sensor

                (gateInput [M1])
                (state Normal))

        ([S2-1] of  Measurement

                (element [S2])
                (value 24))

        ([S2-10] of  Measurement

                (element [S2])
                (value 31))

        ([S2-2] of  Measurement

                (element [S2])
                (value 0))

        ([S2-3] of  Measurement

                (element [S2])
                (value 6))

        ([S2-4] of  Measurement

                (element [S2])
                (value 12))

        ([S2-5] of  Measurement

                (element [S2])
                (value 16))

        ([S2-6] of  Measurement

                (element [S2])
                (value 24))

        ([S2-7] of  Measurement

                (element [S2])
                (value 0))

        ([S2-8] of  Measurement

                (element [S2])
                (value 0))

        ([S2-9] of  Measurement

                (element [S2])
                (value 30))

        ([S3] of  Sensor

                (gateInput [M2])
                (state Normal))

        ([S3-1] of  Measurement

                (element [S3])
                (value 26))

        ([S3-10] of  Measurement

                (element [S3])
                (value 12))

        ([S3-2] of  Measurement

                (element [S3])
                (value 3))

        ([S3-3] of  Measurement

                (element [S3])
                (value 8))

        ([S3-4] of  Measurement

                (element [S3])
                (value 12))

        ([S3-5] of  Measurement

                (element [S3])
                (value 12))

        ([S3-6] of  Measurement

                (element [S3])
                (value 17))

        ([S3-7] of  Measurement

                (element [S3])
                (value 26))

        ([S3-8] of  Measurement

                (element [S3])
                (value 0))

        ([S3-9] of  Measurement

                (element [S3])
                (value 0))
)

;These are facts that we need to be true at the beggining
;the print-next fact makes use in the order that the information will be printed
(deffacts init (print-next 1))

;We couldn't arrange to access multiple objects with a sigle rule
;so we needed to constract facts that would look like (measurement <circle number> <elemend of reference> <input values> <output>)
;this is data from mutliple objects that is necessary for investigating the state of every gate
;the four following rules play this role
(defrule find-num-of-circle
        (object (is-a Circuit+Measurement)
                (number ?c)
                (measurements $?left ?m $?right))
        =>
        (assert (measurement ?c ?m)))
(defrule find-measure-element
        ?m <- (measurement ?c ?nam)
        (object (is-a Measurement)
                (name ?nam)
                (element ?el)
                (value ?val))
        =>
        (assert (measurement ?c ?el ?val))
        (retract ?m))
(defrule find-measure-gate
        (measurement ?c ?el ?val)
        (object (is-a Sensor)
                (name ?el)
                (gateInput ?el2))
        =>
        ;The to-check fact indicates which element must be checked for malfuntions
        ;first we check the gates and then the sensors
        (assert (to-check ?c ?el2)) 
        (assert (measurement ?c ?el2 ?val)))
(defrule find-measure-gate-inputs
        ?m <- (measurement ?c ?el ?val)
        (object (is-a Gate)
                (name ?el)
                (gateInput ?el1 ?el2))
        (measurement ?c ?el1 ?in1)
        (measurement ?c ?el2 ?in2)
        =>
        (assert (measurement ?c ?el ?in1 ?in2 ?val))
        (retract ?m))

;The four following rules investigate if a multiplier or adder is short circuited or if the important bit is off
;if they are, then it needs to be printend, the fact (circle ...) will trigget the rule that prints
;everytime a rule of these is triggered, the to-check fact is deleted
(defrule find-short-adder
        (measurement ?c ?el ?in1 ?in2 0)
        ?m <- (to-check ?c ?el)
        (object (is-a Adder)
                (name ?el))
        (test (<> (mod (+ ?in1 ?in2) 32) 0))
        =>
        (assert (circle ?c short Adder ?el))
        (retract ?m))
(defrule find-short-mult
        (measurement ?c ?el ?in1 ?in2 0)
        ?m <- (to-check ?c ?el)
        (object (is-a Multiplier)
                (name ?el))
        (test (<> (mod (* ?in1 ?in2) 32) 0))
        =>
        (assert (circle ?c short Multiplier ?el))
        (retract ?m))
(defrule find-no-sign-adder
        (measurement ?c ?el ?in1 ?in2 ?out)
        ?m <- (to-check ?c ?el)
        (object (is-a Adder)
                (name ?el))
        (test (= (mod (+ ?in1 ?in2) 16) ?out))
        (test (<> (mod (+ ?in1 ?in2) 32) ?out))
        =>
        (assert (circle ?c no-sign Adder ?el))
        (retract ?m))
(defrule find-no-sign-mult
        (measurement ?c ?el ?in1 ?in2 ?out)
        ?m <- (to-check ?c ?el)
        (object (is-a Multiplier)
                (name ?el))
        (test (= (mod (* ?in1 ?in2) 16) ?out))
        (test (<> (mod (* ?in1 ?in2) 32) ?out))
        =>
        (assert (circle ?c no-sign Multiplier ?el))
        (retract ?m))

;the two following rules check if a adder or multiplier funtions well
;if they are then they are considered checked and the to-check fact is deleted
(defrule check-norm-adder
        (measurement ?c ?el ?in1 ?in2 ?out)
        ?m <- (to-check ?c ?el)
        (object (is-a Adder)
                (name ?el))
        (test (= (mod (+ ?in1 ?in2) 32) ?out))
        =>
        (retract ?m))
(defrule check-norm-mult
        (measurement ?c ?el ?in1 ?in2 ?out)
        ?m <- (to-check ?c ?el)
        (object (is-a Multiplier)
                (name ?el))
        (test (= (mod (* ?in1 ?in2) 32) ?out))
        =>
        (retract ?m))

;the two following rules check if a adder or multiplier doesn't funtions well 
;but also if the malfunction is not for the know reasons that adders and multipliers malfunction
;then we need it check if the sensors that measure the feed funtion well
;therefore a to-check fact regarding the input sensors is added
(defrule find-not-norm-adder "find the"
        (measurement ?c ?el ?in1 ?in2 ?out)
        ?m <- (to-check ?c ?el)
        (object (is-a Adder)
                (name ?el)
                (gateInput ?el2 ?el3))
        (test (<> (mod (+ ?in1 ?in2) 16) ?out))
        (test (<> 0 ?out))
        =>
        (assert (not-normal ?c ?el))
        (assert (to-check ?c ?el2))
        (assert (to-check ?c ?el3))
        (retract ?m))
(defrule find-not-norm-multi "find the"
        (measurement ?c ?el ?in1 ?in2 ?out)
        ?m <- (to-check ?c ?el)
        (object (is-a Multiplier)
                (name ?el)
                (gateInput ?el2 ?el3))
        (test (<> (mod (* ?in1 ?in2) 32) ?out))
        (test (<> (mod (* ?in1 ?in2) 16) ?out))
        (test (<> 0 ?out))
        =>
        (assert (not-normal ?c ?el))
        (assert (to-check ?c ?el2))
        (assert (to-check ?c ?el3))
        (retract ?m))

;the following rule checks the sensors if they are short circuited
;if the sensors are marked to be checked, then it means that there is something wrong with the gates on their right
;we check to see if there is something wrong with the gate on their left
;if there is, then this sensor is to blame
(defrule check-left "find the"
        ?m <- (to-check ?c ?el)
        (object (is-a Sensor)
                (name ?el)
                (gateInput ?el2))
        (not-normal ?c ?el2)
        =>
        (assert (circle ?c short Sensor ?el))
        (retract ?m))

;this rule checks to see if everything when normal in a circle
;if everything that should be checked is checked and there is no report for malfuntioning, 
;then it is considered that it funtions normally
(defrule check-normal-circle
        (measurement ?c $?l)
        (not (to-check ?c $?l1))
        (not (circle ?c short $?l2))
        (not (circle ?c no-sign $?l3))
        (not (circle ?c normal))
        =>
        (assert (circle ?c normal)))

;the following three rules delete all the unecessary facts after it is reported that a circle went smooth or not
;this code is not crucial for the program but it clears every fact that doesn't play a role
;(defrule clear-to-check
;        (circle ?c $?l)
;        ?m <- (to-check ?c ?el)
;        =>
;        (retract ?m))
;(defrule clear-meas
;        (circle ?c $?l)
;        ?m <- (measurement ?c $?l2)
;        =>
;        (retract ?m))
;(defrule clear-not-norm
;        (circle ?c $?l)
;        ?m <- (not-normal ?c $?l2)
;        =>
;        (retract ?m))

;the following three rules print the state of the circuit in every circle
;each rule for each problem
;the rules are triggered only for circle that the print-next refers to
;after they print the print-next fact is set for the next circle
(defrule print-normal-state
        ?m <- (print-next ?c)
        (circle ?c normal)
        =>
        (retract ?m)
        (assert (print-next (+ ?c 1)))
        (printout t "Time " ?c " --> Normal Operation!" crlf))
(defrule print-short-circuit
        ?m <- (print-next ?c)
        (circle ?c short ?el ?nam)
        =>
        (retract ?m)
        (assert (print-next (+ ?c 1)))
        (printout t "Time " ?c " --> " ?el " " ?nam " error: Short-circuit!" crlf))
(defrule print-no-sign-state
        ?m <- (print-next ?c)
        (circle ?c no-sign ?el ?nam)
        =>
        (retract ?m)
        (assert (print-next (+ ?c 1)))
        (printout t "Time " ?c " --> " ?el " " ?nam " error: Most significant bit is off!" crlf))
