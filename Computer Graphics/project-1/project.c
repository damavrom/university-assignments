#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

typedef GLfloat point2[2]; 

int prevx;
int prevy;
int randomc=0;
int points=10000;
float wright=100;
float wleft=-100;
float wtop=100;
float wbottom=-100;

int nvert=5;
float radius=100;
float ratio=1.0/3;

void myinit(void)
{
	glEnable(GL_BLEND);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glPointSize(2.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(wleft,wright,wbottom,wtop);
	glMatrixMode(GL_MODELVIEW);
}

void display(void)
{
	point2 vertices[nvert];
	for (int i=0;i<nvert;i++)
	{
		vertices[i][0]=radius*sin(i*2*M_PI/nvert);
		vertices[i][1]=radius*cos(i*2*M_PI/nvert);
	}

	float side=sqrt(pow(vertices[0][0]-vertices[1][0],2)+pow(vertices[0][1]-vertices[1][1],2));

	point2 p;
	p[0]=0;
	p[1]=0;
	
	glClear(GL_COLOR_BUFFER_BIT);

	for(int k=0; k<points; k++)
	{
		int j=rand()%nvert;
		p[0] = (p[0]+vertices[j][0])*ratio; 
		p[1] = (p[1]+vertices[j][1])*ratio;

		float red;
		float blue;
		float green;
		if (randomc)
		{
			red=(float)rand()/RAND_MAX;
			blue=(float)rand()/RAND_MAX;
			green=(float)rand()/RAND_MAX;
		}
		else
		{
			red=1-sqrt(pow(p[0]-vertices[0][0],2)+pow(p[1]-vertices[0][1],2))/(radius*2);
			blue=1-sqrt(pow(p[0]-vertices[nvert/3][0],2)+pow(p[1]-vertices[nvert/3][1],2))/(radius*2);
			green=1-sqrt(pow(p[0]-vertices[2*nvert/3][0],2)+pow(p[1]-vertices[2*nvert/3][1],2))/(radius*2);
		}

		glBegin(GL_POINTS);
		glColor3f(red,blue,green);
		glVertex2fv(p); 
		glEnd();
	}

	glutSwapBuffers();
}

void move(int x, int y)
{
	float shiftx=(prevx-x)*(wright-wleft)/glutGet(GLUT_WINDOW_WIDTH);
	float shifty=-(prevy-y)*(wtop-wbottom)/glutGet(GLUT_WINDOW_HEIGHT);
	wleft+=shiftx;
	wright+=shiftx;
	wtop+=shifty;
	wbottom+=shifty;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(wleft,wright,wbottom,wtop);
	glMatrixMode(GL_MODELVIEW);

	display();
	
	prevx=x;
	prevy=y;
}
void menu(int id)
{
	if (id==1)
	{
		randomc=0;
		points=8000;
	}
	else if (id==2)
	{
		randomc=1;
		points=10000;
	}
	else if (id==3)
	{
		nvert=5;
		ratio=1.0/3;
	}
	else if (id==4)
	{
		nvert=6;
		ratio=1.0/3;
	}
	else if (id==5)
	{
		nvert=3;
		ratio=1.0/2;
	}
	else if (id==6)
		exit(0);
	glutPostRedisplay();
}

void click(int b,int s,int x, int y)
{
	if (b==GLUT_LEFT_BUTTON)
		if (s==GLUT_DOWN)	
		{
			glutMotionFunc(move);
			prevx=x;
			prevy=y;
		}
		else if (s==GLUT_UP)	
			glutMotionFunc(NULL);
}

void main(int argc, char** argv)
{
	srand(time(0));
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(500,500);
	glutCreateWindow("Project 1");

	glutDisplayFunc(display);
	glutMouseFunc(click);

	glutCreateMenu(menu);
	glutAddMenuEntry("8000 points",1);
	glutAddMenuEntry("10000 points of random colour",2);
	glutAddMenuEntry("Pentagon",3);
	glutAddMenuEntry("Hexagon",4);
	glutAddMenuEntry("Triangle",5);
	glutAddMenuEntry("Exit",6);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	
	myinit();
	glutMainLoop();
}
