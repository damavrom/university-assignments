#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

typedef GLfloat point2[2]; 

int prevx;
int prevy;
int randomc=0;
int points=10000;
float wright=100;
float wleft=-100;
float wtop=100;
float wbottom=-100;

int nvert=5;
int depth=3;
float radius=100;
float ratio=3.0/8;

void myinit(void)
{
	glEnable(GL_BLEND);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glColor3f(1.0,1.0,1.0);
	glPointSize(2.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(wleft,wright,wbottom,wtop);
	glMatrixMode(GL_MODELVIEW);
}

void drawTriangle(point2 *v, int n)
{

		glBegin(GL_LINE_LOOP);
		glVertex2fv(v[0]); 
		glVertex2fv(v[1]);
		glVertex2fv(v[2]); 
		glEnd();
		if (n>0)
		{
			point2 v1[3];
			n--;
			v1[0][0]=(v[0][0]+v[1][0])/2;
			v1[0][1]=(v[0][1]+v[1][1])/2;
			v1[1][0]=(v[0][0]+v[2][0])/2;
			v1[1][1]=(v[0][1]+v[2][1])/2;
			v1[2][0]=(v[1][0]+v[2][0])/2;
			v1[2][1]=(v[1][1]+v[2][1])/2;
			point2 triangle1[3];
			triangle1[0][0]=v[0][0];
			triangle1[0][1]=v[0][1];
			triangle1[1][0]=v1[0][0];
			triangle1[1][1]=v1[0][1];
			triangle1[2][0]=v1[1][0];
			triangle1[2][1]=v1[1][1];
			point2 triangle2[3];
			triangle2[0][0]=v[1][0];
			triangle2[0][1]=v[1][1];
			triangle2[1][0]=v1[0][0];
			triangle2[1][1]=v1[0][1];
			triangle2[2][0]=v1[2][0];
			triangle2[2][1]=v1[2][1];
			point2 triangle3[3];
			triangle3[0][0]=v[2][0];
			triangle3[0][1]=v[2][1];
			triangle3[1][0]=v1[1][0];
			triangle3[1][1]=v1[1][1];
			triangle3[2][0]=v1[2][0];
			triangle3[2][1]=v1[2][1];
			drawTriangle(triangle1,n);
			drawTriangle(triangle2,n);
			drawTriangle(triangle3,n);
		}
}

void display(void)
{
	nvert=3;
	ratio=.5;
	point2 vertices[nvert];
	for (int i=0;i<nvert;i++)
	{
		vertices[i][0]=radius*sin(i*2*M_PI/nvert);
		vertices[i][1]=radius*cos(i*2*M_PI/nvert);
	}

	float side=sqrt(pow(vertices[0][0]-vertices[1][0],2)+pow(vertices[0][1]-vertices[1][1],2));

	glClear(GL_COLOR_BUFFER_BIT);
	drawTriangle(vertices,depth);
	glutSwapBuffers();
}

void move(int x, int y)
{
	float shiftx=(prevx-x)*(wright-wleft)/glutGet(GLUT_WINDOW_WIDTH);
	float shifty=-(prevy-y)*(wtop-wbottom)/glutGet(GLUT_WINDOW_HEIGHT);
	wleft+=shiftx;
	wright+=shiftx;
	wtop+=shifty;
	wbottom+=shifty;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(wleft,wright,wbottom,wtop);
	glMatrixMode(GL_MODELVIEW);

	display();
	
	prevx=x;
	prevy=y;
}

void click(int b,int s,int x, int y)
{
	if (s==GLUT_UP)	
	{
		if (b==GLUT_LEFT_BUTTON)
			depth++;
		else if (b==GLUT_RIGHT_BUTTON)
			depth--;
		display();
	}
}

void main(int argc, char** argv)
{
	srand(time(0));
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(1000,1000);
	glutCreateWindow("Project 1");

	glutDisplayFunc(display);
	glutMouseFunc(click);
	
	myinit();
	glutMainLoop();
}
