#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>  
 
char title[] = "Cube";
int makeside=1;
float alpha=7;
float beta=70;
float vx=1;
float vy=2;
float vz=4;
int theta=0;
int phi=0;
 
void initGL()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
	glClearDepth(1.0f);                   
	glEnable(GL_DEPTH_TEST);   
	glDepthFunc(GL_LESS);    
	//glShadeModel(GL_SMOOTH);   
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  
}
 
void drawCube()
{
	float side=2.0;

	glNewList(makeside,GL_COMPILE);
	glBegin(GL_QUADS);
	glVertex3f(side/2.0,side/2.0,1.0);
	glVertex3f(-side/2.0,side/2.0,1.0);
	glVertex3f(-side/2.0,-side/2.0,1.0);
	glVertex3f(side/2.0,-side/2.0,1.0);
	glEnd();
	glEndList();

	
	glScalef(alpha/2.0,alpha/2.0,alpha/2.0);
	//back
	glPushMatrix();
	glColor3f(1.0,0.0,1.0);
	glTranslatef(0.0,0.0,-1+side/2);
	glCallList(makeside);
	glPopMatrix();

	//front
	glPushMatrix();
	glColor3f(0.0,1.0,0.0);
	glTranslatef(0.0,0.0,-1-side/2);
	glCallList(makeside);
	glPopMatrix();

	
	//north
	glPushMatrix();
	glColor3f(1.0,0.0,0.0);
	glTranslatef(0.0,side/2,0);
	glRotatef(90.0,1.0,0.0,0.0);
	glTranslatef(0.0,0.0,-1);
	glCallList(makeside);
	glPopMatrix();
	
	//south
	glPushMatrix();
	glColor3f(0.0,1.0,1.0);
	glTranslatef(0.0,-side/2,0);
	glRotatef(90.0,1.0,0.0,0.0);
	glTranslatef(0.0,0.0,-1);
	glCallList(makeside);
	glPopMatrix();
	
	//west
	glPushMatrix();
	glColor3f(0.0,0.0,1.0);
	glTranslatef(-side/2,0.0,0);
	glRotatef(90.0,0.0,1.0,0.0);
	glTranslatef(0.0,0.0,-1);
	glCallList(makeside);
	glPopMatrix();
	
	//east
	glPushMatrix();
	glColor3f(1.0,1.0,0.0);
	glTranslatef(side/2,0.0,0);
	glRotatef(90.0,0.0,1.0,0.0);
	glTranslatef(0.0,0.0,-1);
	glCallList(makeside);
	glPopMatrix();
	
}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT); 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -100.0, 0.0, 1.0, 0.0);

	glTranslatef(0.0,0.0,-beta);
	drawCube();
	glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height) 
{  
	if (height == 0) 
		height = 1;                
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);  
	glLoadIdentity();             
	gluPerspective(45.0, (GLfloat)width/(GLfloat)height, 0.0, 100.0);
}
void spin()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT); 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -100.0, 0.0, 1.0, 0.0);

	glTranslatef(0.0,0.0,-beta);

	float size=2+sin(2*M_PI*phi/500);
	glScalef(size,size,size);
	glRotatef(theta/10.0,vx,vy,vz);
	theta++;
	phi++;
	drawCube();
	glutSwapBuffers();
	//usleep(100000);
}

void orbit()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT); 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -100.0, 0.0, 1.0, 0.0);

	glTranslatef(0,0,-8*beta);


	glRotatef(theta,vx,vy,vz);

	
	float r=2*sqrt(pow(vx,2)+pow(vy,2)+pow(vz,2));
	glTranslatef(vz*r,vz*r,-(vx+vy)*r);

	float size=2+sin(2*M_PI*phi/1000);
	glScalef(size,size,size);

	theta++;
	phi++;
	drawCube();
	glutSwapBuffers();
	//usleep(100000);
}
 
int main(int argc, char** argv)
{
	glutInit(&argc, argv);            
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(640, 480);   
	glutInitWindowPosition(50, 50); 
	glutCreateWindow(title);          
	glutDisplayFunc(display);       
	glutReshapeFunc(reshape);       
	glutIdleFunc(orbit);       
	initGL();                       
	glutMainLoop();                 
}
