#include <stdio.h>
#include <windows.h>
#include "stdafx.h"
#include <cmath>
#include <Gl\glut.h>
#include<time.h>
#include <stdlib.h>
char title[] = "Cube";
float makeside = 8;
float alpha = 8;
float beta = 90;
float vx = 1;
float vy = 2;
float vz = 6;
int theta = 90;
int phi = 90;
void initGL()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void drawCube()
{
	float side = 2.0;

	glNewList(makeside, GL_COMPILE);
	glBegin(GL_QUADS);
	glVertex3f(side / 2.0, side / 2.0, 1.0);
	glVertex3f(-side / 2.0, side / 2.0, 1.0);
	glVertex3f(-side / 2.0, -side / 2.0, 1.0);
	glVertex3f(side / 2.0, -side / 2.0, 1.0);
	glEnd();
	glEndList();


	glScalef(alpha + alpha / 6, alpha + alpha / 6, alpha + alpha / 6);
	//back
	glPushMatrix();
	glColor3f(0.0, 1.0, 0.0);
	glTranslatef(0.0, 0.0, -1 + side / 2);
	glCallList(makeside);
	glPopMatrix();

	//front
	glPushMatrix();
	glColor3f(0.0, 1.0, 1.0);
	glTranslatef(0.0, 0.0, -1 - side / 2);
	glCallList(makeside);
	glPopMatrix();


	//north
	glPushMatrix();
	glColor3f(1.0, 1.0, 0.0);
	glTranslatef(0.0, side / 2, 0);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glTranslatef(0.0, 0.0, -1);
	glCallList(makeside);
	glPopMatrix();

	//south
	glPushMatrix();
	glColor3f(1.0, 0.0, 0.0);
	glTranslatef(0.0, -side / 2, 0);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glTranslatef(0.0, 0.0, -1);
	glCallList(makeside);
	glPopMatrix();

	//west
	glPushMatrix();
	glColor3f(0.0, 0.0, 1.0);

	glTranslatef(-side / 2, 0.0, 0);
	glRotatef(90.0, 0.0, 1.0, 0.0);
	glTranslatef(0.0, 0.0, -1);
	glCallList(makeside);
	glPopMatrix();

	//east
	glPushMatrix();
	glColor3f(1.0, 0.0, 1.0);

	glTranslatef(side / 2, 0.0, 0);
	glRotatef(90.0, 0.0, 1.0, 0.0);
	glTranslatef(0.0, 0.0, -1);
	glCallList(makeside);
	glPopMatrix();

}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//gluLookAt(1.0, 1.0, 1.0, 1.0, 1.0, -100.0, 1.0, 100.0, 1.0);\
		glTranslatef(1.0, 1.0, -beta);
	drawCube();
	glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluLookAt(0.3, -0.0, 0.5, 1, 0.0, 0.0, 0.0, 4.0, 0.0);
	glOrtho(-100.0, 100.0, -80, 60.0, -100.0, 100.0);

	glMatrixMode(GL_MODELVIEW);

}
void spin()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0, 0, -0.8 * beta);
	glRotatef(-theta / 10, vx, -vy, -vz);

	float size = 2 + sin(3.14*phi / 10000);
	glScalef(size, size, size);
	theta++;
	phi++;
	drawCube();
	glutSwapBuffers();
}

void orbit()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//gluLookAt(-10, -10, 1.0, 1.0, 1.0, -100.0, 1.0, 1.0, 1.0);

	glRotatef(theta / 10, vx, vy, vz);
	glTranslatef(0, 0, -0.8 * beta);



	float size = 2 + sin(2 * 3.14*phi / 10000);

	glScalef(size, size, size);

	theta++;
	phi++;
	drawCube();
	glutSwapBuffers();

}

void demo_menu(int id)
{
	switch (id) {
	case 1:
		glutIdleFunc(spin);
		break;
	case 2:
		glutIdleFunc(orbit);
		break;
	case 3:
		exit(0); break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(640, 480.);
	glutInitWindowPosition(0, 0);
	glutCreateWindow(title);
	glutDisplayFunc(display);

	glutCreateMenu(demo_menu);
	glutAddMenuEntry("Spin", 1);
	glutAddMenuEntry("Orbit", 2);
	glutAddMenuEntry("quit", 3);
	glutIdleFunc(spin);

	glutAttachMenu(GLUT_RIGHT_BUTTON);

	glutReshapeFunc(reshape);


	initGL();
	glutMainLoop();
}