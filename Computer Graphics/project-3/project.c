#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <time.h>
#include <stdlib.h>

typedef GLfloat point3[3];

float theta=0;
GLfloat camx=70;
GLfloat camy=40;
GLfloat camz=0;
float camTheta;
float sunTheta=0;
int toggle=1;

char title[] = "Cube";
void initGL()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glPointSize(5.0);
	glEnable(GL_DEPTH_TEST);
	
	glClearColor (0.0, 0.0, 0.0, 0.0);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	//glShadeModel(GL_FLAT);
	
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}
void normaliz(point3 p)
{
	float squareSum=0;
	for (int i=0;i<3;i++)
		squareSum+=pow(p[i],2);
	float norm=sqrt(squareSum);
	for (int i=0;i<3;i++)
		p[i]=p[i]/norm;
}
void drawSunAux(point3 a, point3 b, point3 c, int depth)
{
	if (!depth)
	{
		glBegin(GL_TRIANGLES);
		glVertex3fv(a);
		glVertex3fv(b);
		glVertex3fv(c);
		glEnd();
		return;
	}
	point3 ab={(a[0]+b[0])/2,(a[1]+b[1])/2,(a[2]+b[2])/2};
	point3 ac={(a[0]+c[0])/2,(a[1]+c[1])/2,(a[2]+c[2])/2};
	point3 bc={(c[0]+b[0])/2,(c[1]+b[1])/2,(c[2]+b[2])/2};
	normaliz(ab);
	normaliz(ac);
	normaliz(bc);
	depth--;
	drawSunAux(a,ab,ac,depth);
	drawSunAux(b,ab,bc,depth);
	drawSunAux(c,ac,bc,depth);
	drawSunAux(ab,ac,bc,depth);
}
void drawSun()
{
	int depth=3;
	point3 v[]={{0.0,0.0,1.0}, {0.0,0.942809,-0.333333}, {-0.816497,-0.471405,-0.333333}, {0.816497,-0.471405,-0.333333}}; 
	GLfloat light_emission[] = { 1.0,1.0,0.0,1.0 };
	glColor3f(1.0,1.0,0.0);
	drawSunAux(v[0],v[1],v[2],depth);
	drawSunAux(v[0],v[1],v[3],depth);
	drawSunAux(v[0],v[2],v[3],depth);
	drawSunAux(v[1],v[2],v[3],depth);
}
void sunlight()
{
	GLfloat b=.3+.7*sin(M_PI*sunTheta/180);
	GLfloat light_position[] = { 1.0,1.0,1.0,1.0 };
	GLfloat light_diffuse[] = { b,b,b,1.0 };
	GLfloat light_ambient[] = { 0.0,0.0,0.0,0.0 };
	GLfloat light_specular[] = { b,b,b,1.0 };
	glLightfv(GL_LIGHT0, GL_AMBIENT,light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR,light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
}
void spotlight()
{
	GLfloat light_position[] = { 5.0,10.,10.0,1.0 };
	GLfloat light_diffuse[] = { 0.5,0.5,0.5,1.0 };
	GLfloat light_ambient[] = { 0.0,0.0,0.0,0.0 };
	GLfloat light_specular[] = { .5,0.5,0.5,.0 };
	GLfloat light_direction[] = { 1,-2,1 };
	GLfloat light_cutoff[] = { 80 };
	GLfloat light_exponent[] = { 4 };
	glLightfv(GL_LIGHT1, GL_POSITION, light_position);
	glLightfv(GL_LIGHT1, GL_AMBIENT,light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE,light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR,light_specular);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light_direction);
	glLightfv(GL_LIGHT1, GL_SPOT_CUTOFF, light_cutoff);
	glLightfv(GL_LIGHT1, GL_SPOT_EXPONENT, light_exponent);
}

void drawScene() {
	//DrawHouse
	glBegin(GL_QUADS);
	

	//front
	GLfloat ambient2[]= {0.04,0.03,0.01,1};
	GLfloat diffuseLight[] = {0.4, 0.3, 0.1, 1.0};
	GLfloat specular[] = { 0.0, 0.0, 0.0, 1.0};
	GLfloat shininess = 10.0;
	GLfloat emission[] = { 0.0f,0.0f,0.0f,1.0f };


	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseLight);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);
	glMaterialfv(GL_BACK, GL_EMISSION, emission);
	glNormal3f(0,0,-1);
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, -10.0f);
	//back
	glNormal3f(0,0,1);
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	//rear
	glNormal3f(-1,0,0);
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	//back 
	glNormal3f(1,0,0);
	glVertex3f(5.0f,0.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	
	//Draw upper
	
	ambient2[0] = 0.02;
	ambient2[1] = 0.02;
	ambient2[2] = 0.03;
	ambient2[3] = 1.0;


	diffuseLight[0] = 0.2;
	diffuseLight[1] = 0.2;
	diffuseLight[2] = 0.3;
	diffuseLight[3] = 1.0;

	specular[0] = 1.0;
	specular[1] = 1.0;
	specular[2] = 1.0;
	specular[3] = 1.0;

	shininess = 110.0;

	emission[0] = 0.0;
	emission[1] = 0.0;
	emission[2] = 0.0;
	emission[3] = 1.0;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseLight);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);
	glMaterialfv(GL_BACK, GL_EMISSION, emission);
	glNormal3f(0.822,0.56,0);
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(6.0f, 10.0f, -11.0f);
	glVertex3f(6.0f, 10.0f, 11.0f);

	glNormal3f(-0.822,0.56,0);
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(-6.0f, 10.0f, -11.0f);
	glVertex3f(-6.0f, 10.0f, 11.0f);	
	

	glEnd();

	glBegin(GL_TRIANGLES);

	glNormal3f(0,0.56,0.82);
	glVertex3f(6.0f, 10.0f, -11.0f);
	glVertex3f(-6.0f, 10.0f,-11.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);

	glNormal3f(0,0.56,-0.82);
	glVertex3f(6.0f, 10.0f, 11.0f);
	glVertex3f(-6.0f, 10.0f, 11.0f);
	glVertex3f(0.0f, 18.66f, 5.0f);

	glEnd();

	// Draw ground
	ambient2[0] = 0.02;
	ambient2[1] = 0.09;
	ambient2[2] = 0.02;
	ambient2[3] = 1.0;

	diffuseLight[0] = 0.2;
	diffuseLight[1] = 0.9;
	diffuseLight[2] = 0.2;
	diffuseLight[3] = 1.0;

	specular[0] = 0.0;
	specular[1] = 0.1;
	specular[2] = 0.0;
	specular[3] = 1.0;

	shininess = 10.0;

	emission[0] = 0.0;
	emission[1] = 0.0;
	emission[2] = 0.0;
	emission[3] = 1.0;

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseLight);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);
	glMaterialfv(GL_BACK, GL_EMISSION, emission);
	GLfloat n=100;
	GLfloat s=-100;
	GLfloat w=-100;
	GLfloat e=100;
	int N=100;
	for (int i=0;i<N;i++)
		for (int j=0;j<N;j++)
		{
			glBegin(GL_QUADS);
			glNormal3f(0,1,0);
			glVertex3f(w+i*(e-w)/N, 0.0f,s+j*(n-s)/N);
			glVertex3f(w+i*(e-w)/N, 0.0f,s+(j+1)*(n-s)/N);
			glVertex3f(w+(i+1)*(e-w)/N, 0.0f,s+(j+1)*(n-s)/N);
			glVertex3f(w+(i+1)*(e-w)/N, 0.0f, s+j*(n-s)/N);
			glEnd();
		}

}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-100.0, 100.0, -100, 100.0, 0.0, 500.0);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	camx=70*cos(camTheta*M_PI/50);
	camz=70*sin(camTheta*M_PI/50);
	gluLookAt(camx,camy,camz,   -camx,-camy,-camz,  0.0,1.0,0.0);
	
	drawScene();
	spotlight();
	glRotatef(sunTheta,0.0,0.0,1.0);
	glTranslatef(50,0,0);
	glScalef(2,2,2);
	glDisable(GL_LIGHTING);
	drawSun();
	sunlight();
	glEnable(GL_LIGHTING);

	glutSwapBuffers();
}
void movePOV(int key, int xx, int yy) {

	switch (key)
	{
		case GLUT_KEY_LEFT:
		camTheta-=1;
		break;
		case GLUT_KEY_RIGHT:
		camTheta+=1;
		break;
	}
	glutPostRedisplay();
}
void toggleSpotlight(int id)
{
	switch (id) {
	case 1:
	{
		if (toggle)
			glDisable(GL_LIGHT1);
		else
			glEnable(GL_LIGHT1);
		toggle=!toggle;
		break;
	}
	case 2:
	{
		exit(0);
		break;
	}
	}
}
void orbitSun()
{
	sunTheta=(sunTheta+.5);
	if (sunTheta>180)
		sunTheta-=180;
	glutPostRedisplay();
}
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(0, 0);
	glutCreateWindow(title);
	glutDisplayFunc(display);
	glutSpecialFunc(movePOV);
	glutIdleFunc(orbitSun);


	glutCreateMenu(toggleSpotlight);
	glutAddMenuEntry("Toggle Spotlight", 1);
	glutAddMenuEntry("Exit", 2);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	srand(time(0));
	camTheta=rand()%360;
	initGL();
	glutMainLoop();
}
