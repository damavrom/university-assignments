
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <glut.h>
#include <time.h>
#define M_PI 3.14

typedef GLfloat point3[3];
GLfloat mat_specular[] = { 0.9f, 0.9f, 0.2f, 1.0f };
float shininess = 100.0;
GLfloat no_mat[] = { 0.0 , 0.0 , 0.0 , 1.0 };
GLfloat ambient[] = { 0.3f, 0.3f, 0.3f , 1.0f };
GLfloat ambient2[] = { 0.9f, 0.9f, 0.9f , 0.0f };
GLfloat ambient3[] = { 0.5f, 0.5f, 0.5f , 1.0f };
GLfloat position[] = { -50.0f, 0.0f, 0.0f, 0.0f };
GLfloat position2[] = { 0.0f, 18.660f , 10.0f, 1.0f };
GLfloat diffuseLight[] = { 0.0f, 0.0f, 0.5f, 1.0f };
GLfloat spot[] = { 0.0f, 50.0f , 50.0f, 1.0f };
GLfloat specular[] = { 1.0f , 1.0f , 1.0f , 1.0f };
GLfloat emission[] = { 0.3f,0.3f,0.3f,1.0f };



float theta = 0;
GLfloat camx = 70;
GLfloat camy = 40;
GLfloat camz = 0;
float camTheta = 0;
int sunTheta = 0;

char title[] = "Cube";
void initGL()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glPointSize(5.0);
	glEnable(GL_DEPTH_TEST);


	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 50.0 };
	glClearColor(0.0, 0.0, 0.0, 0.0);
	GLfloat amp[] = { 1.0, 1.0, 1.0, 1.0 };

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}
void normaliz(point3 p)
{
	float squareSum = 0;
	for (int i = 0; i<3; i++)
		squareSum += pow(p[i], 2);
	float norm = sqrt(squareSum);
	for (int i = 0; i<3; i++)
		p[i] = p[i] / norm;
}

void drawSunAux(point3 a, point3 b, point3 c, int depth)
{
	if (!depth)
	{
		glBegin(GL_TRIANGLES);
		glVertex3fv(a);
		glVertex3fv(b);
		glVertex3fv(c);
		glEnd();
		return;
	}
	point3 ab = { (a[0] + b[0]) / 2,(a[1] + b[1]) / 2,(a[2] + b[2]) / 2 };
	point3 ac = { (a[0] + c[0]) / 2,(a[1] + c[1]) / 2,(a[2] + c[2]) / 2 };
	point3 bc = { (c[0] + b[0]) / 2,(c[1] + b[1]) / 2,(c[2] + b[2]) / 2 };
	normaliz(ab);
	normaliz(ac);
	normaliz(bc);
	depth--;
	drawSunAux(a, ab, ac, depth);
	drawSunAux(b, ab, bc, depth);
	drawSunAux(c, ac, bc, depth);
	drawSunAux(ab, ac, bc, depth);
}
void drawSun()
{


	int depth = 4;
	point3 v[] = { { 0.0,0.0,1.0 },{ 0.0,0.942809,-0.333333 },{ -0.816497,-0.471405,-0.333333 },{ 0.816497,-0.471405,-0.333333 } };
	glColor3f(1.0, 1.0, 0.0);

	drawSunAux(v[0], v[1], v[2], depth);
	drawSunAux(v[0], v[1], v[3], depth);
	drawSunAux(v[0], v[2], v[3], depth);
	drawSunAux(v[1], v[2], v[3], depth);
}
void drawRect() {
	ambient2[0] = 0.0;
	ambient2[1] = 0.0;
	ambient2[2] = 0.0;
	ambient2[3] = 1.0;

	diffuseLight[0] = 0.25;
	diffuseLight[1] = 0.08;
	diffuseLight[2] = 0.02;
	diffuseLight[3] = 1.0;

	mat_specular[0] = 0.0;
	mat_specular[1] = 0.0;
	mat_specular[2] = 0.0;
	mat_specular[3] = 1.0;

	shininess = 10.0;

	emission[0] = 0.0;
	emission[1] = 0.0;
	emission[2] = 0.0;
	emission[3] = 1.0;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseLight);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);
	glMaterialfv(GL_BACK, GL_EMISSION, emission);
	//DrawHouse
	glBegin(GL_QUADS);
	//front
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, -10.0f);
	//back
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	//rear
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	//back 
	glVertex3f(5.0f, 0.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glEnd();



}
void drawRoof() {
	ambient2[0] = 0.9;
	ambient2[1] = 0.1;
	ambient2[2] = 0.1;
	ambient2[3] = 1.0;


	diffuseLight[0] = 0.9;
	diffuseLight[1] = 0.1;
	diffuseLight[2] = 0.1;
	diffuseLight[3] = 1.0;

	mat_specular[0] = 0.0;
	mat_specular[1] = 0.0;
	mat_specular[2] = 0.0;
	mat_specular[3] = 1.0;

	shininess = 100.0;

	emission[0] = 0.0;
	emission[1] = 0.0;
	emission[2] = 0.0;
	emission[3] = 1.0;

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseLight);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);
	glMaterialfv(GL_BACK, GL_EMISSION, emission);
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);


	glEnd();

	glBegin(GL_TRIANGLES);

	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);

	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(0.0f, 18.66f, 5.0f);

	glEnd();

}
void drawGrass() {

	ambient2[0] = 0.2;
	ambient2[1] = 1.0;
	ambient2[2] = 0.2;
	ambient2[3] = 1.0;

	diffuseLight[0] = 0.2;
	diffuseLight[1] = 1.0;
	diffuseLight[2] = 0.2;
	diffuseLight[3] = 1.0;

	mat_specular[0] = 0.0;
	mat_specular[1] = 0.0;
	mat_specular[2] = 0.0;
	mat_specular[3] = 1.0;

	shininess = 20.0;

	emission[0] = 0.0;
	emission[1] = 0.0;
	emission[2] = 0.0;
	emission[3] = 1.0;

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseLight);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);
	glMaterialfv(GL_BACK, GL_EMISSION, emission);
	glBegin(GL_QUADS);
	glVertex3f(-100.0f, 0.0f, -100.0f);
	glVertex3f(-100.0f, 0.0f, 100.0f);
	glVertex3f(100.0f, 0.0f, 100.0f);
	glVertex3f(100.0f, 0.0f, -100.0f);
	glEnd();

	glFlush();
}
void drawScene() {
	
	drawRect();
	drawRoof();
	drawGrass();
	
}
void light() {
	GLfloat light_position[] = { 0.0, 0.0, 1.5, 1.0 };
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
	gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glPushMatrix();
	glRotated(sunTheta, -50.0, 0.0, 0.0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glPopMatrix();
	glutWireTorus(0.275, 0.275, 8, 15);
	glPopMatrix();
	glFlush();
}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-100.0, 100.0, -100, 100.0, 0.0, 500.0);
	light();
	gluLookAt(camx, camy, camz, -camx, -camy, -camz, 0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	drawScene();
	glRotatef((sunTheta * 1) % 180, 0.0, 0.0, 1.0);
	glTranslatef(50, 0, 0);
	glDisable(GL_LIGHTING);
	glScalef(3, 3, 3);
	drawSun();
	glEnable(GL_LIGHTING);

	glutSwapBuffers();
}
void movePOV(int key, int xx, int yy) {

	switch (key)
	{
	case GLUT_KEY_LEFT:
		camTheta--;
		break;
	case GLUT_KEY_RIGHT:
		camTheta++;
		break;
	}
	camx = 70 * cos(camTheta*M_PI / 50);
	camz = 70 * sin(camTheta*M_PI / 50);
	glutPostRedisplay();
}
void orbitSun()
{
	sunTheta++;
	glutPostRedisplay();
}
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutCreateWindow(title);
	glutDisplayFunc(display);
	glutSpecialFunc(movePOV);
	glutIdleFunc(orbitSun);

	initGL();
	glutMainLoop();
}