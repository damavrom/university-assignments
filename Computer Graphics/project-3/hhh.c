#include "stdafx.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <time.h>
// angle of rotation for the camera direction
float angle = 10.0;
// actual vector representing the camera's direction
float lx = 0.0f, lz = -1.0f;
// XZ position of the camera
float x = 0.0f, z = 0.0f;

void drawScene() {
	//DrawHouse
	glColor3f(0.722, 0.525, 0.043);
	glBegin(GL_QUADS);
	//front
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, -10.0f);
	//back
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	//rear
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	//back 
	glVertex3f(5.0f,0.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	


	glColor3f(0.753, 0.753, 0.753);

	//Draw upper
	
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);	
	

	glEnd();

	glBegin(GL_TRIANGLES);

	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f,-10.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);

	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(0.0f, 18.66f, 5.0f);

	glEnd();

	// Draw ground
	glColor3f(0.486, 0.988, 0.000);
	glBegin(GL_QUADS);
	glVertex3f(-100.0f, 0.0f, -100.0f);
	glVertex3f(-100.0f, 0.0f, 100.0f);
	glVertex3f(100.0f, 0.0f, 100.0f);
	glVertex3f(100.0f, 0.0f, -100.0f);
	glEnd();

	glFlush();
}
void renderScene(void) {

	// Clear Color and Depth Buffers

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();
	// Set the camera
	gluLookAt(x + lx, 50.0f, z + lz, 0, -1.0f, 0.0f,0.0f, 1.0f, 0.0f);
	// Draw 36 SnowMen	
			glPushMatrix();
			drawScene();
			glPopMatrix();
	glutSwapBuffers();
}

void processSpecialKeys(int key, int xx, int yy) {

	float fraction = 0.1f;

	switch (key) {
	case GLUT_KEY_LEFT:
		angle -= 2.0f;
		lx = sin(angle);
		lz = -cos(angle);
		break;
	case GLUT_KEY_RIGHT:
		angle += 2.0f;
		lx = sin(angle);
		lz = -cos(angle);
		break;
	case GLUT_KEY_UP:
		x += lx ;
		z += lz ;
		break;
	case GLUT_KEY_DOWN:
		x -= lx ;
		z -= lz ;
		break;
	}
}


void changeSize(int w, int h)
{

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	float ratio = w * 1.0 / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45.0f, ratio, 0.1f, 100.0f);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void processNormalKeys(unsigned char key, int x, int y) {

	if (key == 27)
		exit(0);
}
int main(int argc, char **argv) {

	// init GLUT and create window

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0.0, 0.0);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Erg3");

	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);

	// OpenGL init
	glEnable(GL_DEPTH_TEST);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 1;
}