void drawScene() {
	//DrawHouse
	glColor3f(0.722, 0.525, 0.043);
	glBegin(GL_QUADS);
	//front
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, -10.0f);
	//back
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	//rear
	glVertex3f(-5.0f, 0.0f, -10.0f);
	glVertex3f(-5.0f, 0.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	//back 
	glVertex3f(5.0f,0.0f, -10.0f);
	glVertex3f(5.0f, 0.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	


	glColor3f(0.698, 0.133, 0.133);

	//Draw upper
	
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(0.0f, 18.66f, 5.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);
	glVertex3f(-5.0f, 10.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);	
	

	glEnd();

	glBegin(GL_TRIANGLES);

	glVertex3f(5.0f, 10.0f, -10.0f);
	glVertex3f(-5.0f, 10.0f,-10.0f);
	glVertex3f(0.0f, 18.66f, -5.0f);

	glVertex3f(5.0f, 10.0f, 10.0f);
	glVertex3f(-5.0f, 10.0f, 10.0f);
	glVertex3f(0.0f, 18.66f, 5.0f);

	glEnd();

	// Draw ground
	glColor3f(0.486, 0.988, 0.000);
	glBegin(GL_QUADS);
	glVertex3f(-100.0f, 0.0f, -100.0f);
	glVertex3f(-100.0f, 0.0f, 100.0f);
	glVertex3f(100.0f, 0.0f, 100.0f);
	glVertex3f(100.0f, 0.0f, -100.0f);
	glEnd();

	glFlush();
}