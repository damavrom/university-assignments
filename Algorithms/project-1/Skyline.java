import java.util.Arrays;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.io.FileReader;
import java.io.FileNotFoundException;
class ExecutionTimer
//Code I got from StackOverflow
//It counts like stopwatch
{
	private long start;
	private long end;
	public void end()
	{
		end = System.currentTimeMillis();
	}
	public long duration()
	{
		return (end-start);
	}
	public void reset()
	{
		start = System.currentTimeMillis();
		end   = 0;
	}
}
class Point implements Comparable<Point>
//class of object point
{
	//coordinates of point
	private Integer x;
	private Integer y;
	public Point(int x, int y)
	//constuctor using coordinates
	{
		this.x=x;
		this.y=y;
	}
	public Point(Point p)
	//constuctor using another point
	{
		this.x=p.getX();
		this.y=p.getY();
	}
	@Override
	public int compareTo(Point p)
	//necessary for the object point to be comperable
	//so I can sort containers of points
	//it compares according to x coordinate
	{
		if (p.getX()==this.x)
			return this.y.compareTo(p.getY());
		return this.x.compareTo(p.getX());
	}
	public boolean isDominant(Point p)
	//checks this instant is dominant of another point
	//I don't use it in the algorithm asked from the assignment
	{
		if (p.getX()<this.x)
			return false;
		if (p.getY()<this.y)
			return false;
		if (p.getY()==this.y & p.getX()==this.x)
			return false;
		return true;
	}
	public int getX()
	{
		return this.x;
	}
	public int getY()
	{
		return this.y;
	}
	public void print()
	//it prints the coordinates
	{
		System.out.println(this.x+" "+this.y);
	}
}
public class Skyline
{
	static Point[] getSkylineAux(Point points[])
	//auxiliary recursive function that implements the divide and conquare algorith that is asked
	//it takes a sorted array of points
	//and it returns it's skyline
	{
		Point[] r;
		if (points.length==1)
		//case that input is a single point
		{
			return points;
		}
		if (points.length==2)
		//case that input are two points
		//I must compute which one is the dominant or not
		//if none is dominant,both are returned with the point with least y first 
		{
			if (points[0].getY()==points[1].getY())
			{
				if (points[0].getX()==points[1].getX())
					return points;
			}
			if (points[0].getY()<=points[1].getY())
			{
				if (points[0].getX()>points[1].getX())
					return points;
				r=new Point[1];
				r[0]=new Point(points[0]);
				return r;
			}
			else
			{
				if (points[0].getX()<points[1].getX())
				{
					r=new Point[2];
					r[0]=new Point(points[1]);
					r[1]=new Point(points[0]);
					return r;
				}
				r=new Point[1];
				r[0]=new Point(points[1]);
				return r;
			}
		}
		//if there are more than two points then I have to divide and call recursion
		//first I compute the middle of the input
		int m=(int)(points.length/2);
		//I partition the input into two seperate arrays, both sorted
		Point[] p1=new Point[m];
		for (int i=0;i<p1.length;i++)
			p1[i]=points[i];
		Point[] p2=new Point[points.length-m];
		for (int i=0;i<p2.length;i++)
			p2[i]=points[i+m];
		//I call recursion for both
		Point[] s1=getSkylineAux(p1);
		Point[] s2=getSkylineAux(p2);
		//after I get the skyline of both, I merge them into one that I return
		//I compute the size of the return array, it will be the size of the S1 skyline, plus the points in S2 that are not dominanated by the points of S1 
		//I know that all the points in S2 that have y less than the least y of S1 are not going to be in the returned array
		//S1 and S2 are soted by y so it's easy to find the what points of S2 will be return
		int cnt=0;
		for (int i=0;i<s2.length;i++)
		{
			if (s2[i].getY()<s1[0].getY())
				cnt++;
			else
				break;
		}
		//I copy the points into the return value so they will be sorted by y
		r=new Point[cnt+s1.length];
		for (int i=0;i<cnt;i++)
			r[i]=new Point(s2[i]);
		for (int i=0;i<s1.length;i++)
			r[i+cnt]=new Point(s1[i]);
		return r;
	}
	static Point[] getSkyline(Point[] points)
	{
		//function that sorts and calls the recurtion
		Arrays.sort(points);
		return getSkylineAux(points);
	}
	static Point[] getSkyline2(Point points[])
	//function that I implemended from algorithm that I made 
	//it was not asked by the assignment
	//it's n*log(n) worst case
	{
		Arrays.sort(points);
		Point[] r=new Point[points.length];
		int leastY=points[0].getY();
		r[0]=new Point(points[0]);
		int cnt=1;
		for (int i=1;i<points.length;i++)
			if (points[i].getY()<leastY)
			{
				leastY=points[i].getY();
				r[cnt]=new Point(points[i]);
				cnt++;
			}
		Point[] r2=new Point[cnt];
		for (int i=0;i<cnt;i++)
			r2[i]=r[i];
		return r2;
	}
	static Point[] getSkyline3(Point points[])
	//function that I implemended from algorithm that I made 
	//it was not asked by the assignment
	//it's n*n worst case
	{
		LinkedList<Point> r=new LinkedList<Point>();
		for (int i=0;i<points.length;i++)
		{
			boolean flag=true;
			for (int j=0;j<r.size();j++)
			{
				if (r.get(j).isDominant(points[i]))
				{
					flag=false;
					break;
				}
			}
			if (flag)
			{
				r.addFirst(new Point(points[i]));
				for (int j=1;j<r.size();j++)
					if (points[i].isDominant(r.get(j)))
					{
						r.remove(j);
						j--;
					}
			}
		}
		Point[] r2=new Point[r.size()];
		int cnt=0;
		while (!r.isEmpty())
		{
			r2[cnt]=r.remove();
			cnt++;
		}
		return r2;
	}
	static Point[] readPoints(String filename)
	//reads points from file and returns them in an array
	{
		int n=0;
		Point points[]=null;
		try
		{
			Scanner in=new Scanner(new FileReader(filename));
			n=Integer.parseInt(in.next());
			points=new Point[n];
			for (int i=0;i<n;i++)
			{
				int x=Integer.parseInt(in.next());
				int y=Integer.parseInt(in.next());
				points[i]=new Point(x,y);
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("file not found");
		}
		return points;
	}
	public static void main(String avgs[])
	//main class that sets the timer, calls skyline and prints the output
	{
		ExecutionTimer timer=new ExecutionTimer();
		Point [] points=readPoints(avgs[0]);
		timer.reset();
		Point[] skyline=getSkyline(points);
		timer.end();
		System.out.println(timer.duration());
		for (int i=0;i<skyline.length;i++)
			skyline[i].print();
	}
}
