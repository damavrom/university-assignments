import math
from random import random
n=10**6
f=open("input","w")
f.write(str(n)+"\n")
for i in range(n):
    r=random()
    nxt=1+math.sin(math.pi+math.pi*r/2)
    nxt*=1000
    nxt=int(nxt)
    f.write(str(nxt)+" ")
    nxt=1+math.cos(math.pi+math.pi*r/2)
    nxt*=1000
    nxt=int(nxt)
    f.write(str(nxt)+"\n")
