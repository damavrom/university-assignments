class Point implements Comparable<Point>
{
	private Integer x;
	private Integer y;
	public Point(int x, int y)
	{
		this.x=x;
		this.y=y;
	}
	public Point(Point p)
	{
		this.x=p.getX();
		this.y=p.getY();
	}
	@Override
	public int compareTo(Point p)
	{
		if (p.getX()==this.x)
			return this.y.compareTo(p.getY());
		return this.x.compareTo(p.getX());
	}
	public boolean isDominant(Point p)
	{
		if (p.getX()<this.x)
			return false;
		if (p.getY()<this.y)
			return false;
		if (p.getY()==this.y & p.getX()==this.x)
			return false;
		return true;
	}
	public int getX()
	{
		return this.x;
	}
	public int getY()
	{
		return this.y;
	}
	public void print()
	{
		System.out.println(this.x+" "+this.y);
	}
}
