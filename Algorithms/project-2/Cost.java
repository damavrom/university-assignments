/**
 * @author AEM: Name:
**/
import java.util.Scanner;
import java.io.FileReader;
import java.io.FileNotFoundException;
public class Cost
{
	public static int[][] setMinCosts(int M, int N, int[][] costs, int delays[][])
	/**
	 * @brief returns an array of minimum cost for every machine in ever step
	 *
	 * @param M number of machines
	 * @param N number of steps
	 * @param costs costs of every task for every machine
	 * @param delays costs of a machine communicating with every other machine
	 * @return the minimum costs of every tasks for a given step and a given machine
	**/
	{
		int[][] minCosts=new int[N][M];//initializing the array to return
		for (int i=0;i<M;i++)
			minCosts[0][i]=costs[0][i]; //setting first step
		for (int i=1;i<N;i++)
			for (int j=0;j<M;j++)
			{
				int min=0;//initializing the variable that minimum is stored
				for (int k=1;k<M;k++)
				//calculating the minimum
				//it's the minimum of all the sums of a machine's costs of the previous step and the cost for that machine to send this
					if (minCosts[i-1][k]+delays[k][j]<minCosts[i-1][min]+delays[min][j])
						min=k;

				//passing minimum to the array to return
				minCosts[i][j]=minCosts[i-1][min]+delays[min][j]+costs[i][j];
			}
		return minCosts;
	}
	public static void main(String avgs[])
	{
		//initializing and reading stats from file
		int M=0;//total of machines
		int N=0;//total of steps
		int[][] costs=new int[0][0];//costs of each task for every machine
		int[][] delays=new int[0][0];//costs of delays for machine communicating with every other machine
		int[][] minCosts;
		try
		{
			Scanner in=new Scanner(new FileReader(avgs[0]));
			N=Integer.parseInt(in.next());
			M=Integer.parseInt(in.next());
			costs=new int[N][M];
			delays=new int[M][M];
			for (int i=0;i<N;i++)
				for (int j=0;j<M;j++)
					costs[i][j]=Integer.parseInt(in.next());
			for (int i=0;i<M;i++)
				for (int j=0;j<M;j++)
					delays[i][j]=Integer.parseInt(in.next());
			in.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("file not found");
		}
		
		//calculating the minimum costs for ever machine in every step
		minCosts=setMinCosts(M,N,costs,delays);

		//printing all the costs
		for (int i=0;i<N;i++)
		{
			for (int j=0;j<M;j++)
			{
				System.out.print(minCosts[i][j]);
				System.out.print(" ");//machine in the same step are separated by space
			}
			System.out.println();//steps are separated by new lines
		}
	}
}
