public class ExecutionTimer
{
	private long start;
	private long end;
	public void end()
	{
		end = System.currentTimeMillis();
	}
	public long duration()
	{
		return (end-start);
	}
	public void reset()
	{
		start = System.currentTimeMillis();
		end   = 0;
	}
}
