import math
from random import randint
n=50
m=20
f=open("input","w")
f.write(str(n)+"\n")
f.write(str(m)+"\n")
f.write("\n") 
for i in range(n):
    for j in range(m):
        f.write(str(randint(1,9))+" ")
    f.write("\n") 
f.write("\n") 

for i in range(m):
    for j in range(m):
        if i==j:
            f.write("0 ")
        else:
            f.write(str(randint(1,10))+" ")
    f.write("\n") 
