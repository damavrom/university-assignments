#include "ex1.h"
using namespace std;

long double f(long double x)
{
	return 14*x*exp(x-2) -12*exp(x-2) -7*pow(x,3) +20*pow(x,2) -26*x +12;
}
long double fprime(long double x)
{
	return 14*exp(x-2) +14*x*exp(x-2) -12*exp(x-2) -21*pow(x,2) +40*x -26;
}
main()
{
	long double exeptableError=pow(10,-6);
	cout<<"Using bisection"<<endl;
	bisection(f,0,1,exeptableError);
	bisection(f,1.6,2.6,exeptableError);

	cout<<endl<<"Using Newton-Raphson"<<endl;
	newton(f,fprime,1,exeptableError,1);
	newton(f,fprime,3,exeptableError,3);
/*
	cout<<endl<<"Using secant"<<endl;
	secant(f,-2,-1.5,exeptableError);
	secant(f,-1.5,1.4,exeptableError);
	secant(f,2,1.5,exeptableError);
*/
}
