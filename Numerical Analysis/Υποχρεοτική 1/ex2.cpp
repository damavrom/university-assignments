#include <cmath>
#include "ex2.h"
#include "ex1.h"
using namespace std;

long double f(long double x)
{
	return 54*pow(x,6) + 45*pow(x,5) - 102*pow(x,4) - 69*pow(x,3) + 35*pow(x,2) + 16*x - 4;
}
long double fprime(long double x)
{
	return 324*pow(x,5) + 225*pow(x,4) - 408*pow(x,3) - 207*pow(x,2) + 70*x + 16;
}
long double fprimeprime(long double x)
{
	return 1620*pow(x,4) + 900*pow(x,3) - 1224*pow(x,2) + 414*x + 70;
}
main()
{
	cout<<"Running modified bisection ten times"<<endl;
	long double exeptableError=pow(10,-6);
	int sum=0;
	for (int i=0;i<10;i++)
		sum+=bisectionedited(f,-2,-1,exeptableError);
	cout<<"Avg iterations:"<<sum/10<<endl;
	cout<<endl;
	cout<<"Using bisection"<<endl;
	bisection(f,-2,-1,exeptableError);
	bisection(f,-1,.4,exeptableError);
	bisection(f,.4,1,exeptableError);
	bisection(f,1,2,exeptableError);
	cout<<"Using modified bisection"<<endl;
	bisectionedited(f,-2,-1,exeptableError);
	bisectionedited(f,-1,.4,exeptableError);
	bisectionedited(f,.4,1,exeptableError);
	bisectionedited(f,1,2,exeptableError);

	cout<<endl<<"Using Newton-Raphson"<<endl;
	newton(f,fprime,-2,exeptableError);
	newton(f,fprime,-1,exeptableError);
	newton(f,fprime,-0,exeptableError);
	newton(f,fprime,.7,exeptableError);
	newton(f,fprime,2,exeptableError);
	cout<<endl<<"Using modified Newton-Raphson"<<endl;
	newtonedited(f,fprime,fprimeprime,-2,exeptableError);
	newtonedited(f,fprime,fprimeprime,-1,exeptableError);
	newtonedited(f,fprime,fprimeprime,-0,exeptableError);
	newtonedited(f,fprime,fprimeprime,.7,exeptableError);
	newtonedited(f,fprime,fprimeprime,2,exeptableError);

	cout<<endl<<"Using secant"<<endl;
	secant(f,-2,-1.5,exeptableError);
	secant(f,-1,-.7,exeptableError);
	secant(f,0,.4,exeptableError);
	secant(f,.6,1,exeptableError);
	secant(f,1,2,exeptableError);
	cout<<endl<<"Using modified secant"<<endl;
	secantedited(f,-2,-1.7,-1.5,exeptableError);
	secantedited(f,-1,-.8,-.7,exeptableError);
	secantedited(f,0,.2,.4,exeptableError);
	secantedited(f,.6,.7,1,exeptableError);
	secantedited(f,1,1.5,2,exeptableError);
}
