#include <iostream>
using namespace std;
double normInf(double **A, int n)
{
	return 1;
}
int canSolve(double **A, int n)
{
	return 1;
}
double *gaussPLU(double **A, double *b, int n)
{
	double **L=new double*[n];
	double **U=new double*[n];
	int *P=new int[n];
	for (int i=0;i<n;i++)
	{
		P[i]=i;
		L[i]=new double[n];
		U[i]=new double[n];
	}
	for (int i=0;i<n;i++)
	{
		int cnt=0;
		do
		{	
			for (int j=0;j<i;j++)
			{
				double sum=0;
				for (int k=0;k<j;k++)
					sum+=L[i][k]*U[k][j];
				L[i][j]=(A[P[i]][j]-sum)/U[j][j];
			}
			L[i][i]=1;
			for (int j=i;j<n;j++)
			{
				double sum=0;
				for (int k=0;k<i;k++)
					sum+=L[i][k]*U[k][j];
				U[i][j]=A[P[i]][j]-sum;
			}
			if (!U[i][i])
			{
				cout<<"needs pivot\n";
				cnt++;
				int temp=P[i];
				for (int k=i+1;k<n;k++)
					P[k-1]=P[k];
				P[n-1]=temp;
				if (cnt>=n-i)
					return 0;
			}
			else
				cnt=0;
		}
		while (cnt);
	}
	double *y=new double[n];
	for (int i=0;i<n;i++)
	{
		double sum=0;
		for (int j=0;j<i;j++)
			sum+=y[j]*L[i][j];
		y[i]=b[P[i]]-sum;
	}
	double *x=new double[n];
	for (int i=n-1;i>=0;i--)
	{
		double sum=0;
		for (int j=n-1;j>i;j--)
			sum+=x[j]*U[i][j];
		x[i]=(y[i]-sum)/U[i][i];
	}
	return x;
}
double **cholesky(double **A, int n)
{
	double **L=new double*[n];
	return L;
}
double *gaussSeidel(double **A, double *b, int n)
{
	double *x=new double[n];
	return x;
}
