#include <cmath>
#include <iostream>
using namespace std;
void printRes(long double root, int i)
//Prints the results of the methods
{
	ios_base::fmtflags oldflags=cout.flags();
	cout.setf(ios::fixed,ios::floatfield);
	cout.precision(6);
	cout<<"approximation:"<<root<<"\titerations:"<<i<<endl;
	cout.flags (oldflags);
}
int bisection(long double (*f)(long double), long double a,long double b,long double error)
//function f,range [a,b] and acceptable error as parameters
{
	cout<<"starting range:["<<a<<", "<<b<<"]\t";
	int cnt=0;
	int flag=0;
	long double root;
	if (f(a)==0)
	{
		root=a;
		flag=1;
	}
	else if (f(b)==0)
	{
		root=b;
		flag=1;
	}
	while (abs(a-b)>error&&!flag)
	{
		long double m=(a+b)/2;
		long double fa=f(a);
		long double fb=f(b);
		long double fm=f(m);
		if (fa*fb<0)
		{
			if (fm==0)
			{
				root=m;
				flag=1;
			}
			else if (fa*fm<0)
				b=m;
			else
				a=m;
		}
		else
		{
			cout<<endl<<"There is probably not a root in this range"<<endl;
			return cnt;
		}
		cnt++;
	}
	if (!flag)
		root=(a+b)/2;
	printRes(root,cnt);
	return cnt;
}
int newton(long double (*f)(long double), long double (*fderiv)(long double), long double a,long double error, double L)
//function f, derivative fderiv, starting value a, acceptable error and Lipschitz constant L as parameters
{
	cout<<"starting value:"<<a<<"\t";
	int cnt=0;
	long double aprev;
	do
		if (fderiv(a))
		{
			aprev=a;
			a=a-f(a)/fderiv(a);
			cnt++;
		}
		else if (f(a))
		{
			cout<<endl<<"Unable to calculate the root"<<endl;
			return cnt;
		}
		else
			break;
	while (L*abs(a-aprev)>error);
	printRes(a,cnt);
	return cnt;
}
int secant(long double (*f)(long double), long double a,long double b, long double error, double L)
//function f, starting values a,b, acceptable error and Lipschitz constant L as parameters
{
	cout<<"starting values:"<<a<<", "<<b<<"\t";
	int cnt=0;
	long double temp;
	while (L*abs(a-b)>error)
		if (f(a)!=f(b))
		{
			temp=a-f(a)/((f(a)-f(b))/(a-b));
			b=a;
			a=temp;
			cnt++;
		}
		else if (f(a))
		{
			cout<<"Unable to calculate the root"<<endl;
			return cnt;
		}
		else
			break;
	printRes(a,cnt);
	return cnt;
}
