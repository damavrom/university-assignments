#include "ex1.h"
using namespace std;

long double f(long double x)
{
	return 14*x*exp(x-2) -12*exp(x-2) -7*pow(x,3) +20*pow(x,2) -26*x +12;
}
long double fprime(long double x)
{
	return 14*exp(x-2) +14*x*exp(x-2) -12*exp(x-2) -21*pow(x,2) +40*x -26;
}
main()
{
	cout<<"Finging roots of f:"<<endl<<endl;
	long double acceptableError=pow(10,-6);
	cout<<"(a) Using bisection"<<endl;
	bisection(f,0,1,acceptableError);
	bisection(f,1.6,2.6,acceptableError);

	cout<<endl<<"(b) Using Newton-Raphson"<<endl;
	newton(f,fprime,1,acceptableError,1);
	newton(f,fprime,3,acceptableError,3);

	cout<<endl<<"(c) Using secant"<<endl;
	secant(f,0,1,acceptableError,1);
	secant(f,1.5,3,acceptableError,3);
}
