#include <time.h>	//time
#include <stdlib.h>	//srand,rand
#include <algorithm>	//min
#include "ex1.h"
using namespace std;

int bisectionEdited(long double (*f)(long double),long double a,long double b,long double error)
//function f,range [a,b] and acceptable error as parameters
{
	cout<<"starting range:["<<a<<", "<<b<<"]\t";
	int cnt=0;
	int static fa=1;
	if (fa)
	{
		fa=0;
		srand(time(0));
	}
	int flag=0;
	long double root;
	if (f(a)==0)
	{
		root=a;
		flag=1;
	}
	else if (f(b)==0)
	{
		root=b;
		flag=1;
	}
	while (abs(a-b)>error&&!flag)
	{
		long double m=((long double)rand()/RAND_MAX)*abs(a-b)+min(a,b);
		long double fa=f(a);
		long double fb=f(b);
		long double fm=f(m);
		if (fa*fb<0)
		{
			if (fm==0)
			{
				root=m;
				flag=1;
			}
			else if (fa*fm<0)
				b=m;
			else
				a=m;
		}
		else
		{
			cout<<endl<<"There is probably not a root in this range"<<endl;
			return cnt;
		}
		cnt++;
	}
	if (!flag)
		root=(a+b)/2;
	printRes(root,cnt);
	return cnt;
}
int newtonEdited(long double (*f)(long double),long double (*fderiv)(long double),long double (*fderivderiv)(long double), long double a,long double error,double L)
//function f, derivative fderiv, double derivative fderivderiv, starting value a, acceptable error and Lipschitz constant L as parameters
{
	cout<<"starting value:"<<a<<"\t";
	int cnt=0;
	long double aprev;
	do
		if (fderiv(a))
		{
			aprev=a;
			a=a-f(a)/fderiv(a)-pow(f(a),2)*fderivderiv(a)/(2*pow(fderiv(a),3));
			cnt++;
		}
		else if (f(a))
		{
			cout<<endl<<"Unable to calculate the root"<<endl;
			return cnt;
		}
		else
			break;
	while (L*abs(a-aprev)>error);
	printRes(a,cnt);
	return cnt;
}
int secantEdited(long double (*f)(long double),long double a,long double b, long double c, long double error, double L)
//function f, starting values a,b,c, acceptable error and Lipschitz constant L as parameters
{
	cout<<"starting values:"<<a<<","<<b<<","<<c<<"\t";
	int cnt=0;
	long double temp;
	long double r;
	long double q;
	long double s;
	while (L*abs(a-b)>error)
		if (f(a)*f(b)*f(c))
		{
			q=f(c)/f(b);
			r=f(a)/f(b);
			s=f(a)/f(c);
			temp=a-(r*(r-q)*(a-b)+(1-r)*s*(a-c))/((q-1)*(r-1)*(s-1));
			c=b;
			b=a;
			a=temp;
			cnt++;
		}
		else 
		{	
			if (!f(b))
				a=b;
			else if (!f(c))
				a=c;
			break;
		}
	printRes(a,cnt);
	return cnt;
}
