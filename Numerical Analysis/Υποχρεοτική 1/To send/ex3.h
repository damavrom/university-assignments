#include <iostream>
#include <cmath>
using namespace std;
double normInf(double *a, int n)
//returns infinite norm of square array a size n
{
	double max=0;
	for (int i=0;i<n;i++)
		if (abs(a[i])>max)
			max=abs(a[i]);
	return max;
}
double *diff(double *a,double *b, int n)
//returns difference of square arrays a and b size n
{
	double *c=new double[n];
	for (int i=0;i<n;i++)
		c[i]=a[i]-b[i];
	return c;
}
double *gaussPLU(double **A, double *b, int n)
//takes square array A and vector b and their size n
//returns vector x, the solution of the linear system of A and b
{
	double **L=new double*[n];
	double **U=new double*[n];
	int *P=new int[n];
	for (int i=0;i<n;i++)
	{
		P[i]=i;
		L[i]=new double[n];
		U[i]=new double[n];
	}
	for (int i=0;i<n;i++)
	{
		int cnt=0;
		do
		{	
			for (int j=0;j<i;j++)
			{
				double sum=0;
				for (int k=0;k<j;k++)
					sum+=L[i][k]*U[k][j];
				L[i][j]=(A[P[i]][j]-sum)/U[j][j];
			}
			L[i][i]=1;
			for (int j=i;j<n;j++)
			{
				double sum=0;
				for (int k=0;k<i;k++)
					sum+=L[i][k]*U[k][j];
				U[i][j]=A[P[i]][j]-sum;
			}
			if (!U[i][i])
			{
				cout<<"needs pivot\n";
				cnt++;
				int temp=P[i];
				for (int k=i+1;k<n;k++)
					P[k-1]=P[k];
				P[n-1]=temp;
				if (cnt>=n-i)
					return 0;
			}
			else
				cnt=0;
		}
		while (cnt);
	}
	double *y=new double[n];
	for (int i=0;i<n;i++)
	{
		double sum=0;
		for (int j=0;j<i;j++)
			sum+=y[j]*L[i][j];
		y[i]=b[P[i]]-sum;
	}
	double *x=new double[n];
	for (int i=n-1;i>=0;i--)
	{
		double sum=0;
		for (int j=n-1;j>i;j--)
			sum+=x[j]*U[i][j];
		x[i]=(y[i]-sum)/U[i][i];
	}
	return x;
}
double **cholesky(double **A, int n)
//takes in square symetrical positive-definite matrix A of size n
//returns the matrix L of Cholesky's decomposition
{
	double **L=new double*[n];
	for (int i=0;i<n;i++)
	{
		L[i]=new double[n];
		for  (int j=0;j<n;j++)
			L[i][j]=0;
	}
	for (int i=0;i<n;i++)
	{
		for  (int j=0;j<i;j++)
		{
			double sum=0;
			for (int k=0;k<j;k++)
				sum+=L[i][k]*L[j][k];
			L[i][j]=(A[i][j]-sum)/L[j][j];
		}
		double sum=0;
		for (int k=0;k<i;k++)
			sum+=pow(L[i][k],2);
		L[i][i]=sqrt(A[i][i]-sum);
	}
	return L;
}
double **getA(int n)
//returns the matrix needed for exercise 3 of size n
{
	double **A=new double*[n];
	for (int i=0;i<n;i++)
	{
		A[i]=new double[n];
		for (int j=0;j<n;j++)
			A[i][j]=0;
		A[i][i]=5;
		if (i>0)
			A[i][i-1]=-2;
		if (i<n-1)
			A[i][i+1]=-2;
	}
	return A;
}
double *getb(int n)
//returns the vector needed for exercise 3 of size n
{
	double *b=new double[n];
	for (int i=1;i<n-1;i++)
		b[i]=1;
	b[0]=3;
	b[n-1]=3;
	return b;
}
double *gaussSeidel(double **A, double *b, double *x, int n, double error)
//impliments the Gauss-Seidel method on a matrix A
//takes square matrix A, vector b and x of size n and the acceptable error
//x is the starting vector 
//returns the solution of the linear equation A*x=b
{
	double *xprev=new double[n];
	double e;
	do
	{
		for (int i=0;i<n;i++)
			xprev[i]=x[i];
		for (int i=0;i<n;i++)
		{
			double sum=0;
			for (int j=0;j<n;j++)
				if (j!=i)
					sum+=A[i][j]*x[j];
			x[i]=(b[i]-sum)/A[i][i];
		}
		e=normInf(diff(xprev,x,n),n);
	}
	while (e>error);
	return x;
}
