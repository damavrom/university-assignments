#include "ex3.h"
using namespace std;
void printArr(double** A, int n)
{
	for (int i=0;i<n;i++)
	{
		cout<<'|';
		for (int j=0;j<n;j++)
			cout<<A[i][j]<<" ";
		cout<<'|'<<endl;
	}
}
void printArr(double* A, int n)
{
	for (int i=0;i<n;i++)
		cout<<'|'<<A[i]<<'|'<<endl;
}
main()
{
	cout<<endl<<"1. Solving linear equation using PLU decompotion"<<endl;
	int n=3;
	double temp1[][n]={{9,3,4},{4,3,4},{1,1,1}};
	double temp2[]={7,8,3};
	double **A=new double*[n];
	double *b=new double[n];
	for (int i=0;i<n;i++)
	{
		A[i]=new double[n];
		b[i]=temp2[i];
		for (int j=0;j<n;j++)
			A[i][j]=temp1[i][j];
	}
	double *x=new double[n];
	cout<<endl<<"A="<<endl;
	printArr(A,n);
	cout<<endl<<"b="<<endl;
	printArr(b,n);
	for (int i=0;i<n;i++)
		x[i]=0;
	x=gaussPLU(A,b,n);
	cout<<endl<<"x="<<endl;
	printArr(x,n);

	cout<<endl<<"2. Decomposing symetrical positive-definite matrix"<<endl;
	n=3;
	double temp3[][n]={{25,15,-5},{15,18,0},{-5,0,11}};
	A=new double*[n];
	double **L=new double*[n];
	for (int i=0;i<n;i++)
	{
		A[i]=new double[n];
		for (int j=0;j<n;j++)
			A[i][j]=temp3[i][j];
	}
	cout<<endl<<"A="<<endl;
	printArr(A,n);
	for (int i=0;i<n;i++)
		L[i]=0;
	L=cholesky(A,n);
	cout<<endl<<"L="<<endl;
	printArr(L,n);

	cout<<endl<<"3. Solving linear equation using Gauss-Seidel method"<<endl;
	n=10;
	A=getA(n);
	cout<<endl<<"A="<<endl;
	printArr(A,n);
	b=getb(n);
	cout<<endl<<"b="<<endl;
	printArr(b,n);
	x=new double[n];
	for (int i=0;i<n;i++)
		x[i]=0;
	cout<<endl<<"x0="<<endl;
	printArr(x,n);
	x=gaussSeidel(A,b,x,n,pow(10,-4));
	cout<<endl<<"Result:"<<endl;
	printArr(x,n);
	n=100;
	A=getA(n);
	cout<<endl<<"A="<<endl;
	printArr(A,n);
	b=getb(n);
	cout<<endl<<"b="<<endl;
	printArr(b,n);
	x=new double[n];
	for (int i=0;i<n;i++)
		x[i]=0;
	cout<<endl<<"x0="<<endl;
	printArr(x,n);
	x=gaussSeidel(A,b,x,n,pow(10,-4));
	cout<<endl<<"Result:"<<endl;
	printArr(x,n);
}
