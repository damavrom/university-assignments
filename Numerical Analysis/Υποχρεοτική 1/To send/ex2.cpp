#include "ex2.h"
using namespace std;

long double f(long double x)
{
	return 94*pow(cos(x),3)-24*cos(x)+177*pow(sin(x),2)-108*pow(sin(x),4)-72*pow(cos(x),3)*pow(sin(x),2)-65;
}
long double fprime(long double x)
{
	return -282*pow(cos(x),2)*sin(x)+24*sin(x)+354*sin(x)*cos(x)-432*pow(sin(x),3)*cos(x)+216*pow(cos(x),2)*pow(sin(x),3)-144*pow(cos(x),4)*sin(x);
}
long double fprimeprime(long double x)
{
	return 564*cos(x)*pow(sin(x),2)-282*pow(cos(x),3)+24*cos(x)+354*pow(cos(x),2)-354*pow(sin(x),2)-1296*pow(sin(x),2)*pow(cos(x),2)+432*pow(sin(x),4)+1224*pow(cos(x),3)*pow(sin(x),2)-432*cos(x)*pow(sin(x),4);-144*pow(cos(x),5);
}
main()
{
	long double acceptableError=pow(10,-6);

	cout<<endl<<"1. Findind roots of f using modified methods"<<endl;
	cout<<endl<<"(a) Using modified bisection"<<endl;
	bisectionEdited(f,0,1,acceptableError);
	bisectionEdited(f,1,2,acceptableError);
	bisectionEdited(f,2,3,acceptableError);
	cout<<endl<<"(b) Using modified Newton-Raphson"<<endl;
	newtonEdited(f,fprime,fprimeprime,.8,acceptableError,1);
	newtonEdited(f,fprime,fprimeprime,1,acceptableError,3);
	newtonEdited(f,fprime,fprimeprime,2.3,acceptableError,1);
	cout<<endl<<"(c) Using modified secant"<<endl;
	secantEdited(f,.6,.7,.8,acceptableError,1);
	secantEdited(f,1,1.1,1.2,acceptableError,3);
	secantEdited(f,2.2,2.3,2.4,acceptableError,1);

	cout<<endl<<"2. Running modified bisection ten times"<<endl<<endl;
	int sum=0;
	for (int i=0;i<10;i++)
		sum+=bisectionEdited(f,0,1,acceptableError);
	cout<<"Avg iterations:"<<sum/10<<endl;

	cout<<endl<<"3. Findind roots of f using original methods"<<endl;
	cout<<endl<<"Using bisection"<<endl;
	bisection(f,0,1,acceptableError);
	bisection(f,1,2,acceptableError);
	bisection(f,2,3,acceptableError);
	cout<<endl<<"Using Newton-Raphson"<<endl;
	newton(f,fprime,.8,acceptableError,1);
	newton(f,fprime,1,acceptableError,3);
	newton(f,fprime,2.3,acceptableError,1);
	cout<<endl<<"Using secant"<<endl;
	secant(f,.7,.8,acceptableError,1);
	secant(f,1,1.1,acceptableError,3);
	secant(f,2.2,2.3,acceptableError,1);
}
