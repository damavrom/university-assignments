#include <cmath>	//abs,pow
#include <time.h>	//time
#include <stdlib.h>	//srand,rand
#include <algorithm>	//min
#include <iostream>
using namespace std;

int bisectionedited(long double (*f)(long double),long double a,long double b,long double error)
{
	cout<<"starting range:["<<a<<","<<b<<"]\t";
	int cnt=0;
	int static flag=1;
	if (flag)
	{
		flag=0;
		srand(time(0));
	}
	long double c=((long double)rand()/RAND_MAX)*abs(a-b)+min(a,b);
	while (abs(a-b)>error)
	{
		if (f(a)*f((c))<0)
			b=c;
		else if  (f(b)*f(c)<0)
			a=c;
		else
		{
			cout<<endl<<"There is probably not a root in this range"<<endl;
			return cnt;
		}
		c=((long double)((rand()+1)%RAND_MAX-1)/RAND_MAX)*abs(a-b)+min(a,b);
		cnt++;	
	}
	ios_base::fmtflags oldflags=cout.flags();
	cout.setf(ios::fixed,ios::floatfield);
	cout.precision(6);
	cout<<"approximation:"<<(a+b)/2<<"\titerations:"<<cnt<<endl;
	cout.flags (oldflags);
	return cnt;
}
int newtonedited(long double (*f)(long double),long double (*fprime)(long double),long double (*fprimeprime)(long double), long double a,long double error)
{
	cout<<"starting value:"<<a<<"\t";
	int cnt=0;
	long double aprev;
	do
		if (fprime(a))
		{
			aprev=a;
			a=a-1/(fprime(a)/f(a)-fprimeprime(a)/(2*fprimeprime(a)));
			cnt++;
		}
		else if (f(a))
		{
			cout<<endl<<"Unable to calculate the root"<<endl;
			return cnt;
		}
	while (abs(a-aprev)>error*abs(a));
	ios_base::fmtflags oldflags=cout.flags();
	cout.setf(ios::fixed,ios::floatfield);
	cout.precision(6);
	cout<<"approximation:"<<a<<"\titerations:"<<cnt<<endl;
	cout.flags (oldflags);
	return cnt;
}
int secantedited(long double (*f)(long double),long double a,long double b, long double c, long double error)
{
	cout<<"starting values:"<<a<<","<<b<<","<<c<<"\t";
	int cnt=0;
	long double temp;
	long double r;
	long double q;
	long double s;
	while (abs(a-b)>abs(b)*error)
		if (f(a)!=f(b))
		{
			q=f(c)/f(b);
			r=f(a)/f(b);
			s=f(a)/f(c);
			temp=a-(r*(r-q)*(a-b)+(1-r)*s*(a-c))/((q-1)*(r-1)*(s-1));
			c=b;
			b=a;
			a=temp;
			cnt++;
		}
		else if (f(a))
		{
			cout<<"Unable to calculate the root"<<endl;
			return cnt;
		}
	ios_base::fmtflags oldflags=cout.flags();
	cout.setf(ios::fixed,ios::floatfield);
	cout.precision(6);
	cout<<"approximation:"<<a<<"\titerations:"<<cnt<<endl;
	cout.flags (oldflags);
	return cnt;
}

