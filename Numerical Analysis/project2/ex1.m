A=[ 
0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 ;
0 0 1 0 1 0 1 0 0 0 0 0 0 0 0 ;
0 1 0 0 0 1 0 1 0 0 0 0 0 0 0 ;
0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 ;
1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ;
0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 ;
0 0 0 0 1 1 0 0 0 1 0 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 ;
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 ;
0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 ;
0 0 0 0 0 0 0 0 1 0 0 0 0 1 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 1 0 1 ;
0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 ]


function x = n(A) 
	l=length(A)
	x=zeros(1,l)
	for i = [1:l]
		for j = [1:l]
			x(i)=x(i)+A(i,j)
		endfor
	endfor
end

function tmp = G(A,q) 
	l=length(A)
	tmp=zeros(l,l)
	n=n(A)
	for i = [1:l]
		for j = [1:l]
			tmp(i,j)=q/l+A(j,i)*(1-q)/n(j)
		endfor
	endfor
end

function tmp = p(A,q)
	l=length(A)
	tmp=ones(l,1)
	G=G(A,q)
	for i = [1:500]
		tmp=tmp/tmp(1)
		tmp=G*tmp
	endfor
	tmp=tmp/sum(tmp)
end
