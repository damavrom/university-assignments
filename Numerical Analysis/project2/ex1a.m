A=[ 
0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 ;
0 0 1 0 1 0 1 0 0 0 0 0 0 0 0 ;
0 1 0 0 0 1 0 1 0 0 0 0 0 0 0 ;
0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 ;
1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ;
0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 ;
0 0 0 0 1 1 0 0 0 1 0 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 ;
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 ;
0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 ;
0 0 0 0 0 0 0 0 1 0 0 0 0 1 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 1 0 1 ;
0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 ]
q=.15
n=15

ni=zeros(1,15)
for i = [1:15]
	for j = [1:15]
		ni(i)=ni(i)+A(i,j)
	endfor
endfor

G=zeros(15,15)
for i = [1:15]
	for j = [1:15]
		G(i,j)=q/n+A(j,i)*(1-q)/ni(j)
	endfor
endfor

p=ones(15,1)
for i = [1:50]
	p=p/p(1)
	p=G*p
endfor
p=p/sum(p)
