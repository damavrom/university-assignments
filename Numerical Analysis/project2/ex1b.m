A=[ 
0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 ;
0 0 1 0 1 0 1 0 0 0 0 0 0 0 0 ;
0 1 0 0 0 1 0 1 0 0 0 0 0 0 0 ;
0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 ;
1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ;
0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 ;
0 0 0 0 1 1 0 0 0 1 0 0 0 0 0 ;
0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 ;
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 ;
0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 ;
1 0 0 0 0 0 0 0 1 0 0 0 0 1 0 ;
1 0 0 0 0 0 0 0 0 1 1 0 1 0 1 ;
1 0 0 0 0 0 0 0 0 0 0 1 0 1 0 ]

q=.15
n=15
ni=zeros(1,15)
for i = [1:15]
	for j = [1:15]
		ni(i)=ni(i)+A(i,j)
	endfor
endfor
G=zeros(15,15)
for i = [1:15]
	for j = [1:15]
		G(i,j)=q/n+A(j,i)*(1-q)/ni(j)
	endfor
endfor
p=ones(15,1)
for i = [1:50]
	p=p/p(1)
	p=G*p
endfor
p=p/sum(p)

q=.01
G01=zeros(15,15)
for i = [1:15]
	for j = [1:15]
		G01(i,j)=q/n+A(j,i)*(1-q)/ni(j)
	endfor
endfor
p01=ones(15,1)
for i = [1:50]
	p01=p01/p01(1)
	p01=G01*p01
endfor
p01=p01/sum(p01)

q=.7
G7=zeros(15,15)
for i = [1:15]
	for j = [1:15]
		G7(i,j)=q/n+A(j,i)*(1-q)/ni(j)
	endfor
endfor

p7=ones(15,1)
for i = [1:50]
	p7=p7/p7(1)
	p7=G*p7
endfor
p7=p7/sum(p7)
