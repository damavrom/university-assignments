import math
a=[]
a+=[0.488]
a+=[2.978]
a+=[-1.894]
a+=[-1.088]
a+=[-1.090]
a+=[0.884]
a+=[1.849]
a+=[0.290]
a+=[-0.160]
a+=[1.917]

a+=[math.pi]
a+=[-math.pi]
f=set()
for i in a:
    f=f|{(i,math.sin(i))}

import ex
fl=open("stats","w")
rng=[]
for i in range(200):
    rng+=[i*2*math.pi/200-math.pi]
for i in rng:
    fl.write(str(i))
    fl.write("\t")
    fl.write(str(ex.lagrange(f,i)-math.sin(i)))
    fl.write("\t")
    fl.write(str(ex.splines(f,i)-math.sin(i)))
    fl.write("\t")
    fl.write(str(ex.leastsquares(f,i,10)-math.sin(i)))
    fl.write("\n")
