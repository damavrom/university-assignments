import math,ex
a=[]
a+=[0.488]
a+=[2.978]
a+=[-1.894]
a+=[-1.088]
a+=[-1.090]
a+=[0.884]
a+=[1.849]
a+=[0.290]
a+=[-0.160]
a+=[1.917]
a+=[math.pi]
a+=[-math.pi]
f=set()
for i in a:
    f=f|{(i,math.sin(i))}

def func(x):
    return ex.lagrange(f,x)

print("Intergal with trapezioum rule")
print(ex.trapezioumRule(func,100,0,math.pi/2))
print("Intergal with the Simpson method")
print(ex.simpson(func,100,0,math.pi/2))
