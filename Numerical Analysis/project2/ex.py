def lagrange(f,x):
#Αύτη τη συνάρτηση την προγραμμάτησα έτσι ώστε να δέχεται ως είσοδο ένα σύνολο διατεταγμένων ζέυγων f και έναν πραγματικό αρίθμο. Πολύ πιθανό είναι να τρέξει και για άλλες δομές δεδομένων
    p=0
    for u in f:
        L=1
        for i in f-{u}:
            L*=(x-i[0])/(u[0]-i[0])
        p+=u[1]*L
    return p 

def splines(f,x):
    from scipy.interpolate import interp1d
    f=sorted(f)
    u1=[]
    u2=[]
    for u in f:
        u1+=[u[0]]
        u2+=[u[1]]
    return float(interp1d(u1,u2,kind='cubic')(x))

def leastsquares(f,x,d):
    from numpy import matrix
    A=[]
    b=[]
    for u in f:
        temp=[]
        for i in range(d):
            temp+=[u[0]**i]
        b+=[[u[1]]]
        A+=[temp]
    A=matrix(A)
    b=matrix(b)
    M=A.transpose()*A
    m=A.transpose()*b
    res=0
    p=M.getI()*m
    for i in range(len(p)):
        res+=float(p[i])*x**i
    return res

def trapezioumRule(f,N,a,b):
    s=0
    xi=a
    for i in range(1,N+1):
        xiprev=xi
        xi=a+i*(b-a)/N
        s+=(f(xiprev)+f(xi))*(xi-xiprev)/2
    return s

def simpson(f,N,a,b):
    if N%2==0:
        N+=1
    s1=0
    s2=0
    for i in range(1,N,2):
        s1+=f(a+i*(b-a)/N)
        s2+=f(a+(i+1)*(b-a)/N)
    return (b-a)*(f(a)+f(b)+2*s1+4*s2)/(3*N)
