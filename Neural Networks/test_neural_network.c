#include "neural_networks.c"
#include "mnist.c"
void getDouble(double **a, uint8_t **b,int n1,int n2)
{
        for (int i=0;i<n1;i++)
        {
                a[i]=(double*)malloc(sizeof(double)*n2);
                for (int j=0;j<n2;j++)
                        a[i][j]=(double)b[i][j]/256;
        }
}
void digToBin(const uint8_t a, double *b)
{
        for (int i=0;i<10;i++)
                b[i]=0;
        b[a%10]=1;
}
int outputToInt(double *out)
{
        double max=0;
        int maxDig=0;
        for (int i=0;i<10;i++)
                if (max<out[i])
                {
                        max=out[i];
                        maxDig=i;
                }
        return maxDig;
}
int main()
{
        printf(" ## Initializing Neural Network ##\n");
        int layers=2;
        int neurons[2]={784,10};
        printf(" -- layers:%d -- neurons in each layer:",layers);
        printf("%d",neurons[0]);
        for (int i=1;i<layers;i++)
                printf(", %d",neurons[i]);
        printf(" --\n");
        double *biases[layers-1];
        double **weights[layers-1];
        init(layers,neurons,biases,weights);
        printf("\n");

        printf(" ## Reading MNIST database ##\n");
        int d=28*28;
        double *in=(double*)malloc(sizeof(double)*d);
        double out[10];

        int sizeTrain=60000;
	double **imagesTrain=(double**)malloc(sizeof(double*)*sizeTrain);
	uint8_t **imagesTrainTemp=(uint8_t**)malloc(sizeof(uint8_t*)*sizeTrain);
	readImages1d("train-images-idx3-ubyte",imagesTrainTemp,sizeTrain,d);
        getDouble(imagesTrain,imagesTrainTemp,sizeTrain,d);
	uint8_t labelsTrain[sizeTrain];
	readLabels("train-labels-idx1-ubyte",labelsTrain,sizeTrain);

	int sizeTest=10000;
	double **imagesTest=(double**)malloc(sizeof(double*)*sizeTest);
	uint8_t **imagesTestTemp=(uint8_t**)malloc(sizeof(uint8_t*)*sizeTest);
	readImages1d("t10k-images-idx3-ubyte",imagesTestTemp,sizeTest,d);
        getDouble(imagesTest,imagesTestTemp,sizeTest,d);
	uint8_t labelsTest[sizeTest];
	readLabels("t10k-labels-idx1-ubyte",labelsTest,sizeTest);
        printf(" -- train samples:%d -- test samples:%d\n",sizeTrain,sizeTest);
        printf("\n");

        printf(" ## Neural Network Training ##\n");
        int epochs=3000;
        printf(" -- epochs:%d --\n",epochs);
        printf("\n");

	int *teach=(int*)malloc(sizeof(int)*sizeTrain);
	int *taught=(int*)malloc(sizeof(int)*sizeTrain);
        for (int j=0;j<sizeTrain;j++){
                teach[j]=0;
                taught[j]=0;
        }
        double beta=.1;
        for (int i=0;i<epochs;i++)
        {
                printf(" ## Epoch %d ##\n",i+1);
                printf("shuffling...\r");
                fflush(stdout);
                for (int j=0;j<sizeTrain;j++)
                {
                        int k=rand()%sizeTrain;
                        double* tempIm=imagesTrain[k];
                        imagesTrain[k]=imagesTrain[j];
                        imagesTrain[j]=tempIm;
                        uint8_t tempLa=labelsTrain[k];
                        labelsTrain[k]=labelsTrain[j];
                        labelsTrain[j]=tempLa;
                        int tempTe=teach[k];
                        teach[k]=teach[j];
                        teach[j]=tempTe;
                        tempTe=teach[k];
                        teach[k]=teach[j];
                        teach[j]=tempTe;
                }
                
                int missTrain=0;

                int samp=100;
                clock_t timer[samp];
                clock_t start=clock();
                for (int j=0;j<samp;j++)
                        timer[j]=clock();
                for (int j=0;j<sizeTrain;j++)
                {
                        if (teach[j]<=i|i%100==0){
                        if (1){
                                feed(layers,neurons,biases,weights,imagesTrain[j],out);
                                if (labelsTrain[j]!=outputToInt(out)){
                                        teach[j]=i;
                                        missTrain++;
                                }
                                else
                                        //teach[j]=1+i+(i-taught[j])/10;
                                        teach[j]=1+i;
                        }
			teach[j]=0;
                        timer[j%samp]=clock();
                        if (j%2000==0)
                        {
                                int k=1;
                                if (j<samp)
                                        k=samp-j-1;
                                int eta=(timer[j%samp]-timer[(j+k)%samp])*(sizeTrain-j)/(CLOCKS_PER_SEC*(samp-k));
                                int etaH=eta/(60*60);
                                int etaM=(eta%(60*60))/60;
                                int etaS=eta%(60);
                                float missTrainPer=100*(float)missTrain/(j+1);
                                printf(" -- training miss rate:%.2f -- ",missTrainPer);
                                printf("epoch eta:");
                                if (etaH>0)
                                        printf(" %dh",etaH);
                                if (etaM>0)
                                        printf(" %dm",etaM);
                                if (etaS>0)
                                        printf(" %ds",etaS);
                                printf(" --        \r");
                                fflush(stdout);
                        }
                        if (teach[j]<=i){
                                digToBin(labelsTrain[j],out);
                                train(layers,neurons,biases,weights,imagesTrain[j],out,.1);
                                taught[j]=i;
                        }
                }
                float missTrainPer=100*(float)missTrain/sizeTrain;
                beta=5.0*missTrain/sizeTrain;
                if (beta>.1){
                        beta=.1;
                }
                printf(" -- training miss rate:%.2f -- ",missTrainPer);
                fflush(stdout);
                int missTest[10];
                for (int j=0;j<10;j++)
                        missTest[j]=0;
                float missTestPer;
                if (i>epochs-10|i%10==2)
                {
                        for (int j=0;j<sizeTest;j++)
                        {
                                feed(layers,neurons,biases,weights,imagesTest[j],out);
                                double max=0;
                                int maxDig=0;
                                int cnt=0;
                                for (int l=0;l<10;l++)
                                        if (out[labelsTest[j]]<out[l])
                                                cnt++;
                                missTest[cnt]++;
                        }
                        missTestPer=100-100*(float)missTest[0]/sizeTest;
                        printf(" testing miss rate:%.2f -- ",missTestPer);
                }
                int dur=(clock()-start)/CLOCKS_PER_SEC;
                int durH=dur/(60*60);
                int durM=(dur%(60*60))/60;
                int durS=dur%(60);
                printf("epoch duration:");
                if (durH>0)
                        printf(" %dh",durH);
                if (durM>0)
                        printf(" %dm",durM);
                if (durS>0)
                        printf(" %ds",durS);
                printf(" --        \r");
                printf("\n");
                printf("\n");

                FILE *logFile=fopen("log.txt","w");
                //fprintf(logFile,"%d %f ",i+1,100-missTrainPer);
                for (int j=0;j<=i;j++)
                {
                        int cnt=0;
                        //missTestPer=100*(float)missTest[j]/sizeTest;
                        for (int l=0;l<sizeTrain;l++)
                                if (j==taught[l])
                                        cnt++;
                        fprintf(logFile,"%d %d\n",j,cnt);
                }
                //fprintf(logFile,"\n");
                fclose(logFile);
        }
}
