#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

double sigmoid(const double x)
{
        return 1/(1+exp(-x));
}
double sigmoidDerivative(const double x)
{
        return sigmoid(x)*(1-sigmoid(x));
}
double ReLU(const double x)
{
        if (x>0)
                return x;
        return x*.1;
}
double ReLUDerivative(const double x)
{
        if (x>0)
                return 1;
        return .1;
}
int init(const int n, const  int *layersize, double **biases, double ***weights)
{
        for (int i=0;i<n;i++)
        srand(time(0));
        for (int i=0;i<n-1;i++)
        {
                biases[i]=(double*)malloc(sizeof(double)*layersize[i+1]);
                weights[i]=(double**)malloc(sizeof(double*)*layersize[i+1]);
                for (int j=0;j<layersize[i+1];j++)
                {
                        double a=1;
                        biases[i][j]=a*(double)rand()/RAND_MAX-a/2;
                        weights[i][j]=(double*)malloc(sizeof(double)*layersize[i]);
                        for (int k=0;k<layersize[i];k++)
                                weights[i][j][k]=a*(double)rand()/RAND_MAX-a/2;
                }
        }
        return 1;
}
int feed(const int n, const int *layersize, double **biases, double ***weights, const double *input, double *output)
{
        double **y=(double**)malloc(sizeof(double*)*n);
        int max=0;
        for (int i=0;i<n;i++)
                if (layersize[i]>max)
                        max=layersize[i];
        y[0]=(double*)malloc(sizeof(double)*max);
        for (int i=0;i<layersize[0];i++)
                y[0][i]=input[i];
        for (int i=1;i<n;i++)
        {
                y[i]=(double*)malloc(sizeof(double)*max);
                for (int j=0;j<layersize[i];j++)
                {
                        double sum=0;
                        for (int k=0;k<layersize[i-1];k++)
                        sum+=weights[i-1][j][k]*y[i-1][k];
                y[i][j]=sigmoid(sum-biases[i-1][j]);
                }
        }
        for (int i=0;i<layersize[n-1];i++)
                output[i]=y[n-1][i];
        for (int i=0;i<n;i++)
                free(y[i]);
        return 1;
}

int train(const int n, const int *layersize, double **biases, double ***weights, const double *input, const double *output, const double step)
{
        //double step=.1;
        double **y=(double**)malloc(sizeof(double*)*n);
        double **z=(double**)malloc(sizeof(double*)*n);
        int max=0;
        for (int i=0;i<n;i++)
                if (layersize[i]>max)
                        max=layersize[i];
        y[0]=(double*)malloc(sizeof(double)*max);
        for (int i=0;i<layersize[0];i++)
                y[0][i]=input[i];
        for (int i=1;i<n;i++)
        {
                y[i]=(double*)malloc(sizeof(double)*max);
                z[i]=(double*)malloc(sizeof(double)*max);
                for (int j=0;j<layersize[i];j++)
                {
                        double sum=0;
                        for (int k=0;k<layersize[i-1];k++)
                                sum+=weights[i-1][j][k]*y[i-1][k];
                        z[i][j]=sum-biases[i-1][j];
                        y[i][j]=sigmoid(z[i][j]);
                }
        }

        double **delta=(double**)malloc(sizeof(double*)*(n-1));
        delta[n-2]=(double*)malloc(sizeof(double)*max);
        for (int j=0;j<layersize[n-1];j++)
                delta[n-2][j]=(y[n-1][j]-output[j])*sigmoidDerivative(z[n-1][j]);

        for (int i=n-3;i>=0;i--)
        {
                delta[i]=(double*)malloc(sizeof(double)*max);
                for (int j=0;j<layersize[i];j++)
                {
                        double sum=0;
                        for (int k=0;k<layersize[i+1];k++)
                                sum+=weights[i][k][j]*delta[i+1][k];
                        delta[i][j]=sum*sigmoidDerivative(z[i+1][j]);
                }
        }
        for (int i=0;i<n-1;i++)
                for (int j=0;j<layersize[i+1];j++)
                {
                        biases[i][j]=-delta[i][j]*step;
                        for (int k=0;k<layersize[i];k++)
                                weights[i][j][k]-=y[i][k]*delta[i][j]*step;
                }
        for (int i=0;i<n;i++)
                free(y[i]);
        for (int i=1;i<n;i++)
                free(z[i]);
        for (int i=0;i<n-1;i++)
                free(delta[i]);
        return 1;
}
double quadraticCost(const int n, const int *layersize, double **biases, double ***weights, const double *input, const double *output)
{
        double sum=0;
        double *y=(double*)malloc(sizeof(double)*layersize[n-1]);
        feed(n,layersize,biases,weights,input,y);
        for (int i=0;i<layersize[n-1];i++)
                sum+=pow(y[i]-output[i],2);
        return sqrt(sum);
}
void testPrint(const int n, const int *layersize, double **biases, double ***weights)
{
        printf(" -- network test dump --\n");
        printf("number of layers:%d\n",n);
        printf("number of neurons in every layer:");
        for (int i=0;i<n;i++)
                printf(" %d",layersize[i]);
        printf("\n");
        for (int i=0;i<n-1;i++)
        {
                for (int j=0;j<layersize[i+1];j++)
                {
                        printf("neuron %d-%d\n",i+2,j+1);
                        printf("weights:\t");
                        for (int k=0;k<layersize[i];k++)
                                printf(" %.2f",weights[i][j][k]);
                        printf("\n");
                        printf("bias:   \t");
                        printf(" %.2f",biases[i][j]);
                        printf("\n");
                }
        }
}
