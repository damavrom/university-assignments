#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact integral type
#include <CGAL/MP_Float.h>
typedef CGAL::MP_Float ET;

// program and solution types
typedef CGAL::Quadratic_program<double> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int train(const int n, double *b, double *w, double **x, const double *y, const int m)
{

        double C=.0000001;
        Program qp (CGAL::EQUAL, true, 0, true, C); 

        std::cout <<"fixing matrices and vectors..."<<std::endl;
        double **yyT;
        double **K;
        yyT=new double*[m];
        K=new double*[m];
        
        for (int i=0;i<m;i++)
                yyT[i]=new double[m];
        for (int i=0;i<m;i++)
                for (int j=0;j<=i;j++)
                        yyT[i][j]=y[i]*y[j];
        for (int i=0;i<m;i++)
                K[i]=new double[m];
        for (int i=0;i<m;i++)
                for (int j=0;j<=i;j++)
                {
                        K[i][j]=0;
                        for (int k=0;k<n;k++)
                                K[i][j]+=x[i][k]*x[j][k];
                }
        std::cout <<"loading matrices and vectors on the qp solver..."<<std::endl;
        for (int i=0;i<m;i++)
                for (int j=0;j<=i;j++)
                        qp.set_d(i,j,yyT[i][j]*K[i][j]);
        for (int i=0;i<m;i++)
        {
                qp.set_c(i,-1);

                qp.set_a(i,0,y[i]);
                qp.set_r(i,CGAL::EQUAL);
                //qp.set_b(i,0);

                qp.set_l(i,true,0);
                qp.set_u(i,true,C);
        }
        
        std::cout <<"solving qp problem..."<<std::endl;
        Solution s = CGAL::solve_quadratic_program(qp, ET());
        assert (s.solves_quadratic_program(qp));
        //std::cout << s; 

        std::cout <<"setting weights..."<<std::endl;
        int i=0;
        for (int j=0;j<n;j++)
                w[j]=0;
        for (Solution::Variable_value_iterator it=s.variable_values_begin();it!=s.variable_values_end();++it)
        {
                double tmp=CGAL::to_double(*it);
                if (tmp!=0)
                {
                        for (int j=0;j<n;j++)
                                w[j]+=tmp*y[i]*x[i][j];
                }
                i++;
                //std::cout <<i<<" out of "<<m<<" multipliers"<<std::endl;
        }

        std::cout <<"setting bias..."<<std::endl;
        *b=0;
        int cnt=0;
        i=0;
        for (Solution::Variable_value_iterator it=s.variable_values_begin();it!=s.variable_values_end();++it)
        {
                if (CGAL::to_double(*it)!=0)
                {
                        *b+=y[i];
                        for (int j=0;j<n;j++)
                                *b-=x[i][j]*w[j];
                        cnt++;
                }
                i++;
        }
        *b/=cnt;
        return 0;
}
int feed(const int d, double bias, double *weights, const double *input)
{
        double sum=0;
        for (int i=0;i<d;i++)
                sum+=weights[i]*input[i];
        sum+=bias;
        if (sum>0)
                return 1;
        else
                return -1;
}
void testPrint(const int d,double bias, double *weights)
{
        std::cout<<" -- svm test dump -- "<<std::endl;
        std::cout<<"dimention of input:"<<d<<std::endl;

        std::cout<<"weights of svm:";
        for (int i=0;i<d;i++)
                std::cout<<std::fixed<<" "<<std::setprecision(2)<<weights[i];
        std::cout<<std::endl;
        std::cout<<"bias: "<<bias<<std::endl;
}
