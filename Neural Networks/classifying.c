#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int min(const int a, const int b)
//returns the minimums of a and b
{
	if (a<b)
		return a;
	return b;
}

int distanceInt(const uint8_t *a, const int *ap, const uint8_t *b, const int *bp, const int d)
//returns the euclidean distance of a and b points in a d dimention space
//it is optimized for when many dimentions have value 0
{
	int dist=0;
	for (int i=0;i<d;i=min(ap[i],bp[i]))
		dist+=(a[i]-b[i])*(a[i]-b[i]);
	return dist;
}

double distanceFloat(const double *a, const uint8_t *b, const int d)
//returns the euclidean distance of a and b points in a d dimention space
{
	int dist=0;
	for (int i=0;i<d;i++)
		dist+=(a[i]-b[i])*(a[i]-b[i]);
	return dist;
}

int kNearest(uint8_t **data, int **pointers, const uint8_t *labels, const int d, const int n, const int k, const uint8_t *input, int *pointersIn)
//guesses the label of the input using the k-nearest algortithm
{
	//find k nearest neighbors
	int nearest[k];
	int distances[k];
	for (int i=0;i<k;i++)
		nearest[i]=-1;
	for (int i=0;i<n;i++)
	{
		int j=0;
		for (j=0;j<k;j++)
			if (nearest[j]>0)
			{
				int iin=distanceInt(data[i],pointers[i],input,pointersIn,d);
				int jin=distances[j];
				if (iin>jin)
					break;
			}
		j--;
		for (int k=0;k<j;k++)
		{
			nearest[k]=nearest[k+1];
			distances[k]=distances[k+1];
		}
		if (j>=0)
		{
			
			distances[j]=distanceInt(data[i],pointers[i],input,pointersIn,d);
			nearest[j]=i;
		}
	}
	//count labels of k nearest
	int cnt[k];
	for (int i=0;i<k;i++)
	{
		cnt[i]=0;
		for (int j=0;j<k;j++)
			if (labels[nearest[i]]==labels[nearest[j]])
				cnt[i]++;
	}
	//find most frequent
	int freq=0;
	for (int i=0;i<k;i++)
		if (cnt[i]>=cnt[freq])
			freq=i;

	return labels[nearest[freq]];
}
int countLabels(const uint8_t *labels, const int n)
//returns the number of all the distinct labels of the set
{
	int d[n];
	for (int i=0;i<n;i++)
		d[i]=labels[0];
	for (int i=0;i<n;i++)
	{
		if (labels[i]!=labels[0]);
		{
			for (int j=1;j<n;j++)
				if (labels[0]==d[j])
				{
					d[j]=labels[i];
					break;
				}
				else if (labels[i]==d[j])
					break;
		}
	}
	int cnt=0;
	for (int i=1;i<n;i++)
	{
		cnt++;
		if (labels[0]==d[i])
			break;
	}
	return cnt;
}
int getCendroids(uint8_t **data, const uint8_t *labels, const int d, const int numD, double **cendroids, int *cendroidLabels, const int numC)
//returns the cendroids and their labels
{
	for (int i=0;i<numC;i++)
	{
		cendroidLabels[i]=labels[0];
		cendroids[i]=(double *)malloc(sizeof(double)*d);
		for (int j=0;j<d;j++)
			cendroids[i][j]=0;
	}
	for (int i=0;i<numC;i++)
		cendroidLabels[i]=labels[0];
	for (int i=0;i<numD;i++)
		if (labels[i]!=labels[0])
			for (int j=0;j<numC;j++)
				if (labels[i]==cendroidLabels[j])
					break;
				else if (cendroidLabels[j]==labels[0])
				{
					cendroidLabels[j]=labels[i];
					break;
				}

	int cnt[numC];
	for (int i=0;i<numC;i++)
		cnt[i]=0;
	for (int i=0;i<numD;i++)
		for (int j=0;j<numC;j++)
			if (cendroidLabels[j]==labels[i])
			{
				cnt[j]++;
				for (int l=0;l<d;l++)
					cendroids[j][l]+=data[i][l];
			}
	for (int i=0;i<numC;i++)
		for (int j=0;j<d;j++)
			cendroids[i][j]/=cnt[i];
	return 0;
}
int nearestCendroid(double **cendroids, const int *labels, const int d, const int n, const uint8_t *input)
//guesses the label of the inpurt using the nearest-cendroid algorithm
{
	int nearest=0;
	for (int i=0;i<n;i++)
	{
		float iin=distanceFloat(cendroids[nearest],input,d);
		float jin=distanceFloat(cendroids[i],input,d);
		if (iin>jin)
			nearest=i;
	}
	return labels[nearest];
	return 1;
}
