#include "mnist.c"
#include "classifying.c"
#include "data_preprocessing.c"
#include <time.h>
#include <stdio.h>

int main()
{
	printf("testing nearest-cendroid algorithm\n");
	printf("reading data from nmist files...\n");
	int n1;
	n1=readNumberOfItems("train-images-idx3-ubyte");
	int resolution[2];
	readResolution("train-images-idx3-ubyte",resolution);
	uint8_t *imagesTrain[n1];
	readImages1d("train-images-idx3-ubyte",imagesTrain,n1,resolution[0]*resolution[1]);
	uint8_t labelsTrain[n1];
	readLabels("train-labels-idx1-ubyte",labelsTrain,n1);

	int n2=readNumberOfItems("t10k-images-idx3-ubyte");
	uint8_t *imagesTest[n2];
	readImages1d("t10k-images-idx3-ubyte",imagesTest,n2,resolution[0]*resolution[1]);
	uint8_t labelsTest[n2];
	readLabels("t10k-labels-idx1-ubyte",labelsTest,n2);

	printf("preprocessing data...\n");
	int numOfCendroids=countLabels(labelsTrain,n1);
	double *cendroids[numOfCendroids];
	int cendroidLabels[numOfCendroids];
	getCendroids(imagesTrain,labelsTrain,resolution[0]*resolution[1],n1,cendroids,cendroidLabels,numOfCendroids);
	
	printf("testing accuracy...\n");
	int miss=0;
	int samp=100;
	clock_t timer[samp];
	for (int i=0;i<samp;i++)
		timer[i]=clock();
	for (int i=0;i<n2;i++)
	{
		int guess=nearestCendroid(cendroids,cendroidLabels,resolution[0]*resolution[1],numOfCendroids,imagesTest[i]);
		if (guess!=labelsTest[i])
			miss++;
		float accuracy=1-(float)miss/i;
		float completed=(float)i/n2;
		timer[i%samp]=clock();
		int j=1;
		if (i<samp)
			j=samp-i-1;
		int eta=(timer[i%samp]-timer[(i+j)%samp])*(n2-i)/(CLOCKS_PER_SEC*(samp-j));
		int etaH=eta/(60*60);
		int etaM=(eta%(60*60))/60;
		int etaS=eta%(60);
		printf("-- completed:%.2f -- accuracy:%.2f -- ",100*completed,100*accuracy);
		printf("eta:",100*completed,100*accuracy);
		if (etaH>0)
			printf(" %dh",etaH);
		if (etaM>0)
			printf(" %dm",etaM);
		if (etaS>0)
			printf(" %ds",etaS);
		printf(" --        \r");
		fflush(stdout);
	}
	printf(" -- accuracy:%.3f --                          \n ",100*(1-(float)miss/n2));
}
