#include "mnist.c"
#include "classifying.c"
#include "data_preprocessing.c"
#include <time.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
        int d=28*28;
        int sizeTrain=60000;
	uint8_t **imagesTrain=(uint8_t**)malloc(sizeof(uint8_t*)*sizeTrain);
	readImages1d("train-images-idx3-ubyte",imagesTrain,sizeTrain,d);
	uint8_t *labelsTrain=(uint8_t*)malloc(sizeof(uint8_t)*sizeTrain);
	readLabels("train-labels-idx1-ubyte",labelsTrain,sizeTrain);

//        int sizeOneTwoTrain=0;
//        for (int i=0;i<sizeTrain;i++)
//                if (labelsTrain[i]==3)
//                        sizeOneTwoTrain++;
//                else if (labelsTrain[i]==4)
//                        sizeOneTwoTrain++;
//	uint8_t *imagesOneTwoTrain[sizeOneTwoTrain];
//	uint8_t labelsOneTwoTrain[sizeOneTwoTrain];
//        int cnt=0;
//        for (int i=0;i<sizeTrain;i++)
//                if (labelsTrain[i]==3)
//                {
//                        imagesOneTwoTrain[cnt]=imagesTrain[i];
//                        labelsOneTwoTrain[cnt]=3;
//                        cnt++;
//                }
//                else if (labelsTrain[i]==4)
//                {
//                        imagesOneTwoTrain[cnt]=imagesTrain[i];
//                        labelsOneTwoTrain[cnt]=4;
//                        cnt++;
//                }


	int sizeTest=10000;
	uint8_t *imagesTest[sizeTest];
	readImages1d("t10k-images-idx3-ubyte",imagesTest,sizeTest,d);
	uint8_t *labelsTest=(uint8_t*)malloc(sizeof(uint8_t)*sizeTest);
       	readLabels("t10k-labels-idx1-ubyte",labelsTest,sizeTest);

//        int sizeOneTwoTest=0;
//        for (int i=0;i<sizeTest;i++)
//                if (labelsTest[i]==3)
//                        sizeOneTwoTest++;
//                else if (labelsTest[i]==4)
//                        sizeOneTwoTest++;
//	uint8_t *imagesOneTwoTest[sizeOneTwoTest];
//	uint8_t labelsOneTwoTest[sizeOneTwoTest];
//        cnt=0;
//        for (int i=0;i<sizeTest;i++)
//                if (labelsTest[i]==3)
//                {
//                        imagesOneTwoTest[cnt]=imagesTest[i];
//                        labelsOneTwoTest[cnt]=3;
//                        cnt++;
//                }
//                else if (labelsTest[i]==4)
//                {
//                        imagesOneTwoTest[cnt]=imagesTest[i];
//                        labelsOneTwoTest[cnt]=4;
//                        cnt++;
//                }

	
	int k=3;
	printf("testing %d-nearest-neighbor algorithm\n",k);
	printf("preprocessing data...\n");
	int **pointersTrain=(int**)malloc((sizeof(int*)*sizeTrain));
	for (int i=0;i<sizeTrain;i++)
	{
		pointersTrain[i]=(int*)malloc((sizeof(int)*d));
		getPointers(imagesTrain[i],d,pointersTrain[i]);
	}

        printf("%d\n",sizeTrain);
	int **pointersTest=(int**)malloc((sizeof(int*)*sizeTest));
	for (int i=0;i<sizeTest;i++)
	{
		pointersTest[i]=(int*)malloc((sizeof(int)*d));
		getPointers(imagesTest[i],d,pointersTest[i]);
	}
	
	printf("testing accuracy...\n");
	int miss=0;
	int samp=100;
	clock_t timer[samp];
	for (int i=0;i<samp;i++)
		timer[i]=clock();
	for (int i=0;i<sizeTest;i++)
	{
		int guess=kNearest(imagesTrain,pointersTrain,labelsTrain,d,sizeTrain,k,imagesTest[i],pointersTest[i]);
		if (guess!=labelsTest[i])
			miss++;
		float accuracy=1-(float)miss/i;
		float completed=(float)i/sizeTest;
		timer[i%samp]=clock();
		int j=1;
		if (i<samp)
			j=samp-i-1;
		int eta=(timer[i%samp]-timer[(i+j)%samp])*(sizeTest-i)/(CLOCKS_PER_SEC*(samp-j));
		int etaH=eta/(60*60);
		int etaM=(eta%(60*60))/60;
		int etaS=eta%(60);
		printf("-- completed:%.2f -- miss:%.2f -- ",100*completed,100-100*accuracy);
		printf("eta:",100*completed,100*accuracy);
		if (etaH>0)
			printf(" %dh",etaH);
		if (etaM>0)
			printf(" %dm",etaM);
		if (etaS>0)
			printf(" %ds",etaS);
		printf(" --        \r");
		fflush(stdout);
	}
	printf(" -- accuracy:%.3f --                          \n ",100*(1-(float)miss/sizeTest));
}
