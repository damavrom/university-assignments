#include "neural_networks.c" 
#include "classifying.c" 

double distanceAux(const double *a, const double *b, int x, int y){
	double dist=0;
	int si=0;
	int ei=28;
	int sj=0;
	int ej=28;
	if (x<0)
		si=x;
	else
		ei=28+x;
	if (y<0)
		sj=y;
	else
		ej=28+y;
	for (int i=si;i<ei;i++)
		for (int j=sj;j<ej;j++){
			if (i<0|j<0)
				dist+=(b[28*(i+x)+j+y])*(b[28*(i+x)+j+y]);
			else if (i>=28|j>=28)
				dist+=(b[28*(i+x)+j+y])*(b[28*(i+x)+j+y]);
			else if (i+x<0|j+y<0)
				dist+=(a[28*i+j])*(a[28*i+j]);
			else if (i+x>=28|j+y>=28)
				dist+=(a[28*i+j])*(a[28*i+j]);
			else
				dist+=(a[28*i+j]-b[28*(i+x)+j+y])*(a[28*i+j]-b[28*(i+x)+j+y]);
	}
	return dist;
}
double distance(const double *a, const double *b, const int d){
	double min=distanceAux(a,b,0,0);
	//return min;
	int x=0;
	int y=0;
	int mx=0;
	int my=0;
	int flag=1;
	int cnt=0;
	while (flag){
		x=mx;
		y=my;
		flag=0;
		for (int i=x-1;i<x+2;i+=2)
				if (i>0 & i<28)
					if (distanceAux(a,b,i,y)<min){
						min=distanceAux(a,b,i,y);
						flag=1;
						mx=i;
						my=y;
					}
		for (int i=y-1;i<y+2;i+=2)
				if (i>0 & i<28)
					if (distanceAux(a,b,x,i)<min){
						min=distanceAux(a,b,x,i);
						flag=1;
						mx=x;
						my=i;
					}
		if (cnt>3)
			return sqrt(min);
		cnt++;
	}
//	if (cnt>15)
//	printf("       %d\n",cnt);
//	if (min==0)
//		printf("%f \n",min);
	return sqrt(min);
}

double RBF(int d, double *x, double *m, double b){
	double dist=distance(x,m,d);
//	if (dist>sqrt(6.6/b));
//		return 0;
        return exp(-pow(b*dist,2));
}

void k_random(int k, double **set, int d, int n, double **centers){
        srand(time(0));
        for (int i=0;i<k;i++){
                centers[i]=(double *)malloc(sizeof(double)*d);
                int r=rand()%n;
                for (int j=0;j<d;j++)
                        centers[i][j]=set[r][j];
        }
}
struct RBFneuron
{
	double *center;
	double beta;
	struct RBFneuron *next;
};
struct RBFneuron *getCBaux(double **set,uint8_t *labels,int d,int n, double **dig, double *minDist, int size, int label)
{
	if (size==0)
		return 0;
	printf("%d\n",size);
	double c=0.5;
	double c2=c+0.05;
	struct RBFneuron *neuron=(struct RBFneuron *)malloc(sizeof(struct RBFneuron));
	int maxCenter;
	double maxDist=0;
	for (int i=0;i<size;i++){
		if (minDist[i]>maxDist){
			maxDist=minDist[i];
			maxCenter=i;
		}
	}
	neuron->beta=sqrt(-log(c)/pow(maxDist,2));
	//printf("%f\n",maxDist);
        neuron->center=(double *)malloc(sizeof(double)*d);
	for (int i=0;i<d;i++)
		neuron->center[i]=dig[maxCenter][i];
	int size2=0;
	for (int j=0;j<size;j++)
		if (RBF(d,dig[j],dig[maxCenter],neuron->beta)<c2)
			size2++;
	double *dig2[size2];
	double minDist2[size2];

	//printf("test\n");
	int cnt=0;
	for (int i=0;i<size;i++){
		if (RBF(d,dig[i],dig[maxCenter],neuron->beta)<c2){
			dig2[cnt]=(double *)malloc(sizeof(double)*d);
			minDist2[cnt]=minDist[i];
			for (int j=0;j<d;j++)
				dig2[cnt][j]=dig[i][j];
			cnt++;
		}
	}
	for (int i=0;i<size;i++){
		free(dig[i]);
	}
	//free(minDist);
	neuron->next=getCBaux(set,labels,d,n,dig2,minDist2,size2,label);
	return neuron;
}
struct RBFneuron *getCBaux2(double **set,uint8_t *labels,int d,int n, double **dig, double *minDist, int size, int label)
{
	printf("%d\n",size);
	double c=0.5;
	double c2=c+0.05;
	struct RBFneuron *neuron=(struct RBFneuron *)malloc(sizeof(struct RBFneuron));
	if (size==1){
		neuron->beta=-log(c)/pow(minDist[0],2);
		//printf("%f\n",neuron->beta);
		neuron->center=(double *)malloc(sizeof(double)*d);
		for (int i=0;i<d;i++)
			neuron->center[i]=dig[0][i];
		neuron->next=0;
		return neuron;
	}
	//printf("test\n");
	int maxCenter;
	int maxCnt=0;
	int cnt;
	for (int i=0;i<size;i++){
		cnt=0;
		for (int j=0;j<size;j++)
			if (RBF(d,dig[j],dig[i],-log(c)/pow(minDist[i],2))>c2)
				cnt++;
		if (maxCnt<cnt){
			maxCnt=cnt;
			maxCenter=i;
		}
	}
//	if (i%100==0)
//		printf("%d\n",i);
	neuron->beta=-log(c)/pow(minDist[maxCenter],2);
	//printf("%f\n",neuron->beta);
        neuron->center=(double *)malloc(sizeof(double)*d);
	for (int i=0;i<d;i++)
		neuron->center[i]=dig[maxCenter][i];
	double *dig2[size-maxCnt];
	double minDist2[size-maxCnt];

	cnt=0;
	for (int i=0;i<size;i++){
		if (RBF(d,dig[i],dig[maxCenter],neuron->beta)<c2){
			dig2[cnt]=(double *)malloc(sizeof(double)*d);
			minDist2[cnt]=minDist[i];
			for (int j=0;j<d;j++)
				dig2[cnt][j]=dig[i][j];
			cnt++;
		}
	}
	for (int i=0;i<size;i++){
		free(dig[i]);
	}
	//free(minDist);
	neuron->next=getCBaux2(set,labels,d,n,dig2,minDist2,size-maxCnt,label);
	return neuron;
}
void getCentersBetas(double **set,uint8_t *labels, int d, int n, struct RBFneuron **neurons)
{
	int k[10];
	for (int i=0;i<10;i++){
		int size=0;
		for (int j=0;j<n;j++)
			if (labels[j]==i)
				size++;
		double *dig[size];
		int cnt=0;
                for (int j=0;j<n;j++){
                        if (labels[j]==i){
				dig[cnt]=(double *)malloc(sizeof(double)*d);
				for (int k=0;k<d;k++)
					dig[cnt][k]=set[j][k];
                                cnt++;
                        }
		}
		double minDist[size];
		for (int m=0;m<size;m++){
			minDist[m]=0;
			for (int j=0;j<n;j++)
				if (labels[j]!=i){
					if (minDist[m]==0){
						minDist[m]=distance(set[j],dig[m],d);
					}
					else if (minDist[m]>distance(set[j],dig[m],d)){
						minDist[m]=distance(set[j],dig[m],d);
					}
				}
		}
		neurons[i]=getCBaux(set,labels,d,n,dig,minDist,size,i);
		printf("%d\n",i);
	}
}
void k_means(int k, double **set, int d, int n, double **centers){
        k_random(k,set,d,n,centers);
        int flag=0;
        int cnt=0;
        double **sums=(double **)malloc(sizeof(double *)*k);
        int *nums=(int *)malloc(sizeof(int)*k);
        for (int i=0;i<k;i++)
                sums[i]=(double *)malloc(sizeof(double)*d);
        while (flag==0){
                int maxNums=0;
                int maxP=0;
                for (int i=0;i<k;i++){
                        nums[i]=0;
                        for (int j=0;j<d;j++)
                                sums[i][j]=0;
                }
                for (int i=0;i<n;i++){
                        double dist;
                        int closest=0;
                        double min=distance(set[i],centers[0],d);
                        for (int j=1;j<k;j++){
                                dist=distance(set[i],centers[j],d);
                                if (dist<min){
                                        min=dist;
                                        closest=j;
                                }
                        }
                        for (int j=0;j<d;j++)
                                sums[closest][j]+=set[i][j];
                        nums[closest]++;
                }
                flag=1;
                for (int i=0;i<k;i++)
                        for (int j=0;j<d;j++)
                                if (nums[i]!=0){
                                        if (nums[i]>=maxNums){
                                                maxNums=nums[i];
                                                maxP=i;
                                        }
                                        if (centers[i][j]!=sums[i][j]/nums[i]){
                                                centers[i][j]=sums[i][j]/nums[i];
                                                flag=0;
                                        }
                                }
                //for (int i=0;i<k;i++)
                        //printf("%d ",nums[i]);
                //printf("\n");
                //testDumb(centers[maxP]);
                if (cnt>=10)
                        break;
                cnt++;
        }
}

double deviation(double **set, int d, int n, double *center){
        double sum=0;
        for (int i=0;i<n;i++)
                sum+=distance(center,set[i],d);
        return sum/n;
}

void getBetas(int k, double **set, int d, int n, double **centers, double *betas){
        for (int i=0;i<k;i++)
                betas[i]=1/(2*pow(deviation(set,d,n,centers[i]),2));
                //betas[i]=.0009;
}

void getBetas2(int k, double **set, int d, int n, double *betas){
        double *center=(double *)malloc(sizeof(double)*d);
        k_means(1,set,d,n,&center);
        for (int i=0;i<k;i++)
                betas[i]=1/(2*pow(deviation(set,d,n,center),2));
}

void feedRBF(int k, int d, double **centers, double *betas, double *input, double *output){
        for (int i=0;i<k;i++)
                output[i]=RBF(d,input,centers[i],betas[i]);
}

void testDumb(double *image){
        for (int i=0;i<28;i++){
                for (int j=0;j<28;j++)
                        if (image[i*28+j]<1.0/4)
                                printf("  ");
                        else if (image[i*28+j]<2.0/4)
                                printf(". ");
                        else if (image[i*28+j]<3.0/4)
                                printf("* ");
                        else
                                printf("# ");
                printf("\n");
        }
}
