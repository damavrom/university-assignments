#include "svm.cpp"
#include "mnist.c"
void getDouble(double **a, uint8_t **b,int n1,int n2)
{
        for (int i=0;i<n1;i++)
        {
                a[i]=(double*)malloc(sizeof(double)*n2);
                for (int j=0;j<n2;j++)
                        a[i][j]=(double)b[i][j];
        }
}

int main()
{
        std::cout<<" ## Problem Description ##";
        std::cout<<std::endl;
        std::cout<<" -- classify digits of 3 and 4 --";
        std::cout<<std::endl;
        std::cout<<std::endl;
        std::cout<<" ## Reading MNIST database ##";
        std::cout<<std::endl;
        int d=28*28;

        int sizeTrain=60000;
	double **imagesTrain=new double*[sizeTrain];
	uint8_t **imagesTrainTemp=new uint8_t*[sizeTrain];
	readImages1d("train-images-idx3-ubyte",imagesTrainTemp,sizeTrain,d);
        getDouble(imagesTrain,imagesTrainTemp,sizeTrain,d);
	double labelsTrain[sizeTrain];
	uint8_t labelsTrainTemp[sizeTrain];
	readLabels("train-labels-idx1-ubyte",labelsTrainTemp,sizeTrain);
        for (int i=0;i<sizeTrain;i++)
                if (labelsTrainTemp[i]%2==0)
                        labelsTrain[i]=1;
                else
                        labelsTrain[i]=-1;

        int sizeOneTwoTrain=0;
        for (int i=0;i<sizeTrain;i++)
                if (labelsTrainTemp[i]==3)
                        sizeOneTwoTrain++;
                else if (labelsTrainTemp[i]==4)
                        sizeOneTwoTrain++;
	double **imagesOneTwoTrain=new double*[sizeOneTwoTrain];
	double *labelsOneTwoTrain=new double[sizeOneTwoTrain];
        int cnt=0;
        for (int i=0;i<sizeTrain;i++)
                if (labelsTrainTemp[i]==3)
                {
                        imagesOneTwoTrain[cnt]=imagesTrain[i];
                        labelsOneTwoTrain[cnt]=1;
                        cnt++;
                }
                else if (labelsTrainTemp[i]==4)
                {
                        imagesOneTwoTrain[cnt]=imagesTrain[i];
                        labelsOneTwoTrain[cnt]=-1;
                        cnt++;
                }

	int sizeTest=10000;
	double **imagesTest=new double*[sizeTest];
	uint8_t **imagesTestTemp=new uint8_t*[sizeTest];
	readImages1d("t10k-images-idx3-ubyte",imagesTestTemp,sizeTest,d);
        getDouble(imagesTest,imagesTestTemp,sizeTest,d);
	uint8_t labelsTest[sizeTest];
	readLabels("t10k-labels-idx1-ubyte",labelsTest,sizeTest);

        int sizeOneTwoTest=0;
        for (int i=0;i<sizeTest;i++)
                if (labelsTest[i]==3)
                        sizeOneTwoTest++;
                else if (labelsTest[i]==4)
                        sizeOneTwoTest++;
	double **imagesOneTwoTest=new double*[sizeOneTwoTest];
	double *labelsOneTwoTest=new double[sizeOneTwoTest];
        cnt=0;
        for (int i=0;i<sizeTest;i++)
                if (labelsTest[i]==3)
                {
                        imagesOneTwoTest[cnt]=imagesTest[i];
                        labelsOneTwoTest[cnt]=1;
                        cnt++;
                }
                else if (labelsTest[i]==4)
                {
                        imagesOneTwoTest[cnt]=imagesTest[i];
                        labelsOneTwoTest[cnt]=-1;
                        cnt++;
                }
        sizeOneTwoTrain=900;

        std::cout<<" -- train samples:";
        std::cout<<sizeOneTwoTrain;
        std::cout<<" -- test samples:";
        std::cout<<sizeOneTwoTest;
        std::cout<<" -- ";
        std::cout<<std::endl;
        std::cout<<std::endl;

        std::cout<<" ## SVM Training ##";
        std::cout<<std::endl;
        clock_t timer=clock();
        double b;
        double *w=new double[d];
        train(d,&b,w,imagesOneTwoTrain,labelsOneTwoTrain,sizeOneTwoTrain);
        std::cout<<" -- duration:";
        std::cout<<(clock()-timer)/CLOCKS_PER_SEC;
        std::cout<<" sec -- ";
        std::cout<<std::endl;
        std::cout<<std::endl;
        //std::cout<<" ## SVM Parametres ##";
        //testPrint(d,b,w);
        //std::cout<<std::endl;

        std::cout<<" ## SVM Testing ##";
        std::cout<<std::endl;
        int missTest=0;
        for (int i=0;i<sizeOneTwoTest;i++)
                if (labelsOneTwoTest[i]!=feed(d,b,w,imagesOneTwoTest[i]))
                        missTest++;
        std::cout<<" -- testing miss rate:"<<100*(float)missTest/sizeOneTwoTest<<"% -- "<<std::endl;
}
