int getPointers(const uint8_t *data, const int d, int *pointers)
//returns an array that every cell indicates the next non-zero value of the data array
{
	int lastPointer=0;
	for (int i=0;i<d;i++)
		pointers[i]=d;
	for (int i=0;i<d;i++)
		if (data[i])
		{
			for (int j=lastPointer;j<i;j++)
				pointers[j]=i;
			lastPointer=i;
		}
	return 1;
}
