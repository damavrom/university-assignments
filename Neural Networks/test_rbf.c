#include "rbf_networks.c"
#include "mnist.c"
void standarlize(double **a, uint8_t **b,int n1,int n2)
{
        for (int i=0;i<n1;i++)
                a[i]=(double*)malloc(sizeof(double)*n2);
        for (int i=0;i<n2;i++){
		double mean=0;
		double deviation=0;
		for (int j=0;j<n1;j++)
			mean+=b[j][i];
		mean/=n1;
		for (int j=0;j<n1;j++)
			deviation+=pow((b[j][i]-mean),2);
		deviation/=n1;
		if (deviation==0)
			for (int j=0;j<n1;j++)
				a[j][i]=0;
		else
			for (int j=0;j<n1;j++)
				a[j][i]=(b[j][i]-mean)/deviation;
	}
}
void doublelize(double **a, uint8_t **b,int n1,int n2)
{
        for (int i=0;i<n1;i++)
        {
                a[i]=(double*)malloc(sizeof(double)*n2);
                for (int j=0;j<n2;j++)
                        a[i][j]=(double)b[i][j];
//			if (b[i][j]>255/2)
//				a[i][j]=1;
//			else
//				a[i][j]=0;
        }
}
void digToBin(const uint8_t a, double *b)
{
        for (int i=0;i<10;i++)
                b[i]=0;
        b[a%10]=1;
}
int outputToInt(double *out)
{
        double max=0;
        int maxDig=0;
        for (int i=0;i<10;i++)
                if (max<out[i])
                {
                        max=out[i];
                        maxDig=i;
                }
        return maxDig;
}
int main()
{
        printf(" ## Reading MNIST database ##\n");
        int d=28*28;
        double *in=(double*)malloc(sizeof(double)*d);
        double out[10];

        int sizeTrain=60000;
	double **imagesTrain=(double**)malloc(sizeof(double*)*sizeTrain);
	uint8_t **imagesTrainTemp=(uint8_t**)malloc(sizeof(uint8_t*)*sizeTrain);
	readImages1d("train-images-idx3-ubyte",imagesTrainTemp,sizeTrain,d);
        doublelize(imagesTrain,imagesTrainTemp,sizeTrain,d);
	uint8_t labelsTrain[sizeTrain];
	readLabels("train-labels-idx1-ubyte",labelsTrain,sizeTrain);

	int sizeTest=10000;
	double **imagesTest=(double**)malloc(sizeof(double*)*sizeTest);
	uint8_t **imagesTestTemp=(uint8_t**)malloc(sizeof(uint8_t*)*sizeTest);
	readImages1d("t10k-images-idx3-ubyte",imagesTestTemp,sizeTest,d);
        doublelize(imagesTest,imagesTestTemp,sizeTest,d);
	uint8_t labelsTest[sizeTest];
	readLabels("t10k-labels-idx1-ubyte",labelsTest,sizeTest);
        printf(" -- train samples:%d -- test samples:%d\n",sizeTrain,sizeTest);
        printf("\n");
		
	srand(time(0));
	for (int j=0;j<sizeTrain;j++)
	{
		int k=rand()%sizeTrain;
		double* tempIm=imagesTrain[k];
		imagesTrain[k]=imagesTrain[j];
		imagesTrain[j]=tempIm;
		uint8_t tempLa=labelsTrain[k];
		labelsTrain[k]=labelsTrain[j];
		labelsTrain[j]=tempLa;
	}
//	for (int j=0;j<d;j++)
//		printf("%.2f ",imagesTrain[5][j]);
//        printf("\n");
        //int rbf_for_dig=200;
        //int rbf_neurons=rbf_for_dig*10;
        int rbf_neurons=0;
	struct RBFneuron *neuronsTemp[10];

        sizeTrain=100;
        printf(" ## RBF Training ##\n");
        getCentersBetas(imagesTrain,labelsTrain,d,sizeTrain,neuronsTemp);
        for (int i=0;i<10;i++){
		struct RBFneuron *temp;
		temp=neuronsTemp[i];
		while (temp){
			rbf_neurons++;
			temp=temp->next;
		}
	}
        double **rbf_centers=(double **)malloc(sizeof(double *)*rbf_neurons);
        double *rbf_betas=(double *)malloc(sizeof(double)*rbf_neurons);
	int cnt=0;
        for (int i=0;i<10;i++){
		struct RBFneuron *temp;
		temp=neuronsTemp[i];
		while (temp){
			rbf_centers[cnt]=(double *)malloc(sizeof(double)*d);
			for (int j=0;j<d;j++){
				rbf_centers[cnt][j]=temp->center[j];
				rbf_betas[cnt]=temp->beta;
			}
			temp=temp->next;
			cnt++;
		}
	}
        printf(" -- neurons: %d --\n",rbf_neurons);
        printf("\n");
	//return 0;
        //k_random(rbf_neurons,imagesTrain,d,sizeTrain,rbf_centers);
//        for (int i=0;i<10;i++){
//                printf("  computing centers (%d/%d)\r",rbf_neurons*i/10,rbf_neurons);
//                fflush(stdout);
//                int sizeMeans=0;
//                for (int j=0;j<sizeTrain;j++)
//                        if (labelsTrain[j]==i)
//                                sizeMeans++;
//                double *imagesMeans[sizeMeans];
//                int cnt=0;
//                for (int j=0;j<sizeTrain;j++)
//                        if (labelsTrain[j]==i)
//                        {
//                                imagesMeans[cnt]=imagesTrain[j];
//                                cnt++;
//                        }
//                k_means(rbf_for_dig,imagesMeans,d,sizeMeans,rbf_centers+i*rbf_for_dig);
//                getBetas(rbf_for_dig,imagesMeans,d,sizeMeans,rbf_centers+i*rbf_for_dig,rbf_betas+i*rbf_for_dig);
                //getBetas(rbf_neurons,imagesTrain,d,sizeTrain,rbf_centers,rbf_betas);
//        }
//        printf("  computing centers (%d/%d)\n",rbf_neurons,rbf_neurons);
//        //double *center=(double *)malloc(sizeof(double)*d);
//        //k_means(1,imagesTrain,d,sizeTrain,&center);
//        //double beta=1/(2*pow(deviation(imagesTrain,d,sizeTrain,center),2));
//        for (int i=0;i<rbf_neurons;i++){
//                printf("  computing deviations (%d/%d)\r",i,rbf_neurons);
//                fflush(stdout);
//                //rbf_betas[i]=beta;
//                rbf_betas[i]=1/(2*pow(deviation(imagesTrain,d,sizeTrain,rbf_centers[i]),2));
//        }
//        printf("  computing deviations (%d/%d)\n",rbf_neurons,rbf_neurons);
////        for (int i=0;i<d;i++)
////                printf("%.2f ",rbf_centers[4][i]);
////        printf("\n");
//        //getBetas2(rbf_neurons,imagesTrain,d,sizeTrain,rbf_betas);
//        printf("\n");
////        for (int i=0;i<rbf_neurons;i++)
////                printf("%f ",rbf_betas[i]);
////        printf("\n");
////        return 1;
//        printf("\n");
        
        sizeTrain=60000;
        printf(" ## Adjusting datasets ##\n");
        double **rbf_output=(double **)malloc(sizeof(double)*sizeTrain);

	rbf_output[9990]=(double *)malloc(sizeof(double)*rbf_neurons);
	feedRBF(rbf_neurons,d,rbf_centers,rbf_betas,imagesTrain[9990],rbf_output[9990]);
	printf("             \n");
	for (int m=0;m<rbf_neurons;m++)
	{
		printf("%f ",rbf_output[9990][m]);
		printf("             \n");
	}
	printf("             \n");
	printf(" -- neurons: %d --\n",rbf_neurons);
	printf("%d\n",labelsTrain[9990]);
	//return 0;

//        for (int j=0;j<sizeTrain;j++)
//        {
//                if (j%200==0)
//                {
//                        printf("  train data (%d/%d)\r",j,sizeTrain);
//                        fflush(stdout);
//                }
//                rbf_output[j]=(double *)malloc(sizeof(double)*rbf_neurons);
//                feedRBF(rbf_neurons,d,rbf_centers,rbf_betas,imagesTrain[j],rbf_output[j]);
//		free(imagesTrain[j]);
//        }
	//return 1;
//        printf("  train data (%d/%d)\n",sizeTrain,sizeTrain);
        double **rbf_output_test=(double **)malloc(sizeof(double)*sizeTest);
//        for (int j=0;j<sizeTest;j++)
//        {
//                if (j%200==0)
//                {
//                        printf("  test data (%d/%d)\r",j,sizeTest);
//                        fflush(stdout);
//                }
//                rbf_output_test[j]=(double *)malloc(sizeof(double)*rbf_neurons);
//                feedRBF(rbf_neurons,d,rbf_centers,rbf_betas,imagesTest[j],rbf_output_test[j]);
//        }
//        printf("  test data (%d/%d)\n",sizeTest,sizeTest);
//        printf("\n");


        printf(" ## Initializing MLP ##\n");
        int layers=2;
        int neurons[layers];
        neurons[0]=rbf_neurons;
        neurons[1]=10;
        printf(" -- layers:%d -- neurons in each layer:",layers);
        printf("%d",neurons[0]);
        for (int i=1;i<layers;i++)
                printf(", %d",neurons[i]);
        printf(" --\n");
        double *biases[layers-1];
        double **weights[layers-1];
        init(layers,neurons,biases,weights);
        printf("\n");

        printf(" ## MLP Training ##\n");
        int epochs=1000;
        printf(" -- epochs:%d --\n",epochs);
        printf("\n");
        for (int i=0;i<epochs;i++)
        {
                printf(" ## Epoch %d ##\n",i+1);
                for (int j=0;j<sizeTrain;j++)
                {
                        int k=rand()%sizeTrain;
                        double* tempIm=rbf_output[k];
                        rbf_output[k]=rbf_output[j];
                        rbf_output[j]=tempIm;
                        uint8_t tempLa=labelsTrain[k];
                        labelsTrain[k]=labelsTrain[j];
                        labelsTrain[j]=tempLa;
			if (i==0){
				tempIm=imagesTrain[k];
				imagesTrain[k]=imagesTrain[j];
				imagesTrain[j]=tempIm;
			}
                }
                
                int missTrain=0;

                int samp=1000;
                clock_t timer[samp];
                clock_t start=clock();
                for (int j=0;j<samp;j++)
                        timer[j]=clock();
                for (int j=0;j<sizeTrain;j++)
                {
			if (i==0)
			{
				rbf_output[j]=(double *)malloc(sizeof(double)*rbf_neurons);
				feedRBF(rbf_neurons,d,rbf_centers,rbf_betas,imagesTrain[j],rbf_output[j]);
				free(imagesTrain[j]);
			}
                        int teach=1;
                        feed(layers,neurons,biases,weights,rbf_output[j],out);
                        if (labelsTrain[j]!=outputToInt(out))
                        {
                                teach=1;
                                missTrain++;
                        }
                        timer[j%samp]=clock();
                        if (j%200==0)
                        {
                                int k=1;
                                if (j<samp)
                                        k=samp-j-1;
                                int eta=(timer[j%samp]-timer[(j+k)%samp])*(sizeTrain-j)/(CLOCKS_PER_SEC*(samp-k));
                                int etaH=eta/(60*60);
                                int etaM=(eta%(60*60))/60;
                                int etaS=eta%(60);
                                float missTrainPer=100*(float)missTrain/(j+1);
                                printf(" -- training miss rate:%.2f -- ",missTrainPer);
                                printf("epoch eta:");
                                if (etaH>0)
                                        printf(" %dh",etaH);
                                if (etaM>0)
                                        printf(" %dm",etaM);
                                if (etaS>0)
                                        printf(" %ds",etaS);
                                printf(" --        \r");
                                fflush(stdout);
                        }
                        if (teach)
                        {
				digToBin(labelsTrain[j],out);
				train(layers,neurons,biases,weights,rbf_output[j],out,0.1);
                        }
                }
                float missTrainPer=100*(float)missTrain/sizeTrain;
                printf(" -- training miss rate:%.2f --",missTrainPer);
                fflush(stdout);
                int missTest[10];
                for (int j=0;j<10;j++)
                        missTest[j]=0;
                float missTestPer;
                if (i>epochs-10|i%10==0)
                {
                        for (int j=0;j<sizeTest;j++)
                        {
				if (i==0)
				{
					rbf_output_test[j]=(double *)malloc(sizeof(double)*rbf_neurons);
					feedRBF(rbf_neurons,d,rbf_centers,rbf_betas,imagesTest[j],rbf_output_test[j]);
				}
                                feed(layers,neurons,biases,weights,rbf_output_test[j],out);
                                double max=0;
                                int maxDig=0;
                                int cnt=0;
                                for (int l=0;l<10;l++)
                                        if (out[labelsTest[j]]<out[l])
                                                cnt++;
                                missTest[cnt]++;
                        }
                        missTestPer=100-100*(float)missTest[0]/sizeTest;
                        printf(" testing miss rate:%.2f -- ",missTestPer);
                }
                int dur=(clock()-start)/CLOCKS_PER_SEC;
                int durH=dur/(60*60);
                int durM=(dur%(60*60))/60;
                int durS=dur%(60);
                printf("epoch duration:");
                if (durH>0)
                        printf(" %dh",durH);
                if (durM>0)
                        printf(" %dm",durM);
                if (durS>0)
                        printf(" %ds",durS);
                printf(" --        \r");
                printf("\n");
                printf("\n");
                //FILE *logFile=fopen("log6.txt","a");
                //fprintf(logFile,"%d %f %f\n",i+1,missTrainPer,missTestPer);
                //fclose(logFile);
        }
}
