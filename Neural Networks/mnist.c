#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

int readNumberOfItems(const char *filename)
{
	FILE* datafile;
	datafile=fopen(filename,"r");
	if (!datafile)
	{
		printf("error opening file\n");
		return 0;
	}
	unsigned char buff[4];
	fseek(datafile,4,SEEK_SET);
	fread(buff,1,4,datafile);
	int n=0;
	for (int i=0;i<4;i++)
		n+=(int)buff[3-i]*pow(2,8*i);
	fclose(datafile);
	return n;
}

int readResolution(const char *filename, int *resolution)
{
	FILE* datafile;
	datafile=fopen(filename,"r");
	if (!datafile)
	{
		printf("error opening file\n");
		return 0;
	}
	int magic=0;
	unsigned char buff[4];
	fread(buff,1,4,datafile);
	for (int i=0;i<4;i++)
	{
		magic+=(int)buff[3-i]*pow(2,8*i);
	}
	if (magic!=2051)
	{
		printf("incompatible data file\n");
		fclose(datafile);
		return 0;
	}
	fseek(datafile,8,SEEK_SET);
	for (int j=0;j<2;j++)
	{
		fread(buff,1,4,datafile);
		for (int i=0;i<4;i++)
		{
			resolution[j]+=(int)buff[3-i]*pow(2,8*i);
		}
	}
	fclose(datafile);
	return 1;
}
int readImages(const char *filename, uint8_t ***images, const int n, const int rows, const int columns)
{
	//open file
	FILE* datafile;
	datafile=fopen(filename,"r");
	if (!datafile)
	{
		printf("error opening file\n");
		return 0;
	}

	//read magic number
	uint8_t buff[4];
	int magic=0;
	fread(buff,1,4,datafile);
	for (int i=0;i<4;i++)
		magic+=(int)buff[3-i]*pow(2,8*i);
	if (magic!=2051)
	{
		printf("incompatible data file\n");
		fclose(datafile);
		return 0;
	}

	//read images
	fseek(datafile,16,SEEK_SET);
	for (int i=0;i<n;i++)
	{
		images[i]=(uint8_t **)malloc(sizeof(uint8_t*)*rows);
		for (int k=0;k<rows;k++)
		{
			images[i][k]=(uint8_t *)malloc(sizeof(uint8_t)*columns);
			for (int l=0;l<columns;l++)
			{
				fread(buff,1,1,datafile);
				images[i][k][l]=*buff;
			}
		}
	}
	fclose(datafile);
	return 1;
}

int readImages1d(const char *filename, uint8_t **images, const int n, const int size)
{
	//open file
	FILE* datafile;
	datafile=fopen(filename,"r");
	if (!datafile)
	{
		printf("error opening file\n");
		return 0;
	}

	//read magic number
	uint8_t buff[4];
	int magic=0;
	fread(buff,1,4,datafile);
	for (int i=0;i<4;i++)
		magic+=(int)buff[3-i]*pow(2,8*i);
	if (magic!=2051)
	{
		printf("incompatible data file\n");
		fclose(datafile);
		return 0;
	}

	//read images
	fseek(datafile,16,SEEK_SET);
	for (int i=0;i<n;i++)
	{
		images[i]=(uint8_t *)malloc(sizeof(uint8_t)*size);
		for (int j=0;j<size;j++)
		{
				fread(buff,1,1,datafile);
				images[i][j]=*buff;
		}
	}
	fclose(datafile);
	return 1;
}

int readLabels(const char *filename, uint8_t *labels, const int n)
{
	FILE* datafile;
	datafile=fopen(filename,"r");
	if (!datafile)
	{
		printf("error opening file\n");
		return 0;
	}
	int magic=0;
	uint8_t buff[4];
	fread(buff,1,4,datafile);
	for (int i=0;i<4;i++)
		magic+=(int)buff[3-i]*pow(2,8*i);
	if (magic!=2049)
	{
		printf("incompatible data file\n");
		fclose(datafile);
		return 0;
	}
	fseek(datafile,8,SEEK_SET);
	for (int i=0;i<n;i++)
	{
		fread(buff,1,1,datafile);
		labels[i]=(int)*buff;
	}
	fclose(datafile);
	return 1;
}
