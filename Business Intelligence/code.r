###   Exercise i   ###

# Defining a list of the interesting products
prods = c("citrus fruit","tropical fruit","whole milk",
          "other vegetables","rolls/buns","chocolate","bottled water",
          "yogurt","sausage","root vegetables","pastry","soda","cream")

# Reading data from file
groc.init <- read.csv("GroceriesInitial.csv", header=TRUE)
#groc.init <- groc.init[sample(nrow(groc.init), 1000),]

# Defining new structure and adding new values like discrete value and
# products
groc.proc <- groc.init[c("id", "basket_value", "recency_days")] 
for (prod in prods){
    groc.proc[prod] <- rep(FALSE, nrow(groc.proc))
    for (i in c(4:35)){
        groc.proc[prod] <- ( prod == groc.init[,i] ) | groc.proc[prod]
    }
}
groc.proc$low_value_basket <- groc.proc$basket_value < 2.8
groc.proc$medium_value_basket <- groc.proc$basket_value >= 2.8 &
    groc.proc$basket_value < 5.9
groc.proc$high_value_basket <- groc.proc$basket_value >= 5.9


###   Exercise ii   ###

##  Part a  ##
# Importing library with apriori
library(arules) 
# Defining a list with different minimum support
supps <- c(0.01, 0.02)
ctrl <- list(verbose = FALSE)
# Running apriori algorithm for every minimum support
for (s in supps){
    params <- list(supp = s, conf = 0.5)
    rules <- apriori(groc.proc[prods], parameter = params, control = ctrl)
    inspect(rules)
}

## Part b ##
# Mining all rules using apriori
params <- list(supp = 0.0001, conf = 0.1, maxlen = 2, minlen = 2)
rules <- apriori(groc.proc[prods], parameter = params, control = ctrl)
# Sorting rules
rules.sorted <- sort(rules, by=c("confidence","support"))
# Printing
inspect(rules.sorted[1:20])

## Part c ##
# Mining all rules using apriori
params <- list(supp = 0.0, conf = 0.0, maxlen = 2, minlen = 2)
rules <- apriori(
   groc.proc[c(prods,
               "low_value_basket",
               "medium_value_basket",
               "high_value_basket")],
   parameter = params,
   control = ctrl)
# Sorting rules
rules.sorted <- sort(rules, by=c("confidence","support"))
# Printing only those associated with value
inspect(subset(
    rules.sorted,
    subset = rhs %in% c("low_value_basket",
                        "medium_value_basket",
                        "high_value_basket"))[1:20])


###   Exercise iii   ###

##  Part a  ##
fit <- kmeans(groc.proc[,c("basket_value", "recency_days")], 5)
fit

##  Part b  ##
fit$centers
write.csv(fit$centers, "centers.csv")

stand.dev <- NULL
for (i in c(1:5)){
    stand.dev <- rbind(stand.dev, sapply(groc.proc[which(fit$cluster == i), c("basket_value", "recency_days")], sd))
}
typeof(stand.dev)
print(stand.dev)
write.csv(stand.dev, "standard_deviation.csv")

##  Part c  ##
for (i in c(1:5)){
    groc.proc[paste("cluster", i, sep = "")] <- fit$cluster == i
}


###   Exercise iv   ###

params <- list(supp = 0.0, conf = 0.0, maxlen = 2, minlen = 2)
rules <- apriori(
   groc.proc[c(prods,
             paste("cluster", c(1:5), sep = "")
               )],
   parameter = params,
   control = ctrl)
# Sorting rules
rules.sorted <- sort(rules, by=c("confidence","support"))
# Printing only those associated with clustter
inspect(subset(
    rules.sorted,
    subset = lhs %in% paste("cluster", c(1:5), sep = ""))[1:20])
write(subset(
    rules.sorted,
    subset = lhs %in% paste("cluster", c(1:5), sep = ""))[1:20], "rules.dat")

###   Exercise v   ###
